<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutputTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('output', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('source_id');
			$table->string('status', 255);
			$table->string('send_method', 255);
			$table->string('hash', 255);
			$table->string('import_batch');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('output');
	}

}
