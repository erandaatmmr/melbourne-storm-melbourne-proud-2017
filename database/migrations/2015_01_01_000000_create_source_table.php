<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('source', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('send_method', 255)->default('email');
			$table->string('hash', 255);
			$table->string('short_url', 255);
			$table->string('import_batch');
			$table->string('mailing_list');
			$table->integer('email_duplicates')->default('1');
			$table->integer('mobile_duplicates')->default('1');
			$table->integer('invalid_email')->default('0');
			$table->integer('invalid_mobile')->default('0');
			$table->integer('output_id');
			$table->dateTime('output_at');
			$table->integer('bounced')->default('0');
			$table->dateTime('clicked_at');
			$table->integer('resend1_exclude')->default('0');
			$table->integer('resend2_exclude')->default('0');
			$table->integer('resend3_exclude')->default('0');
			$table->integer('resend4_exclude')->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('source');
	}

}
