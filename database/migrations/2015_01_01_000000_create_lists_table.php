<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cm_lists', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 255);
			$table->integer('list_created')->default('0');
			$table->integer('fields_created')->default('0');
			$table->integer('list_imported')->default('0');
			$table->string('campaign_id', 255)->default('0');
			$table->string('list_id', 255);
			$table->string('import_batch', 255);
			$table->string('mailing_list', 255);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cm_lists');
	}

}
