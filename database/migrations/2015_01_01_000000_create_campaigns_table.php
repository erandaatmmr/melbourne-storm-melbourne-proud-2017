<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cm_campaigns', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 255);
			$table->integer('created')->default('0');
			$table->string('campaign_id', 255);
			$table->integer('list_id')->default('0');
			$table->string('cm_list_id', 255);
			$table->string('template_id', 255);
			$table->string('from_name', 255);
			$table->string('from_email', 255);
			$table->string('reply_email', 255);
			$table->string('subject', 255);
			$table->string('import_batch', 255);
			$table->string('mailing_list', 255);
			$table->string('sent', 255);
			$table->integer('recipients')->default('0');
			$table->integer('opened')->default('0');
			$table->integer('clicks')->default('0');
			$table->integer('bounces')->default('0');
			$table->integer('unsubscribes')->default('0');
			$table->integer('complaints')->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cm_campaigns');
	}

}
