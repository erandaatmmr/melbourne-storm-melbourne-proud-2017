<?php



use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder{

    public function run()
    {

        if (App::environment() === 'production') {
            exit('Production!');
        }

        User::create([
            'name' => 'MMR Studio',
            'email' => 'web@mmr.com.au',
            'password' => bcrypt('hpi2010AFO'),
            'role_id' => '1',
        ]);

    }

}
