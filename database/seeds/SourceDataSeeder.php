<?php



use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Source;

class SourceDataSeeder extends Seeder{

    public function run()
    {

        if (App::environment() === 'production') {
            exit('Production Environment!');
        }

        $source_data = [];
        $source_file = storage_path('import.csv');
        //echo $source_file; exit;

        ini_set('memory_limit','1024M');

        Excel::load($source_file, function($reader) {

            $source_data = $reader->all()->toArray();

            //print_r($source_data); exit;

            $import_batch = date('U');
    
            DB::table('source')->truncate();
    
            foreach($source_data as $member)
            {
                $member['import_batch'] = $import_batch;
                $member['hash'] = md5(implode('', $member));    
                Source::create($member);
            }

            // check for email duplicates
            

        });

    }

}