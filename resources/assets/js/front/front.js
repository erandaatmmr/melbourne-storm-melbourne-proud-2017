
    function capitalize(s) {
        return s[0].toUpperCase() + s.slice(1);
    }

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    @@include('plugins/jquery.ui.widget.js')
    @@include('plugins/jquery.iframe-transport.js')
    @@include('plugins/jquery.fileupload.js')
    @@include('../../../../node_modules/video.js/dist/video.js')

    function trackEvent(trackData) {
        //alert(JSON.stringify(trackData, null, 4));
        //console.log(trackData);
        ga('send', 'event', trackData.category, trackData.action, trackData.label, trackData.value);
    }

    function trackLink() {

        var link = $(this);
        var trackData = {
            category: link.data('track-category'),
            action: link.data('track-action'),
            label: link.data('track-label'),
            value: link.data('track-value') ? link.data('track-value') : 0
        }

        trackEvent(trackData);

    }

    function fbTrackLink() {
        var link = $(this);
        fbq('track', link.data('fb-track'));
    }

    function initTrackLinks($root) {
        $root.find('.track-link').on('click', trackLink);
        $root.find('.fb-track').on('click', fbTrackLink);
    }

    function urlParam(name) {
        var url = location.href;
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( url );
        return results == null ? null : decodeURIComponent(results[1]);
    }

    $(document).ready(function() {

        //BP_SC.afl.reportingBeacon('BP|Sport|AFL|AFL Clubs|Western Bulldogs|Lost Bulldogs|' + currentRoute);

        var $b = $('body');

        initTrackLinks($('body'));

        $('.nav-share-video').on('click', function(e) { return e.preventDefault() });


        $('.scroll__button').on('click', function(e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: $('.product-offerings--home').offset().top+80
            }, 2000);

            return false;
		});

        $('.join-button').on('click', function() {
            fbq('track', 'CompleteRegistration');
            window.open($(this).data('link'));
        });

        $(document).on('click', '.vjs-big-play-button', function() {

            trackEvent({
                category: 'Video',
                action: 'Member Video',
                label: $('#memberVideoPlayer').data('id'),
                value: $('#memberVideoPlayer').data('id')
            });

        });


        var memberVideoEl = $('#memberVideoEl');

        if(memberVideoEl.length > 0) {

            var memberVideo = videojs('memberVideo', {}, function() {



            });

        }

        // Create friend poster
        var createFriendPoster = $('#createFriendPoster');

        if(createFriendPoster.length > 0) {

            var createFriendPosterUpload = $('#createFriendPosterUpload');
            var createFriendPosterUploadLabel = $('#createFriendPosterUploadLabel');
            var createFriendPosterInput = $('#createFriendPosterInput');

            var createFriendPosterNameFirst = $('#createFriendPosterNameFirst');
            var createFriendPosterEmail = $('#createFriendPosterEmail');
            var createFriendPosterSubmit = $('#createFriendPosterSubmit');
            var createFriendPosterCreated = $('#createFriendPosterCreated');
            var createFriendPosterCreatedInner = $('#createFriendPosterCreatedInner');
            //var createFriendPosterCreatedName = $('#createFriendPosterCreatedName');
            var createFriendPosterCreatedThumb = $('#createFriendPosterCreatedThumb');
            var createFriendPosterCreatedPDFPrint = $('#createFriendPosterCreatedPDFPrint');
            var createFriendPosterCreatedPDFDownload = $('#createFriendPosterCreatedPDFDownload');
            var createFriendPosterCreatedFBShare = $('#createFriendPosterCreatedFBShare');
            var createFriendPosterCreatedTwitterShare = $('#createFriendPosterCreatedTwitterShare');
            var createFriendPosterCreatedEmail = $('#createFriendPosterCreatedEmail');
            var createFriendPosterCreatedHash = $('#createMemberPosterCreatedHash');


            createFriendPosterUpload.on('click', function (e) {

                if(createFriendPosterNameFirst.val().trim().length == 0) {
                    e.preventDefault();
                    return alert('Please enter your name');
                }

                var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                if( ! re.test(createFriendPosterEmail.val())){
                    e.preventDefault();
                    return alert('Please enter a valid email');
                }
            });

            createFriendPosterUpload.fileupload({
                url: rootURL + '/poster/upload',
                dataType: 'json',

                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    createFriendPosterUploadLabel.html(progress.toString() + '%');
                },

                add: function (e, data) {
                    var uploadErrors = [];

                    var acceptFileTypes = /\/(jpeg|jpg)$/i;
                    if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                        uploadErrors.push('Invalid file type. Pleas upload JPEG images');
                    }

                    if (data.originalFiles[0]['size'] > 5000000) {
                        uploadErrors.push('Filesize is too big');
                    }

                    if(uploadErrors.length > 0) {
                        alert(uploadErrors.join("\n"));
                    } else {
                        data.submit();
                    }

                },

                done: function (e, data, r) {
                    //console.log('data.result', data.result);

                    if(data.result.status == 'ok') {

                        var thumbURL = data.result.thumb_url;
                        var pdfURL = data.result.pdf_url;
                        var pdfDownloadURL = data.result.pdf_download_url;
                        var downloadURL = data.result.download_url;
                        var shareURL = data.result.share_url;
                        var posterHash = data.result.hash;
                        var email = data.result.email;

                        trackEvent({
                            category: 'Friend Poster',
                            action: 'Create',
                            label: 'Created',
                            value: email
                        });

                        $.ajax({ url: thumbURL }).done(function(data) {

                            $b.animate({
                                scrollTop: createFriendPoster.position().top + 320
                            }, 500);

                            createFriendPosterCreatedThumb.attr('src', thumbURL);
                            createFriendPosterCreatedPDFPrint.attr('href', downloadURL);
                            createFriendPosterCreatedPDFDownload.attr('href', pdfDownloadURL);
                            createFriendPosterCreatedFBShare.attr('href', 'https://www.facebook.com/dialog/feed?app_id=1514935971851671&link=' + shareURL);
                            createFriendPosterCreatedTwitterShare.attr('href', 'https://twitter.com/intent/tweet?status=' + encodeURIComponent('Check out who Storm\'s Secret Weapon is! #purplepride @storm ' + shareURL));
                            createFriendPosterCreatedHash.val(posterHash);

                            createFriendPosterCreated.find('.track-link').attr('data-track-value', email);

                            setTimeout(function() {
                                createFriendPosterCreated.css({
                                    height: createFriendPosterCreatedInner.outerHeight()
                                });

                                createFriendPosterUploadLabel.html('Upload Photo to create poster');

                            }, 500);

                        });

                    }

                }
            });

            createFriendPoster.on('submit', function(e) {
                e.preventDefault();

                var friendName = createFriendPosterNameFirst.val();
                var friendEmail = createFriendPosterEmail.val();
                var thumbURL = rootURL + '/poster/friend/thumb?name=' + friendName;

                if(friendName.val().trim().length == 0) {
                    return alert('Please enter your name');
                }

                var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                if( ! re.test(friendEmail)){
                    return alert('Please enter a valid email');
                }

                createFriendPosterCreated.css({
                    height: 0
                });

                createFriendPosterSubmit.html('Loading...').prop('disabled', true);

                $.ajax({ url: thumbURL }).done(function(data) {
                    createFriendPosterCreatedThumb.attr('src', thumbURL);
                    //createFriendPosterCreatedName.html(friendName);

                    $b.animate({
                        scrollTop: createFriendPoster.position().top + 320
                    }, 500);

                    var downloadURL = rootURL + '/poster/friend/download?name=' + friendName;
                    var pdfURL = rootURL + '/poster/friend/pdf?name=' + friendName;
                    var shareURL = rootURL + '/poster/friend/share/' + friendName;

                    createFriendPosterCreatedPDFPrint.attr('href', downloadURL);
                    createFriendPosterCreatedPDFDownload.attr('href', pdfURL + '&download=1');
                    createFriendPosterCreatedFBShare.attr('href', 'https://www.facebook.com/dialog/feed?link=' + shareURL);
                    createFriendPosterCreatedTwitterShare.attr('href', 'https://twitter.com/intent/tweet?status=' + encodeURIComponent('Have you seen a lost bulldog? #bemorebulldog in 2016 #lostbulldogs @westernbulldogs ' + shareURL));
                    createFriendPosterCreatedEmail.val(friendName);

                    trackEvent({
                        category: 'Friend Poster',
                        action: 'Create',
                        label: 'Created'
                    });

                    setTimeout(function() {

                        createFriendPosterCreated.css({
                            height: createFriendPosterCreatedInner.outerHeight()
                        });

                        createFriendPosterSubmit.html('Create Poster').prop('disabled', false);

                    }, 500);

                });

            });

        }

        // Create member poster
        var createMemberPoster = $('#createMemberPoster');

        $(document).on('click', '.fb-share', function(e) {
            e.preventDefault();

            FB.ui({
              method: 'share',
              href: $(this).attr('href')
            }, function(response){});

        });

        // DYNAMIC VIDEO
        var dynamicVideoForm = $('#dynamicVideoForm');
        if(dynamicVideoForm.length > 0) {

            var dynamicVideoWrap = $('#dynamicVideoWrap');
            var dynamicVideoPlayer = $('#dynamicVideoPlayer');
            var dynamicVideoNameFrist = $('#dynamicVideoNameFrist');
            var dynamicVideoNameLast = $('#dynamicVideoNameLast');
            var dynamicVideoEmail = $('#dynamicVideoEmail');
            var dynamicVideoRecieveEmails = $('#dynamicVideoRecieveEmails');
            var dynamicVideoSubmit = $('#dynamicVideoSubmit');

            dynamicVideoForm.on('submit', function(e) {
                e.preventDefault();

                //var videoName = dynamicVideoName.val();
                //var videoYear = dynamicVideoLastYear.val();

                // validate input
                if(dynamicVideoNameFrist.val().trim().length == 0) {
                    return alert('Please enter your first name');
                }

                if(dynamicVideoNameLast.val().trim().length == 0) {
                    return alert('Please enter your last name');
                }

                var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                if( ! re.test(dynamicVideoEmail.val())){
                    return alert('Please enter a valid email');
                }

                var re = /^[a-zA-Z0-9_-\s]*$/;
                if( ! re.test(dynamicVideoNameFrist.val())){
                    return alert('There is an invalid character in first name');
                }

                if( ! re.test(dynamicVideoNameLast.val())){
                    return alert('There is an invalid character in last name');
                }


                var videoRequestURL = rootURL + '/video/url?namefirst=' + encodeURIComponent(dynamicVideoNameFrist.val()) + '&namelast=' + encodeURIComponent(dynamicVideoNameLast.val()) + '&email=' + encodeURIComponent(dynamicVideoEmail.val())+ '&recieve_emails=' + encodeURIComponent(dynamicVideoRecieveEmails.prop('checked'));

                dynamicVideoWrap.addClass('hero__video__home--loading');

                $.get(videoRequestURL).done(function(response) {
                    var responseHTML = $('<div>' + response.toString() + '</div>');
                    var videoURLEl = responseHTML.find('#creatify_video');
                    var videoURL = videoURLEl.attr('va');


                    //console.log('videoURL', videoURL);

                    var fbShareURLParam = dynamicVideoNameFrist.val() + '/' + videoURL.replace('https://s3-ap-southeast-2.amazonaws.com/mstorm1216/render/', '');
                    var fbShareURL = window.fbShareURLTemplate.replace('{LINK}', window.btoa(fbShareURLParam));

                    $('.videoShareLinkFB').attr('href', fbShareURL);
                    $('.videoDownloadLink').attr('href', videoURL);


                    var videoEl = $('<video id="dynamicVideo" class="video-js vjs-default-skin" controls preload="auto" width="640" height="264" autoplay data-setup=""><source src="' + videoURL + '" type="video/mp4" /></video>');

                    dynamicVideoPlayer.append(videoEl);
                    dynamicVideoPlayer.removeClass('hero__video__player--hidden');

                    videojs('dynamicVideo', {}, function() {

                        trackEvent({
                            category: 'Video',
                            action: 'Play',
                            label: 'Dynamic Video',
                            value: 0
                        });

                    });

                    dynamicVideoWrap.addClass('hero__video__home--loaded');
                    dynamicVideoWrap.removeClass('hero__video__home--loading');

                });

                //alert(videoRequestURL);

            });

        }

        var requestCallModal = $('#requestCallModal');
        if(requestCallModal.length > 0) {

            $('.request-call-modal').on('click', function(e) {
                e.preventDefault();
                requestCallModal.removeClass('modal--hidden');
            });

            var requestCallModalBody = $('#requestCallModalBody');
            var requestCallModalErrors = $('#requestCallModalErrors');
            var requestCallForm = $('#requestCallForm');
            var requestCallPhone = $('#requestCallPhone');
            var requestCallTime = $('#requestCallTime');
            var requestCallSubmit = $('#requestCallSubmit');

            requestCallForm.on('submit', function(e) {
                e.preventDefault();

                var rcData = $(this).serialize();

                $.post('/request-call', rcData).always(function(response) {

                    var responseJSON = response.responseJSON;

                    //console.log('responseJSON', responseJSON);

                    if(response == 'ok') {
                        requestCallModalBody.html('<p class="modal__response">Thanks! A member of the Lost Dog Squad will be in touch with you shortly.</p>');

                        trackEvent({
                            category: 'Contact',
                            action: 'Request a Call',
                            label: 'Submit',
                            value: 0
                        });

                        fbq('track', 'Lead');

                    } else {
                        requestCallModalErrors.removeClass('modal__errors--hidden');

                        var errors = [];

                        $.each(responseJSON, function(i, v) {
                            errors.push(v[0]);
                        });

                        requestCallModalErrors.find('.errors').html(errors.join('<br>'));
                    }

                });

                //console.log(rcData);

            });

        }

        var sendFriendPosterModal = $('#sendFriendPosterModal');
        if(sendFriendPosterModal.length > 0) {

            var friendName = '';

            $('.send-friend-poster').on('click', function(e) {
                e.preventDefault();
                sendFriendPosterModal.removeClass('modal--hidden');
                friendName = $(this).data('name');
            });

            var sendFriendPosterForm = $('#sendFriendPosterForm');
            var sendFriendPosterModalBody = $('#sendFriendPosterModalBody');
            var sendFriendPosterModalErrors = $('#sendFriendPosterModalErrors');
            var sendFriendPosterSubmit = $('#sendFriendPosterSubmit');

            sendFriendPosterForm.on('submit', function(e) {
                e.preventDefault();

                var formData = $(this).serialize();
                //console.log('formData', formData);

                $.post('/send-friend-poster', formData).always(function(response) {

                    var responseJSON = response.responseJSON;

                    //console.log('responseJSON', responseJSON);

                    if(response == 'ok') {
                        sendFriendPosterModalBody.html('<p class="modal__response">Thanks! Your poster has been sent.</p>');

                        trackEvent({
                            category: 'Friend Poster',
                            action: 'Send to Friend',
                            label: '',
                            value: 0
                        });

                        trackEvent({
                            category: 'Share',
                            action: 'Send to Friend',
                            label: 'Friend Poster',
                            value: 0
                        });

                    } else {
                        sendFriendPosterModalErrors.removeClass('modal__errors--hidden');

                        var errors = [];

                        $.each(responseJSON, function(i, v) {
                            errors.push(v[0]);
                        });

                        sendFriendPosterModalErrors.find('.errors').html(errors.join('<br>'));
                    }

                });

            });

        }

        var sendMemberPosterModal = $('#sendMemberPosterModal');
        if(sendMemberPosterModal.length > 0) {

            var friendName = '';

            $('.send-member-poster').on('click', function(e) {
                e.preventDefault();
                sendMemberPosterModal.removeClass('modal--hidden');
                friendName = $(this).data('name');
            });

            var sendMemberPosterForm = $('#sendMemberPosterForm');
            var sendMemberPosterModalBody = $('#sendMemberPosterModalBody');
            var sendMemberPosterModalErrors = $('#sendMemberPosterModalErrors');
            var sendMemberPosterSubmit = $('#sendMemberPosterSubmit');

            sendMemberPosterForm.on('submit', function(e) {
                e.preventDefault();

                var formData = $(this).serialize();
                //console.log('formData', formData);

                $.post('/send-member-poster', formData).always(function(response) {

                    var responseJSON = response.responseJSON;

                    //console.log('responseJSON', responseJSON);

                    if(response == 'ok') {
                        sendMemberPosterModalBody.html('<p class="modal__response">Thanks! Your poster has been sent.</p>');

                        trackEvent({
                            category: 'Member Poster',
                            action: 'Send to Friend',
                            label: '',
                            value: 0
                        });

                        trackEvent({
                            category: 'Share',
                            action: 'Send to Friend',
                            label: 'Member Poster',
                            value: 0
                        });

                    } else {
                        sendMemberPosterModalErrors.removeClass('modal__errors--hidden');

                        var errors = [];

                        $.each(responseJSON, function(i, v) {
                            errors.push(v[0]);
                        });

                        sendMemberPosterModalErrors.find('.errors').html(errors.join('<br>'));
                    }

                });

            });

        }

        $(document).on('click', '.modal__close', function(e) {
            e.preventDefault();
            $(this).parent().parent().addClass('modal--hidden');
        });

        var toggleNav = $('#toggleNav');
        var responsiveNavDissmiss = $('#responsiveNavDissmiss, #toggleNavClose');

        // RESPONSIVE NAV
        toggleNav.on('click', function() {
            $b.toggleClass('nav-open');
        });

        responsiveNavDissmiss.on('click', function() {
            $b.removeClass('nav-open');
        });

        if(urlParam('render') == 'true') {
            $('#dynamicVideoNameFrist').val(urlParam('fname'));
            $('#dynamicVideoNameLast').val(urlParam('lname'));
            $('#dynamicVideoEmail').val(urlParam('email'));
            $('#dynamicVideoForm').submit();
        }

        $('.videoShareLinkFB, .videoDownloadLink').on('click', function(e) {
            var a = $(this);
            if(a.attr('href') == '#') {
                e.preventDefault();
                alert('Please enter your details and generate the video')
                $('html, body').animate({
                    scrollTop: 0
                }, 1000);
            }

        });

    });
