<!DOCTYPE html>
<html lang="en">

    <head>

        @include('includes.header-common')

    </head>

    <body class="route-{{ Route::currentRouteName() }}">

        @include('includes.page-header')

        <section class="main">
            <div class="container">
                @yield('content')
            </div>
        </section>

        @include('includes.footer-common')

    </body>

</html>
