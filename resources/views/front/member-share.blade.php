@extends('../front')

@section('content')

    <div class="hero">

        <h1 class="hero__title">Found Bulldog</h1>

        <div class="hero__image">
            <img src="{{ $photo_url }}" alt="You've Been Found">
        </div>

        <div class="hero__member-found hero__member-found--share">
            <h2 class="hero__member-found__title">{{ $poster->name }}</h2>
            <div class="hero__member-found__text">IS COMMITTED TO THE KENNEL IN 2016<br>#BEMOREBULLDOG</div>
        </div>

    </div>

    @include('front.home-hero')

    @include('front.home-content')

    @include('front.callouts')

@endsection
