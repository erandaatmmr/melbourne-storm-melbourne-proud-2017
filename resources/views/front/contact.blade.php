@extends('../front')

@section('content')

    <div class="contact top">

        <div class="container">
            <h1>Contact</h1>

            <div class="contact__col">
			@if($posted)
          	
          	<div class="contact__col__center">
				<p class="contact__sent ">Thanks! Your enquiry has been sent. A member of the team will be in touch with you shortly.</p>
				<script> fbq('track', 'Lead'); </script>
			   <h2 class="contact__col__title">Have a question?<br><span>call us on<br> 1300 786 767</span></h2>
           
			</div>
           @else
            <div class="contact__col-half ">
				
					<h2 class="contact__col__title">Have a question?<br><span>call us on<br> 1300 786 767</span></h2>
					

					





						

					

						<p class="contact__col__title-white">If you have any queries please send us a message. Our Membership Team will get back to you as soon as possible.</p>
				
            </div>

            <div class="contact__col-half">
            @if ($errors->has())
            <div class="contact__errors">
                <div class="contact__errors__title">Please check the following errors:</div>
                @foreach ($errors->all() as $message)
                {{ $message }}<br>
                @endforeach
            </div>
            @endif

            
            {!! Form::open(array('action' => 'FrontController@postContact', 'class' => 'contact__form form')) !!}

                <div class="contact__form__field form__field">
                    
						<label>Name*</label>
                        <input type="text" name="name"  class="form__input" value="{{ old('name', '') }}">
                    
                </div>

                <div class="contact__form__field form__field">
                   
                       <label>Email*</label>
                        <input type="text" name="email" class="form__input" value="{{ old('email', '') }}">
                   
                </div>

                <div class="contact__form__field form__field">
                    
                        <label>Phone / mobile*</label>
                        <input type="text" name="phone" class="form__input" value="{{ old('phone', '') }}">
                    
                </div>

                <div class="contact__form__field form__field">
                    
                         <label>Comments*</label>
                        <textarea name="message" placeholder="Any other questions or comments" class="form__input">{{ old('message', '') }}</textarea>
                    
                </div>

                <div class="contact__form__submit form__submit">
                    <button class="button button--gold" type="submit">Submit</button>
                </div>

            {!! Form::close() !!}
            </div>
            @endif
            </div>

           

        </div>

    </div>
    
    <div class="hexarrow contact">
        
    </div>
    
    <div class="contact bottom">

        <div class="container">
        	 <div class="contact__connect">

                <p>Visit <a href="http://www.melbournestorm.com.au/" target="_blank">melbournestorm.com.au</a></p>

                <ul class="social-buttons">
                    <li><a href="https://www.facebook.com/MelbStormRLC" class="social-buttons__button social-buttons__button__facebook" target="_blank">Facebook</a></li>
                    <li><a href="https://twitter.com/storm" class="social-buttons__button social-buttons__button__twitter" target="_blank">Twitter</a></li>
                    <li><a href="https://www.instagram.com/melbstormrlc/" class="social-buttons__button social-buttons__button__instagram" target="_blank">Instagram</a></li>
                </ul>

            </div>
		</div>
	</div>

   

@endsection
