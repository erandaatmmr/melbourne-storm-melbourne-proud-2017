@extends('../front')

@section('content')

    <div class="hero">

        <div class="hero__video">
            <div class="hero__video__inner">
                <div class="hero__video__player" id="memberVideoPlayer" data-id="{{ $source->id }}">

                    <video id="memberVideo" class="video-js vjs-default-skin" controls preload="auto" width="640" height="264" poster="{{ asset('/images/video-poster.jpg') }}" data-setup='{}'>
                        <source src="{{ $source->video_url }}" type="video/mp4" />
                        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that supports HTML5 video</p>
                    </video>

                </div>
            </div>
        </div>

        <h1 class="hero__title">{{ $source->name_first }}</h1>
        <h1 class="hero__title--sub">You are our Secret Weapon, pick your place in the team below.</h1>

    </div>


    <div class="hexarrow">
        <div class="container">

            <div class="product-offerings product-offerings--home">

                <div class="product-offerings__offering">

                    <div class="product-offerings__offering__option"><span>{{($source->data_set == 'Unrenewed')?'Existing':'Option 1'}}</span></div>
                    <div class="product-offerings__offering__title">{{ $source->option1_title }}</div>
                    <div class="product-offerings__offering__offer__action">{{ $source->option1_description }}</div>
                    <div class="product-offerings__offering__offer__year">2017 Adult Membership just</div>
                    <div class="product-offerings__offering__offer__price">${{ number_format($source->option1_price_month, 2, ".", "") }}<sup>*</sup></div>
                    <div class="product-offerings__offering__offer__duration">Per month</div>



                    {!! Form::open(array('action' => 'FrontController@process')) !!}
                        <input type="hidden" name="id" value="{{ $source->id }}">
                        <input type="hidden" name="source" value="{{ $source->hash }}">

                        @if ($source->source_id)
                        <input type="hidden" name="update" value="{{ $source->source_id }}">
                        @endif

                        <input type="hidden" name="entry_type" value="update">
                        <input type="hidden" name="selected_product" value="{{ $source->option1_title }}">

                        <button class="button btn-strike join-button track-link" type="submit" data-package="{{ $source->option1_title }}" data-link="{{ $source->option1_url }}" data-track-category="Join Now" data-track-action="Click" data-track-label="{{ $source->option1_title }}" data-track-value="{{ $source->option1_title }}">
                            <div class="btn-strike__inner">Join Now</div>
                            <div class="btn-strike__strike"></div>
                        </button>
                    {!! Form::close() !!}

                    <div class="fine-print fine-print__mobile">
                        <sup>*</sup>10 monthly payments.
                    </div>

                </div>

                @if($source->option2_price_month)
                <div class="product-offerings__offering">
                    <div class="product-offerings__offering__option"><span>{{($source->data_set == 'Unrenewed')?'Upgrade':'Option 2'}}</span></div>
                    <div class="product-offerings__offering__title">{{ $source->option2_title }}</div>
                    <div class="product-offerings__offering__offer__action">{{ $source->option2_description }}</div>
                    <div class="product-offerings__offering__offer__year">2017 Adult Membership just</div>
                    <div class="product-offerings__offering__offer__price">${{ rtrim(rtrim(number_format($source->option2_price_month, 2, ".", ""), '0'), '.') }}<sup>*</sup></div>
                    <div class="product-offerings__offering__offer__duration">Per month</div>

                    {!! Form::open(array('action' => 'FrontController@process')) !!}
                        <input type="hidden" name="id" value="{{ $source->id }}">
                        <input type="hidden" name="source" value="{{ $source->hash }}">

                        @if ($source->source_id)
                        <input type="hidden" name="update" value="{{ $source->source_id }}">
                        @endif

                        <input type="hidden" name="entry_type" value="update">
                        <input type="hidden" name="selected_product" value="{{ $source->option2_title }}">

                        <button class="button btn-strike join-button track-link" type="submit" data-package="{{ $source->option2_title }}" data-link="{{ $source->option2_url }}" data-track-category="Join Now" data-track-action="Click" data-track-label="{{ $source->option2_title }}" data-track-value="{{ $source->option2_title }}">
                            <div class="btn-strike__inner">Join Now</div>
                            <div class="btn-strike__strike"></div>
                        </button>
                    {!! Form::close() !!}

                    <div class="fine-print fine-print__mobile">
                        <sup>*</sup>10 monthly payments.
                    </div>
                </div>
                @else
                <div class="product-offerings__offering">
                    <div class="product-offerings__offering__option"><span>{{($source->data_set == 'Unrenewed')?'Other':'Option 2'}}</span></div>
                    <div class="product-offerings__offering__title">Browse Other Packages</div>
                    <div class="product-offerings__offering__offer__action">{{ $source->option2_description }}</div>
                    <div class="product-offerings__offering__offer__year"></div>
                    <div class="product-offerings__offering__offer__price"></div>
                    <div class="product-offerings__offering__offer__duration"></div>

                    {!! Form::open(array('action' => 'FrontController@process')) !!}
                        <input type="hidden" name="id" value="{{ $source->id }}">
                        <input type="hidden" name="source" value="{{ $source->hash }}">

                        @if ($source->source_id)
                        <input type="hidden" name="update" value="{{ $source->source_id }}">
                        @endif

                        <input type="hidden" name="entry_type" value="update">
                        <input type="hidden" name="selected_product" value="Other Packages">

                        <button class="button btn-strike button--red join-button track-link" type="submit" data-package="All Packages" data-link="{{ $source->option2_url }}" data-track-category="Join Now" data-track-action="Click" data-track-label="All Packages" data-track-value="All Packages">

                            <div class="btn-strike__inner">View</div>
                            <div class="btn-strike__strike"></div>

                        </button>
                    {!! Form::close() !!}

                     
                </div>

                @endif

                @if( $source->data_set == 'Unrenewed')
                    @if( $source->option3_url)
                    <div class="product-offerings__offering">
                        <div class="product-offerings__offering__option"><span>Other</span></div>
                        <div class="product-offerings__offering__title">Browse Other Packages</div>
                        <div class="product-offerings__offering__offer__action">{{ $source->option3_description }}</div>
                        <div class="product-offerings__offering__offer__year"></div>
                        <div class="product-offerings__offering__offer__price"></div>
                        <div class="product-offerings__offering__offer__duration"></div>

                        {!! Form::open(array('action' => 'FrontController@process')) !!}
                            <input type="hidden" name="id" value="{{ $source->id }}">
                            <input type="hidden" name="source" value="{{ $source->hash }}">

                            @if ($source->source_id)
                            <input type="hidden" name="update" value="{{ $source->source_id }}">
                            @endif

                            <input type="hidden" name="entry_type" value="update">
                            <input type="hidden" name="selected_product" value="Other Packages">

                            <button class="button btn-strike button--red join-button track-link" type="submit" data-package="All Packages" data-link="{{ $source->option3_url }}" data-track-category="Join Now" data-track-action="Click" data-track-label="All Packages" data-track-value="All Packages">

                                <div class="btn-strike__inner">View</div>
                                <div class="btn-strike__strike"></div>

                            </button>
                        {!! Form::close() !!}

                         
                    </div>
                    @endif
                @else

                    <div class="product-offerings__offering">
                        <div class="product-offerings__offering__option"><span>Option 3</span></div>
                        <div class="product-offerings__offering__title">{{ $source->option3_title }}</div>
                        <div class="product-offerings__offering__offer__action">{{ $source->option3_description }}</div>
                        <div class="product-offerings__offering__offer__year">2017 Adult Membership just</div>
                        <div class="product-offerings__offering__offer__price">${{ rtrim(rtrim(number_format($source->option3_price_month, 2, ".", ""), '0'), '.') }}<sup>*</sup></div>
                        <div class="product-offerings__offering__offer__duration">Per month</div>

                        {!! Form::open(array('action' => 'FrontController@process')) !!}
                            <input type="hidden" name="id" value="{{ $source->id }}">
                            <input type="hidden" name="source" value="{{ $source->hash }}">

                            @if ($source->source_id)
                            <input type="hidden" name="update" value="{{ $source->source_id }}">
                            @endif

                            <input type="hidden" name="entry_type" value="update">
                            <input type="hidden" name="selected_product" value="{{ $source->option3_title }}">

                            <button class="button btn-strike join-button track-link" type="submit" data-package="{{ $source->option3_title }}" data-link="{{ $source->option3_url }}" data-track-category="Join Now" data-track-action="Click" data-track-label="{{ $source->option3_title }}" data-track-value="{{ $source->option3_title }}">
                                <div class="btn-strike__inner">Join Now</div>
                                <div class="btn-strike__strike"></div>
                            </button>
                        {!! Form::close() !!}

                        <div class="fine-print fine-print__mobile">
                            <sup>*</sup>10 monthly payments.
                        </div>
                    </div>
                @endif

            </div>

            <div class="fine-print fine-print__desktop">
                <sup>*</sup>10 monthly payments.
            </div>

        </div>
    </div>

    @include('front.callouts')

@endsection
