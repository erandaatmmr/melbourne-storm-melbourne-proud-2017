@extends('../front')

@section('content')

    <div class="hero">

        <h1 class="hero__title">Found Bulldog</h1>

        <div class="hero__image">
            <img src="{{ asset('/images/member-found.jpg') }}" alt="You've Been Found">
        </div>

        <div class="hero__member-found">
            <h2 class="hero__member-found__title">You&rsquo;ve Been Found!</h2>
            <div class="hero__member-found__text">Thank you for submitting your details. A member of the Lost Dog Squad will be in touch with you shortly.</div>
            <script>
                $(document).ready(function() {
                    trackEvent({ category: 'Member', action: 'View', label: 'Found Page', value: {{ $source->id }} });
                });
            </script>
        </div>

    </div>

    <div class="member-poster-callout">
        <div class="member-poster-callout__image">
            <img src="{{ asset('/images/member-poster-callout.jpg') }}" alt="Create Your Poster">
        </div>
        <div class="member-poster-callout__text">
            <p>Create and share your own personalised poster to let everyone know you're found and committed to The Kennel in 2016. #bemorebulldog</p>

            <form class="create-poster__form form" id="createMemberPoster">

                <input type="hidden" name="id" value="{{ $source->id }}" id="createMemberPosterID">
                <input type="hidden" name="hash" value="{{ $source->hash }}" id="createMemberPosterHash">
                <input type="hidden" name="name" value="{{ $source->first_name }}" id="createMemberPosterName">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="createMemberPosterToken">

                <div class="create-poster__form__buttons" id="createMemberPosterInput">
                    <p>To create your poster, select your photo to upload:</p>
                    <input type="file" class="form__file track-link" name="files[]" id="createMemberPosterUpload" data-track-category="Member Poster" data-track-action="Create" data-track-label="Upload" data-track-value="{{ $source->id }}">
                    <label for="createMemberPosterUpload">Upload Photo</label>
                </div>

                <!-- <div class="create-poster__form__submit form__submit">
                    <button class="button button--red" type="submit" id="createMemberPosterSubmit">Create Poster</button>
                </div> -->

            </form>

        </div>
    </div>

    <div class="create-poster">

        <div class="create-poster__created" id="createMemberPosterCreated">

            <div class="create-poster__created__inner" id="createMemberPosterCreatedInner">

                <div class="create-poster__created__thumbnail">
                    <img src="" id="createMemberPosterCreatedThumb">
                </div>

                <div class="create-poster__created__text">
                    <p>All done! Your Lost Bulldog poster has been created. Select from the following options to send.</p>

                    <div class="create-poster__created__buttons">
                        <ul class="social-buttons">
                            <li><a href="#" class="social-buttons__button social-buttons__button__facebook track-link" target="_blank" id="createMemberPosterCreatedFBShare" data-track-category="Share" data-track-action="Facebook" data-track-label="Member Poster">Facebook</a></li>
                            <li><a href="#" class="social-buttons__button social-buttons__button__twitter track-link" target="_blank" id="createMemberPosterCreatedTwitterShare" data-track-category="Share" data-track-action="Twitter" data-track-label="Member Poster">Twitter</a></li>
                            <li><a href="#" class="social-buttons__button social-buttons__button__email send-member-poster track-link" data-track-category="Share" data-track-action="Email" data-track-label="Member Poster">Email</a></li>
                        </ul>
                        <a href="#" class="button button--red button--large track-link" id="createMemberPosterCreatedPDFPrint" data-track-category="Member Poster" data-track-action="Download" data-track-label="JPG">Download JPG</a><br>
                        <a href="#" class="button button--red button--large track-link" id="createMemberPosterCreatedPDFDownload" data-track-category="Member Poster" data-track-action="Download" data-track-label="PDF">Download PDF</a>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="modal modal--hidden" id="sendMemberPosterModal">
        <div class="modal__inner">
            <h3>Send to Friend</h3>

            <div id="sendMemberPosterModalBody">

            <p>Enter your friend's email and name to send them your poster:</p>

            <div class="modal__errors modal__errors--hidden" id="sendMemberPosterModalErrors">
                <div class="modal__errors__title">Please check the following errors:</div>
                <div class="errors"></div>
            </div>

            {!! Form::open(array('action' => 'FrontController@postSendFriendPoster', 'class' => 'form', 'id' => 'sendMemberPosterForm')) !!}

                <div class="create-poster__form__field form__field">
                    <input type="text" name="friend_name" placeholder="Friend's Name" class="form__input">
                </div>

                <div class="create-poster__form__field form__field">
                    <input type="text" name="email" placeholder="Friend's Email" class="form__input">
                </div>

                <div class="create-poster__form__submit form__submit">
                    <button class="button button--red" type="submit">Send Poster</button>
                </div>

                <input type="hidden" name="poster_hash" value="" id="createMemberPosterCreatedHash">

            {!! Form::close() !!}

            </div>

            <button class="modal__close">Close</button>

        </div>
    </div>

    @include('front.callouts')

@endsection
