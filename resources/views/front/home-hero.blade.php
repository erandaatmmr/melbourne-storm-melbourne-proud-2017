<div class="container">

    @if($video_url)
    <div class="hero">

        <h1 class="hero__title hero__title">Are you our Secret Weapon?</h1>

        <div class="hero__video">
            <div class="hero__video__inner">
                <div class="hero__video__player" id="memberVideoPlayer" data-id="SHARE">

                    <video id="memberVideo" class="video-js vjs-default-skin" controls preload="auto" width="640" height="264" poster="{{ asset('/images/video-poster.jpg') }}" data-setup='{}'>
                        <source src="{{ $video_url }}" type="video/mp4" />
                        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that supports HTML5 video</p>
                    </video>

                </div>
            </div>
        </div>

    </div>
    @else
    <div class="hero">

        <h1 class="hero__title hero__title">Are you our Secret Weapon?</h1>

        <div class="hero__video hero__video--home">
            <div class="hero__video__inner hero__video__home-wrap">
                <div class="hero__video__poster"></div>
                <div class="hero__video__player hero__video__player--hidden" id="dynamicVideoPlayer"></div>
                <div class="hero__video__home" id="dynamicVideoWrap">
                    <div class="hero__video__home__inner">
                        <div class="hero__video__home__title">Enter your name<br>and press play</div>
                        <form action="#" method="POST" class="hero__video__home__form form" id="dynamicVideoForm">

                            <div class="hero__video__home__form__field form__field">
                                <div class="form__field__skew">
                                    <input type="text" name="name_first" placeholder="Your first name here" class="form__input" id="dynamicVideoNameFrist">
                                </div>
                            </div>

                            <div class="hero__video__home__form__field form__field">
                                <div class="form__field__skew">
                                    <input type="text" name="name_last" placeholder="Your surname here" class="form__input" id="dynamicVideoNameLast">
                                </div>
                            </div>

                            <div class="hero__video__home__form__field form__field">
                                <div class="form__field__skew">
                                    <input type="text" name="email" placeholder="Your email here" class="form__input" id="dynamicVideoEmail">
                                </div>
                            </div>

                            <div class="custom-ckeck-wrapper">
                                <div class="custom-ckeck-box">
                                    <input type="checkbox" value="Subscribe me to Melbourne Storm newsletter" id="dynamicVideoRecieveEmails" name="dynamicVideoRecieveEmails">
                                    <label for="dynamicVideoRecieveEmails"></label>
                                </div>

                                <span class="custom-ckeck-label" for="dont_subscribe">Please tick if you wish to receive emails from Melbourne Storm.</span>
                            </div>

                            <div class="hero__video__home__form__buttons form__submit">
                                <button class="button button--gold" type="submit" id="dynamicVideoSubmit"><span>Play</span></button>
                            </div>

                        </form>
                    </div>
                    <div class='uil-rolling-css' style='transform:scale(0.45);'><div><div></div><div></div></div></div>
                </div>
            </div>
        </div>

    </div>
    @endif

    <div class="scroll">
		<a href="#" class="scroll__button"></a>
	</div>
</div>
