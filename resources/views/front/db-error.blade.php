@extends('../front')

@section('content')

@include('../includes.header-common')

<div class="container-fluid error-messages">
    <div class="container">
        <h2>A temporary error has occurred, please try again later. Sorry for the inconvenience.</h2>
    </div>
</div>
@endsection
