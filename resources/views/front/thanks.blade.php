@extends('../front')

@section('content')

<div class="container-fluid main-wrap">

    <div class="container">
    
    	<div class="row">
    
    		<div class="main col-xs-12">

                <div class="welcome">
                    <h1>Thank you for boosting your<br>membership package for Season 2016</h1>
                    <h2>Our membership team will process your request and make the necessary changes to your membership. If you have any queries please do not hesitate to contact us on <strong>1300 46 36 47</strong> or via email at <strong><a href="mailto:membership@westernbulldogs.com.au">membership@westernbulldogs.com.au</a></strong></h2>
                </div>

    		</div>

    	</div>

    </div>

</div>

@endsection
