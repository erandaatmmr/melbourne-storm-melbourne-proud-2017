
    @if(isset($posted) && ! $posted)

<div class="hexarrow">
    <div class="container">

        <div class="product-offerings product-offerings--home">

            <h3 class="product-offerings__current">It's easy and affordable to become a<br> member of the storm in 2017</h3>

            <div class="product-offerings__offering">

                <div class="product-offerings__offering__option"><span>Option 1</span></div>
                <div class="product-offerings__offering__title">4 game membership</div>
                <div class="product-offerings__offering__offer__action">General Admission access to 4 home games at AAMI Park plus a member scarf</div>
                <div class="product-offerings__offering__offer__year">2017 Adult Membership just</div>
                <div class="product-offerings__offering__offer__price">$9.50<sup>*</sup></div>
                <div class="product-offerings__offering__offer__duration">Per month</div>

                <a href="http://membership.melbournestorm.com.au/package/4-game-membership/" class="btn-strike track-link fb-track">
                    <div class="btn-strike__inner">Join Now</div>
                    <div class="btn-strike__strike"></div>
                </a>

                <div class="fine-print fine-print__mobile">
                    <sup>*</sup>10 monthly payments.
                </div>
            </div>

            <div class="product-offerings__offering">
                <div class="product-offerings__offering__option"><span>Option 2</span></div>
                <div class="product-offerings__offering__title">storm spirit</div>
                <div class="product-offerings__offering__offer__action">General Admission access to 11 home games, member scarf plus other benefits</div>
                <div class="product-offerings__offering__offer__year">2017 Adult Membership just</div>
                <div class="product-offerings__offering__offer__price">$21<sup>*</sup></div>
                <div class="product-offerings__offering__offer__duration">Per month</div>

                <a href="http://membership.melbournestorm.com.au/package/storm-spirit/" class="btn-strike track-link fb-track">
                    <div class="btn-strike__inner">Join Now</div>
                    <div class="btn-strike__strike"></div>
                </a>

                <div class="fine-print fine-print__mobile">
                    <sup>*</sup>10 monthly payments.
                </div>
            </div>

            <div class="product-offerings__offering">
                <div class="product-offerings__offering__option"><span>Option 3</span></div>
                <div class="product-offerings__offering__title">club house</div>
                <div class="product-offerings__offering__offer__action">Level 2 seating, the ability to watch the game behind glass, player interviews and much more</div>
                <div class="product-offerings__offering__offer__year">2017 Adult Membership just</div>
                <div class="product-offerings__offering__offer__price">$75<sup>*</sup></div>
                <div class="product-offerings__offering__offer__duration">Per month</div>
                
                <a href="http://membership.melbournestorm.com.au/package/club-house/" class="btn-strike track-link fb-track">
                    <div class="btn-strike__inner">Join Now</div>
                    <div class="btn-strike__strike"></div>
                </a>

                <div class="fine-print fine-print__mobile">
                    <sup>*</sup>10 monthly payments.
                </div>
            </div>

        </div>

        @endif

        <div class="fine-print fine-print__desktop">
            <sup>*</sup>10 monthly payments.
        </div>
        
    </div>
</div>