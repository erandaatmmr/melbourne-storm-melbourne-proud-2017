<div class="callouts-wrapper">

    <div class="container">

        <div class="callouts">

            <div class="callouts__callout callouts__callout__bottom-border">

                <div class="callouts__callout-one-third">
                    <h1 class="callouts__callout__title">More options?</h1>
                </div>

                <div class="callouts__callout-two-third">
                    <p  class="callouts__callout__sub-title">Want to view our full range of membership options? <a href="http://membership.melbournestorm.com.au/" target="_blank">Click here</a> to visit our website or call <a href="tel:1300786767">1300 STORMRLC (1300 786 767)</a> to speak to a member of our team.</p>
                </div>

            </div>

            <div class="callouts__callout">

                <div class="callouts__callout-half callouts__callout__right-border">
                    <h1 class="callouts__callout__title">Create your front page!</h1>

                    <div class="inline">
                        <div class="inline__image" >
                            <img src="{{ asset('/images/create-poster.jpg') }}" alt="Create a Poster" width="170">
                        </div>

                        <div class="inline__text">
                            <p>Let Melbourne know who Storm’s new Secret Weapon is by creating your own front page! Then share it on Facebook and Twitter.</p>

                            <a href="{{ url((isset($source->acct_id) ? 'create-poster/' . $source->hash : 'create-poster')) }}" class="button button--gold button--large button--icon"><span>Create</span></a>
                        </div>
                    </div>

                </div>

                <div class="callouts__callout-half">
                    <h1 class="callouts__callout__title">Share your video!</h1>

                    <div class="inline">

                        <div class="inline__image" >
                            <img src="{{ asset('/images/send-to-friend.jpg') }}" alt="Send to Friend"  width="200">
                        </div>

                        <div class="inline__text">
                            <p>Share your personalised video and let your mates know who Storm’s new Secret Weapon is!</p>
                            <ul class="social-buttons">
                                <li><a id="videoShareLinkFB" href="{{ isset($fb_share_url) ? $fb_share_url : '#' }}" class="social-buttons__button social-buttons__button__facebook track-link videoShareLinkFB" data-track-category="Share" data-track-action="Facebook" data-track-label="Homepage" target="_blank">Facebook</a></li>
                                <li><a href="https://twitter.com/intent/tweet?status={{ urlencode('Let people know about storm’s Secret Weapon #purplepride @storm http://bit.ly/2l0qIHR') }}" class="social-buttons__button social-buttons__button__twitter track-link" data-track-category="Share" data-track-action="Twitter" data-track-label="Homepage" target="_blank">Twitter</a></li>
                                
                                <li><a href="{{ !empty($download_url) ? $download_url : '#' }}" class="social-buttons__button icon-download_icon track-link videoDownloadLink" data-track-category="Share" data-track-action="Download" id="videoDownloadLink" data-track-label="Homepage" download>Download</a></li>
                                
                            </ul>
                        </div>

                    </div>
                </div>

            </div>


        </div>

    </div>

    <div class="page-bg page-bg--bottom"></div>
</div>

<script>
window.fbShareURLTemplate = 'https://www.facebook.com/dialog/feed?app_id={{ config('datacleanse.facebook_app_id') }}&link={{ url('/share/video/dynamic/') }}/{LINK}';
</script>
