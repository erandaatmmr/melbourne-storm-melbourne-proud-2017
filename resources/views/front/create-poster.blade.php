@extends('../front')

@section('content')


    <div class="container">

        <div class="hero">

            <h1 class="hero__title hero__title--two-line">Let people know about <br>storm’s Secret weapon</h1>

        <div class="hero__video">
            <div class="poster-form">
                <div class="poster-form__col">

                    <div class="poster-form__col-half">
                        <img class="poster-form__col__image" src="{{ asset('/images/create-poster-hero.jpg') }}" alt="Create Poster">

                    </div>

                    <div class="poster-form__col-half poster-form__col-top-border">
                        <h2 class="poster-form__col__title">We’ve found our Secret Weapon!<br><span>make your front page and let people know</span></h2>
                        <h2 class="poster-form__col__title-gold">Use our poster creator and let your mates know who Storm's new Secret Weapon is.</h2>

                        <form class="create-poster__form form" id="createFriendPoster">

                            <div class="form__field">
                                <div class="form__field__skew">
                                    <input type="text" name="name_first" placeholder="Your name here" class="form__input" id="createFriendPosterNameFirst" value="">
                                </div>
                            </div>

                            <div class="form__field">
                                <div class="form__field__skew">
                                    <input type="text" name="email" placeholder="Your email here" class="form__input" id="createFriendPosterEmail" value="">
                                </div>
                            </div>

                            <div class="custom-ckeck-wrapper">
                                <div class="custom-ckeck-box">
                                    <input type="checkbox" value="true" id="custom-Ckeck" name="recieve_emails">
                                    <label for="custom-Ckeck"></label>
                                </div>

                                <span class="custom-ckeck-label" for="dont_subscribe">Please tick if you wish to receive emails from Melbourne Storm.</span>
                            </div>

                            <div class="create-poster__form__buttons button button--gold" id="createFriendPosterInput">
                                <input type="file" class="form__file track-link" name="files[]" id="createFriendPosterUpload" data-track-category="Member Poster" data-track-action="Create" data-track-label="Upload"  accept="image/jpg,image/jpeg" >
                                <label for="createFriendPosterUpload" id="createFriendPosterUploadLabel">Upload Photo to create poster</label>
                            </div>

                            <!-- <button class="form__button__create button button--gold" type="submit" id="createFriendPosterSubmit" >Create</button> -->

                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="createMemberPosterToken">
                            <input type="hidden" name="poster_hash" value="" id="createFriendPosterCreatedHash">
                            <input type="hidden" name="hash" value="{{ isset($hash) ? $hash : '' }}" id="createFriendPosterCreatedMemberHash">

                        </form>


                    </div>

                </div>
            </div>
        </div>

        </div>
    </div>

    <div class="hexarrow">
        <div class="container">

            <div class="create-poster__created" id="createFriendPosterCreated">

                <div class="create-poster__created__inner" id="createFriendPosterCreatedInner">

                    <div class="create-poster__created__thumbnail">
                        <img src="" id="createFriendPosterCreatedThumb">
                    </div>

                    <div class="create-poster__created__text">
                        <h2>All done!</h2>
                        <p>Your Secret Weapon poster has been created. Select from the following options to download or share it.</p>

                        <div class="create-poster__created__buttons">
                            <ul class="social-buttons">
                                <li><a href="#" class="social-buttons__button social-buttons__button__facebook track-link" target="_blank" id="createFriendPosterCreatedFBShare" data-track-category="Share" data-track-action="Facebook" data-track-label="Friend Poster">Facebook</a></li>
                                <li><a href="#" class="social-buttons__button social-buttons__button__twitter track-link" target="_blank" id="createFriendPosterCreatedTwitterShare" data-track-category="Share" data-track-action="Twitter" data-track-label="Friend Poster">Twitter</a></li>
<<<<<<< HEAD
                               <!-- <li><a href="#" class="social-buttons__button social-buttons__button__email send-friend-poster track-link" data-track-category="Share" data-track-action="Email" data-track-label="Friend Poster">Email</a></li>-->
=======
                                <!-- <li><a href="#" class="social-buttons__button social-buttons__button__email send-friend-poster track-link" data-track-category="Share" data-track-action="Email" data-track-label="Friend Poster">Email</a></li> -->
>>>>>>> 64c52b96a7e7076bf4d890528e9621c60ceddd74
                            </ul>
                            <a href="#" class="button  button--purple button--large track-link" id="createFriendPosterCreatedPDFPrint" data-track-category="Friend Poster" data-track-action="Download" data-track-label="JPG"><span>Download JPG</span></a>
                            <a href="#" class="button button--purple button--large track-link" id="createFriendPosterCreatedPDFDownload" data-track-category="Friend Poster" data-track-action="Download" data-track-label="PDF"><span>Download PDF</span></a>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>


    <div class="modal modal--hidden" id="sendFriendPosterModal">
        <div class="modal__inner">
            <h3>Send to Friend</h3>

            <div id="sendFriendPosterModalBody">

            <p>Enter your name and your friend's email to send them your poster:</p>

            <div class="modal__errors modal__errors--hidden" id="sendFriendPosterModalErrors">
                <div class="modal__errors__title">Please check the following errors:</div>
                <div class="errors"></div>
            </div>

            {!! Form::open(array('action' => 'FrontController@postSendFriendPoster', 'class' => 'form', 'id' => 'sendFriendPosterForm')) !!}

                <div class="create-poster__form__field form__field">
                    <input type="text" name="sender_name" placeholder="Your Name" class="form__input" id="sendFriendPosterSender">
                </div>

                <div class="create-poster__form__field form__field">
                    <input type="text" name="sender_email" placeholder="Your Email" class="form__input" id="sendFriendPosterSenderEmail">
                </div>

                <div class="create-poster__form__field form__field">
                    <input type="text" name="email" placeholder="Friend's Email" class="form__input" id="sendFriendPosterEmail">
                </div>

                <!-- <div class="create-poster__form__field form__field">
                    <textarea name="message" placeholder="Message" class="form__input" id="sendFriendPosterMessage"></textarea>
                </div> -->

                <div class="create-poster__form__submit form__submit">
                    <button class="button button--gold" type="submit" id="sendFriendPosterSubmit">Send Poster</button>
                </div>

                <input type="hidden" name="name" value="" id="createFriendPosterCreatedEmail">

            {!! Form::close() !!}

            </div>

            <button class="modal__close">Close</button>

        </div>
    </div>






@endsection
