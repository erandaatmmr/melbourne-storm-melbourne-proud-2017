
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-JVPF67P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <header class="header">

       <!-- <div class="page-bg page-bg--top"></div>-->

        <div class="container header__inner">

            <div class="header__links">

            <div class="header__logo">
                <a href="{{ url('/') }}">Melbourne Storm - 2017 Memberships</a>
            </div>

            <ul>
                <li class="header__links__share-link">
                    <a class="header__links__link nav-share-video" href="#">Share The Video</a>
                    <div class="header__links__share">
                        <div class="arrow"></div>
                        <ul class="social-buttons">
                            <li><a href="{{ isset($fb_share_url) ? $fb_share_url : '#' }}" class="social-buttons__button social-buttons__button__facebook track-link videoShareLinkFB" data-track-category="Share" data-track-action="Facebook" data-track-label="Homepage" target="_blank">Facebook</a></li>
                            <li><a href="https://twitter.com/intent/tweet?status={{ urlencode('Let people know about Storm’s Secret Weapon #purplepride @storm  http://bit.ly/2l0qIHR') }}" class="social-buttons__button social-buttons__button__twitter track-link" data-track-category="Share" data-track-action="Twitter" data-track-label="Homepage" target="_blank">Twitter</a></li>
                            <li><a href="{{ !empty($download_url) ? $download_url : '#' }}" class="social-buttons__button icon-download_icon track-link videoDownloadLink" data-track-category="Share" data-track-action="Download" data-track-label="Homepage" download>Download</a></li>
                        </ul>
                    </div>
                </li>
                <li><a class="header__links__link" href="{{ url((isset($source->acct_id) ? 'create-poster/' . $source->hash : 'create-poster')) }}">CreatE posteR</a></li>
                <li><a class="header__links__link" href="{{ url('contact') }}">Contact</a></li>
            </ul>

            <button class="header__nav-toggle" id="toggleNav">
                <span></span>
                <span></span>
                <span></span>
            </button>

            </div>

        </div>

    </header>

    <div class="responsive-nav-wrap">
        <nav class="responsive-nav">

            <div style="width: 100%; text-align: right; padding-right: 20px;">
                <button class="header__nav-toggle" id="toggleNavClose">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>

            <div class="responsive-nav__share">

                <p>Share The Video</p>
                <ul class="social-buttons">
                    <li><a href="{{ isset($fb_share_url) ? $fb_share_url : '#' }}" class="social-buttons__button social-buttons__button__facebook track-link videoShareLinkFB" data-track-category="Share" data-track-action="Facebook" data-track-label="Homepage" target="_blank">Facebook</a></li>
                    <li><a href="https://twitter.com/intent/tweet?status={{ urlencode('Let people know about Storm’s Secret Weapon #purplepride @storm  http://bit.ly/2l0qIHR') }}" class="social-buttons__button social-buttons__button__twitter track-link" data-track-category="Share" data-track-action="Twitter" data-track-label="Homepage" target="_blank">Twitter</a></li>
                    <li><a href="{{ !empty($download_url) ? $download_url : '#' }}" class="social-buttons__button icon-download_icon track-link videoDownloadLink" data-track-category="Share" data-track-action="Download" id="videoDownloadLink" data-track-label="Homepage" download>Download</a></li>
                </ul>

            </div>

            <ul class="responsive-nav__ul">
                <li><a href="{{ url((isset($source->acct_id) ? 'create-poster/' . $source->hash : 'create-poster')) }}">CreatE posteR</a></li>
                <li><a href="{{ url('contact') }}">Contact</a></li>
            </ul>

        </nav>
        <div class="responsive-nav-dismiss" id="responsiveNavDissmiss"></div>
    </div>
