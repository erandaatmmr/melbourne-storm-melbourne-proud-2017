
    <footer class="footer">

        <div class="footer__sponsors">

            <div class="footer__inner">
            
                <div class="footer__logo">
                    <a href="http://membership.melbournestorm.com.au" target="_blank">Melbourne Storm - 2017 Memberships</a>
                </div>

                <div class="footer__sponsors__title">2017 Sponsors</div>

                <ul class="footer__sponsors__logos">


                    <li class="footer__sponsors__sponsor footer__sponsors__sponsor--primary">
                        <a href="http://www.crownresorts.com.au/" target="_blank" title="Crown Resorts">
                            <img src="{{ asset('/images/sponsors/sponsor-crown.png') }}" alt="Crown Resorts Logo">
                        </a>
                    </li>
                    <li class="footer__sponsors__sponsor ">
                        <a href="http://www.suzuki.com.au/" target="_blank" title="Suzuki">
                            <img src="{{ asset('/images/sponsors/sponsor-suzuki.png') }}" alt="Suzuki Logo">
                        </a>
                    </li>
                    <li class="footer__sponsors__sponsor ">
                        <a href="https://tigerair.com.au/" target="_blank" title="Tiger Air">
                            <img src="{{ asset('/images/sponsors/sponsor-tiger.png') }}" alt="Tiger Air Logo">
                        </a>
                    </li>
                    <li class="footer__sponsors__sponsor ">
                        <a href="http://hostplus.com.au/" target="_blank" title="Hostplus">
                            <img src="{{ asset('/images/sponsors/sponsor-hostplus.png') }}" alt="Hostplus Logo">
                        </a>
                    </li>
                    <li class="footer__sponsors__sponsor ">
                        <a href="http://www.iscsport.com/" target="_blank" title="ISC">
                            <img src="{{ asset('/images/sponsors/ISC-1.png') }}" alt="ISC Logo">
                        </a>
                    </li>
                    <li class="footer__sponsors__sponsor ">
                        <a href="http://whitehorsetruckandbus.com.au/" target="_blank" title="Fuso">
                            <img src="{{ asset('/images/sponsors/White.png') }}" alt="Fuso Logo">
                        </a>
                    </li>
                    
                </ul>

            </div>

        </div>

        <div class="footer__bottom">
            <div class="footer__inner">
                <span>© {{date("Y")}} Melbourne Storm.</span>

                <ul class="nav-footer-nav">
                    <li class="menu-item"><a target="_blank" href="https://www.melbournestorm.com.au/about/privacy-policy.html">Privacy Policy</a></li>

                    <li class="menu-item"><a target="_blank" href="http://membership.melbournestorm.com.au/terms-conditions/">Terms &amp; Conditions</a></li>

                    <li class="menu-item"><a target="_blank" href="https://www.melbournestorm.com.au/about/privacy-policy.html">Code of Conduct</a></li>
                </ul>

                <div class="footer__inner__by"><a href="http://mmr.com.au" target="_blank">Created by MMR</a></div>
            </div>
        </div>

    </footer>

    <div class="modal modal--hidden" id="requestCallModal">
        <div class="modal__inner">
            <h3>Request a Call</h3>

            <div id="requestCallModalBody">

            <p>Please enter your phone number and a preferred time to call:</p>

            <div class="modal__errors modal__errors--hidden" id="requestCallModalErrors">
                <div class="modal__errors__title">Please check the following errors:</div>
                <div class="errors"></div>
            </div>

            {!! Form::open(array('action' => 'FrontController@postRequestCall', 'class' => 'form', 'id' => 'requestCallForm')) !!}

                <div class="form__field">
                    <input type="text" name="phone" placeholder="Phone *" class="form__input" id="requestCallPhone">
                </div>

                <div class="form__field">
                    <div class="form__select">
                        <select name="time" id="requestCallTime">
                            <option value="">Select time</option>
                            <option value="Morning">Morning</option>
                            <option value="Afternoon">Afternoon</option>
                            <option value="Evening">Evening</option>
                        </select>
                    </div>
                </div>

                <div class="form__submit">
                    <button class="button button--red" type="submit" id="requestCallSubmit">Submit</button>
                </div>

            {!! Form::close() !!}

            </div>

            <button class="modal__close">Close</button>

        </div>
    </div>

    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script src="{{ asset('js/front.js') }}"></script>
