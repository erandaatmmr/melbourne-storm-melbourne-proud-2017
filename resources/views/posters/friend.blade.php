@extends('../poster')

@section('content')

    <div class="poster poster--friend">
        <div class="poster--friend__title"><span>STORM’S</span><br>SECRET WEAPON</div>
        <div class="poster--friend__name {{ strlen($name) > 16 ? 'poster--friend__name--long' : '' }}">{{ $name }},</div>
        <div class="poster--friend__under-name">tipped to be<br>unveiled for 2017... </div>
        <div class="poster--friend__under-name-small">REPORT &amp; PICTURES PAGE 7, SPORT</div>
        <div class="poster--friend__photo" style="background-image: url({{ $photo }});"></div>
        <div class="poster--friend__caption">{{ $name }} is yet to confirm for 2017</div>
    </div>

@endsection