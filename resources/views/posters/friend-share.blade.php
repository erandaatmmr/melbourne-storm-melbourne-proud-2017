@extends('../poster')

@section('content')

    <div class="poster poster--friend poster--share">
        <div class="poster--friend__title">HAVE YOU SEEN A<br>LOST BULLDOG?</div>
        <div class="poster--friend__name">{{ $name }}</div>
        <div class="poster--friend__message">MISSING FOR TOO LONG</div>
    </div>

@endsection
