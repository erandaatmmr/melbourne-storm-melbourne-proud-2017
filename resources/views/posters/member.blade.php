@extends('../poster')

@section('content')

    <div class="poster poster--member">
        <div class="poster--member__title">Found Bulldog!</div>
        <div class="poster--member__photo" style="background-image: url({{ $photo }});"></div>
        <div class="poster--member__name">{{ $name }}</div>
        <div class="poster--member__message">IS COMMITTED TO THE KENNEL IN 2016<br>#BEMOREBULLDOG</div>
    </div>

@endsection
