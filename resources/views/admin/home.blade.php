@extends('../admin')

@section('content')
<div class="container-fluid">
	<div class="row">

		<div class="col-xs-6">

            <h2>Column Statistics</h2>

            <table class="table table-bordered">

                @foreach ($column_stats as $stat => $records)

                <tr class="active">
                    <th colspan="2" class="active">{{ $stat }}</th>
                </tr>

                @foreach ($records as $record)

                <tr>
                    <?php 

                        if('Country' === $stat) {
                            $record['value'] = ('' !== $record['value'] ? $record['value'] : 'AUSTRALIA');
                        }
                    ?>

                    <td>{{ $record['value'] }}</td>
                    <td style="text-align: right">{{ $record['count'] }}</td>
                </tr>

                @endforeach

                @endforeach

            </table>

		</div>

		<div class="col-xs-6">

            <h2>Data</h2>

            <table class="table table-bordered">
                <tr>
                    <th>Total Records</th>
                    <td style="text-align: right">{{ $source['total'] }}</td>
                </tr>
                <tr>
                    <th>Total Confirmed</th>
                    <td style="text-align: right">{{ $source['confirmed'] }} ({{ intval(($source['confirmed'] / $source['total']) * 100) }}%)</td>
                </tr>
                <tr>
                    <th>Confirmed</th>
                    <td style="text-align: right">{{ $confirmed }} ({{ intval(($confirmed / $source['total']) * 100) }}%)</td>
                </tr>
                <tr>
                    <th>Updated</th>
                    <td style="text-align: right">{{ $updated }} ({{ intval(($updated / $source['total']) * 100) }}%)</td>
                </tr>
                <tr>
                    <th>Unconfirmed</th>
                    <td style="text-align: right">{{ $source['unconfirmed'] }} ({{ intval(($source['unconfirmed'] / $source['total']) * 100) }}%)</td>
                </tr>
            </table>

            <h2>Batches &amp; Mailing Lists</h2>

            <table class="table table-bordered">
                <tr>
                    <th>&nbsp;</th>
                    <th style="text-align: center;">Total</th>
                    <th style="text-align: center;" colspan="2">Confirmed</th>
                    <th style="text-align: center;" colspan="2">Unconfirmed</th>
                </tr>
                @foreach ($batches as $batch)

                <tr class="active">
                    <th>{{ $batch['batch'] }}</th>
                    <td style="text-align: right; width: 125px;"><strong>{{ $batch['total'] }}</strong></td>
                    <td style="text-align: right; width: 80px;"><strong>{{ $batch['confirmed'] }}</strong></td>
                    <td style="text-align: right; width: 45px;"><span class="label label-default">{{ $batch['confirmed_pc'] }}%</span></td>
                    <td style="text-align: right; width: 80px;"><strong>{{ $batch['unconfirmed'] }}</strong></td>
                    <td style="text-align: right; width: 45px;"><span class="label label-default">{{ $batch['unconfirmed_pc'] }}%</span></td>
                </tr>

                @endforeach
            </table>

		</div>

	</div>
</div>
@endsection
