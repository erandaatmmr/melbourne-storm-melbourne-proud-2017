<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AFL Auskick Datacleanse</title>

	<link href="{{ asset('/css/admin.css') }}" rel="stylesheet">

</head>
<body>


    <table class="table table-bordered table-striped">

        <thead>
            <tr>
                @foreach ($columns as $col)
                <th>{{ $col }}</th>
                @endforeach
            </tr>
        </thead>

        <tbody>
            @foreach ($rows as $row)
            <tr>
                @foreach ($row as $key => $val)
                <td>{{ $val }}</td>
                @endforeach
            </tr>
            @endforeach
        </tbody>

    </table>

</body>
</html>
