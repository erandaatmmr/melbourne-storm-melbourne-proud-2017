@extends('../admin')

@section('content')
<div class="container">

    <div class="row">

        <div class="col-xs-4">

            <div class="panel panel-default">
                <div class="panel-heading">Batch</div>
                <div class="panel-body">

                    <div class="btn-group btn-group-justified">
                        <a href="?batch=all" class="btn btn-default {{ (! $batch) ? 'active' : '' }}" role="button">All</a>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle {{ ($batch) ? 'active' : '' }}" data-toggle="dropdown" aria-expanded="false">
                                {{ ($batch) ? $batch : 'Select Batch' }} <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                @foreach (App\Source::batches() as $batch_item)
                                <li class="{{ ($batch === $batch_item->import_batch) ? 'active' : '' }}"><a href="?batch={{ $batch_item->import_batch }}">{{ $batch_item->import_batch }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-xs-4">

            <div class="panel panel-default">
                <div class="panel-heading">Show Only</div>
                <div class="panel-body">

                    <div class="btn-group btn-group-justified">
                        <a href="?show=all" class="btn btn-default {{ (! $show) ? 'active' : '' }}" role="button">All</a>
                        <a href="?show=confirmed" class="btn btn-default {{ ($show == 'confirmed') ? 'active' : '' }}">Confirmed</a>
                        <a href="?show=unconfirmed" class="btn btn-default {{ ($show == 'unconfirmed') ? 'active' : '' }}">Unconfirmed</a>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-xs-4">

            <div class="panel panel-default">
                <div class="panel-heading">Export</div>
                <div class="panel-body">

                    <div class="btn-group btn-group-justified">
                        <a href="{{ url('admin/export-preview?model=source&data=source_ids') }}" class="btn btn-default" role="button" target="_blank">Preview</a>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">Download <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('admin/export?model=source&data=source_ids&format=csv') }}">CSV</a></li>
                                <li><a href="{{ url('admin/export?model=source&data=source_ids&format=xlsx') }}">Excel</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

	<div class="row">
		<div class="col-xs-12">

            <table class="table table-striped" id="sourceTable">

                <thead>
                    <tr>
                        <th data-name="id">ID</th>
                        <th data-name="member_id">Member ID</th>
                        <th data-name="club_member_id">Club ID</th>
                        <th data-name="member_status">Member Status</th>
                        <th data-name="first_name">First Name</th>
                        <th data-name="last_name">Last Name</th>
                        <th data-name="email_address">Email</th>
                        <th data-name="output_id">Confirmed</th>
                    </tr>
                </thead>

            </table>
            
            
            <script>
            
                $(document).ready( function () {

                    window.dataTable = $('#sourceTable').DataTable({
                        stateSave: true,
                        searching: true,
                        serverSide: true,
                        pageLength: 25,
                        lengthMenu: [ 10, 25, 50, 75, 100, 250, 500 ],
                        ajax: {
                            url : "{{ url('datatable/source') }}",
                            type : "GET"
                        }
                    });

/*
                    $('#exportPreview').on('click', function() {
                        var _this = $(this);
                        var href = "{{ url('admin/source-export-preview') }}?ids=" + getDataIDs().join(',');
                        _this.attr('href', href);
                    });
*/

                });

                function getDataIDs() {
                    return dataTable.ajax.json().ids;
                }
                
            </script>

		</div>
	</div>
</div>
@endsection
