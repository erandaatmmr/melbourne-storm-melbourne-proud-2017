<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<style>
body{
	-webkit-text-size-adjust:none;
    margin: 0;
    padding: 0;
}
.ReadMsgBody { width: 100%;}
.ExternalClass {width: 100%;}
.ExternalClass * {line-height: 100%}
.span1{
	font-family:Helvetica, sans-serif;
	font-size:29px;
	font-weight:bold;
	color:#e16800;
}



.span9{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;

	color:#000001;
}
.span9 a{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	text-decoration:none;
	color:#000001;
}

.preheader {
    display: none !important;
}

</style>
</head>

<body><span class="preheader">Hey {{ $request->friend_name }}, I'm back at the Kennel for season 2016, check out your personalised video below and come join me.</span>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#f7f7f7">
    	<tr>
        	<td>
            	<table width="800" cellpadding="0" cellspacing="0" border="0" align="center">
                	<tr>
                    	<td width="800" valign="top" colspan="1">
                    		<img src="{{ asset('images/emails/send-member-poster/email_02.jpg') }}" width="800" height="189" border="0" style="display:block;" >
                    	</td>
                    </tr>
                	<tr>
                    	<td width="800" valign="top" bgcolor="#ffffff" style="padding:0px;">
                        	<table width="800" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td width="130" valign="top" colspan="1">

                                        <img src="{{ asset('images/emails/send-member-poster/email_07.jpg') }}" width="130" height="676" border="0" style="display:block;"  />
                                    </td>
                                    <td width="540" valign="top" colspan="1">
                                    	 <table width="540" cellpadding="0" cellspacing="0" border="0" align="center">
                                         	<tr>
                                                <td width="540" height="402" valign="middle" colspan="3" align="center"   >
                                                	<table width="540" cellpadding="0" cellspacing="0" border="0" align="center">
                                                        <tr>
                                                        	 <td width="128" >

                                                            </td>
                                                            <td width="284"  valign="top"   >
                                                                <img src="{{ $poster_data->thumb_url }}" width="284" height="402" border="0" style="display:block;"  />
                                                            </td>
                                                            <td width="128">

                                                            </td>
                                                        </tr>
                                                     </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="540" height="85" valign="middle" colspan="3" align="center" style="font-family:Arial, Helvetica, sans-serif;
	font-size:52px;
	text-decoration:none;
    font-weight:bold;
	color:#c50b2f; text-transform:uppercase;">
                                                     {{ $poster->name }}

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="156" valign="top" colspan="1" align="left" style="padding-bottom:0px;">
                                                   <img src="{{ asset('images/emails/send-member-poster/email_12.jpg') }}" width="156" height="28" border="0" style="display:block;"  />

                                                </td>
                                                <td width="228" valign="top" colspan="1" align="center" style="font-family:Arial, Helvetica, sans-serif;
	font-size:15px;
	text-decoration:none;
	color:#000001; text-transform:uppercase; padding-bottom:0px;">
                                                    IS BACK AT THE KENNEL

                                                </td>
                                                <td width="156" valign="top" colspan="1" align="right"  style="padding-bottom:0px;">

                                                    <img src="{{ asset('images/emails/send-member-poster/email_14.jpg') }}" width="156" height="28" border="0" style="display:block;"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="540" height="69" valign="middle" colspan="3" align="center" style="font-family:Arial, Helvetica, sans-serif;
	font-size:14px;
	text-decoration:none;
    font-weight:bold;
	color:#000001; ">
                                                     Hey {{ $request->friend_name }}, I'm back at the Kennel for season 2016, check out your personalised video below and come join me.

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="540"  valign="top" colspan="3" align="center"   >
                                                	<table width="540" cellpadding="0" cellspacing="0" border="0" align="center">
                                                        <tr>
                                                        	 <td width="199" >

                                                            </td>
                                                            <td width="143"  valign="top"   >
                                                                <a href="{{ url() }}"><img src="{{ asset('images/emails/send-member-poster/email_17.jpg') }}" width="143" height="34" border="0" style="display:block;"  /></a>
                                                            </td>
                                                            <td width="198">

                                                            </td>
                                                        </tr>
                                                     </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="540" height="58" valign="top" colspan="3" align="center"   >

                                                	 <img src="{{ asset('images/emails/send-member-poster/email_19.jpg') }}" width="540" height="58" border="0" style="display:block;"  />
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                    <td width="130" valign="top" colspan="1">

                                        <img src="{{ asset('images/emails/send-member-poster/email_10.jpg') }}" width="130" height="676" border="0" style="display:block;"  />
                                    </td>
                                </tr>
                            </table>
                        </td>
                     </tr>
                      <tr>
                    	<td width="800" valign="top" colspan="1">
                    		<img src="{{ asset('images/emails/send-member-poster/email_20.jpg') }}" width="800" height="310" border="0" style="display:block;" >
                    	</td>
                    </tr>
                    <tr>
                    	<td width="800" align="center" valign="top" colspan="1" style="font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	text-decoration:none;
	color:#ffffff; line-height:17px; padding-bottom:10px;">
<span class="span9">This email was sent to <a href="mailto:[email]" style="color:#000001; text-decoration:none;">[email]</a> by the Western Bulldogs Football Club<br>
<a href="http://www.westernbulldogs.com.au/club/privacy-policy/privacy-statement">Privacy Policy</a>   |   <a href="mailto:membership@westernbulldogs.com.au">Contact Sender</a>
&copy; 2015-2016 Western Bulldogs, 417 Barkly St, Footscray West 3012. All Rights Reserved.</span>
                    	</td>
                    </tr>
                    <tr>
                    	<td width="800" valign="top"  style="padding:0px 0 45px 0;">
                        	<table width="800" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td width="274" valign="top" colspan="1">

                                        <img src="{{ asset('images/emails/send-member-poster/img_10.jpg') }}" width="274" height="88" border="0" style="display:block;"  />
                                    </td>
                                    <td width="73" valign="top" colspan="1">

                                        <a href="http://www.westernbulldogs.com.au/"><img src="{{ asset('images/emails/send-member-poster/img_11.jpg') }}" width="73" height="88" border="0" style="display:block;"  /></a>
                                    </td>
                                    <td width="66" valign="top" colspan="1">

                                        <a href="http://www.missionmenus.com/au/"><img src="{{ asset('images/emails/send-member-poster/img_12.jpg') }}" width="66" height="88" border="0" style="display:block;"  /></a>
                                    </td>
                                    <td width="102" valign="top" colspan="1">

                                        <a href="http://geelongtravel.com.au/"><img src="{{ asset('images/emails/send-member-poster/img_13.jpg') }}" width="102" height="88" border="0" style="display:block;"  /></a>
                                    </td>
                                    <td width="285" valign="top" colspan="1">

                                        <img src="{{ asset('images/emails/send-member-poster/img_14.jpg') }}" width="285" height="88" border="0" style="display:block;"  />
                                    </td>
                                </tr>
                            </table>
                        </td>
                     </tr>
                 </table>
            </td>
       	</tr>
    </table>
    <div style="display:none; white-space:nowrap; font:15px courier; line-height:0;">
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</div>
</body>
</html>
