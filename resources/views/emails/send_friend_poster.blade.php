<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
body{
	-webkit-text-size-adjust:none;
	margin:0;
	padding:0;
}
.ReadMsgBody { width: 100%;}
.ExternalClass {width: 100%;}
.ExternalClass * {line-height: 100%}
.span1{
	font-family:Helvetica, sans-serif;
	font-size:29px;
	font-weight:bold;
	color:#e16600;
}
img {
  border: 0 !important;
  outline: none !important;
  }
  p {
  Margin: 0px !important;
  padding: 0px !important;
  }
  table {
  border-collapse: collapse;
  mso-table-lspace: 0px;
  mso-table-rspace: 0px;
  }
  td {
  border-collapse: collapse;
  mso-line-height-rule: exactly;
  }
.span9{
	font-family:Arial, Helvetica, sans-serif;
	font-size:11px;
	color:#ffffff;
}
.span9 a{
	font-family:Arial, Helvetica, sans-serif;
	font-size:11px;
	color:#ffffff;
}
.span10{
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;
	color:#626262;
}
.span10 a{
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;
	color:#19191e;
}
.preheader {
    display: none !important;
}
@media only screen and (max-width:480px) {
      table[class=em_main_table]{
      width: 100% !important;
      }
      table[class=em_wrapper]{
      width: 100% !important;
      }
	  td[class=em_hide], td[class=em_hide2], br[class=em_hide] {
      display: none !important;
      }
	  img[class=em_img] {
      width: 100% !important;
      height:auto !important;
      }
	  table.wrapper2{
      padding-bottom: 30px!important;
      }
	  table.wrapper2 td{
      text-align: center!important;
      }
	  table.wrapper2 td img{
       display: inline-block!important;
      }
}
</style>
</head>



	
<body><span class="preheader">{{ $request->sender_name }} sent you a personalised poster</span>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
    	<tr>
        	<td>
            	<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="em_main_table">
               		<tr>
                    	<td valign="middle" height="40" colspan="1" align="center" style="font-family:Arial, Helvetica, sans-serif;
	font-size:10px; color:#626262; ">
							
                    	</td>
                    </tr>
                	<tr>
                    	<td valign="top" colspan="1">
							<a href="[update_url]"><img src="{{ asset('images/emails/send-friend-poster/img_03.jpg') }}" width="600" height="623" border="0" style="display:block;" class="em_img"></a>
                    	</td>
                    </tr>
                	<tr>
                    	<td valign="top" bgcolor="#ebebeb" style="padding:0px 0 5px 0;">
                        	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="em_wrapper">
                                <tr>
                                    <td valign="top" colspan="1" align="center" style="font-family:Arial, Helvetica, sans-serif;
	font-size:42px; color:#040200; font-weight:bold; text-transform: uppercase; ">
                                       {{ $poster->friend_name }},
                                    </td>
                                </tr>
                            </table>
                        </td>
                     </tr>
                     <tr>
                    	<td valign="top" colspan="1">
                    		<img src="{{ asset('images/emails/send-friend-poster/img_06.jpg') }}" width="600" height="50" border="0" style="display:block;" class="em_img">
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" bgcolor="#ebebeb" style="padding:0px;">
                        	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="em_wrapper">
                                <tr>
                                    <td valign="top" colspan="1" align="center" style="font-family:Arial, Helvetica, sans-serif;
	font-size:16px; color:#040200; font-weight:bold; line-height:22px; ">
                                        Click on the video above to see who is<br>
 Melbourne Storm’s Secret Weapon for season 2017.
                                    </td>
                                </tr>
                            </table>
                        </td>
                     </tr>
                     <tr>
                    	<td valign="top" colspan="1">
                    		<img src="{{ asset('images/emails/send-friend-poster/img_08.jpg') }}" width="600" height="84" border="0" style="display:block;" class="em_img">
                    	</td>
                    </tr>
                    <tr>
                    	<td valign="top" bgcolor="#ebebeb" style="padding:0px;" bordercolor="#fff">
                        	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="em_wrapper">
                                <tr>
                                   <td align="center" valign="top">
                                   		<table width="323" cellpadding="0" cellspacing="0" border="0" align="left" class="em_wrapper">
                                   			<tr>
												<td valign="top" colspan="1" align="center" bgcolor="#ffffff">
													<a href="https://www.crownmelbourne.com.au/"><img src="{{ asset('images/emails/send-friend-poster/img_09.jpg') }}" width="113" height="98" border="0" style="display:block;"></a>
												</td>
												<td valign="top" colspan="1" align="center" bgcolor="#ffffff">
													<a href="http://www.suzuki.com.au/"><img src="{{ asset('images/emails/send-friend-poster/img_10.jpg') }}" width="122" height="98" border="0" style="display:block;"></a>
												</td>
												<td valign="top" colspan="1" align="center" bgcolor="#ffffff">
													<a href="https://tigerair.com.au/"><img src="{{ asset('images/emails/send-friend-poster/img_11.jpg') }}" width="88" height="98" border="0" style="display:block;"></a>
												</td>
											</tr>
									   </table>
                                  	   <!--[if gte mso 9]>
                                      </td>
                                      <td valign="top">
                                        <![endif]-->
                                   	   <table width="277" cellpadding="0" cellspacing="0" border="0" align="center" class="em_wrapper">
                                   			<tr>
												<td valign="top" colspan="1" align="center" bgcolor="#ffffff">
													<a href="https://hostplus.com.au/"><img src="{{ asset('images/emails/send-friend-poster/img_12.jpg') }}" width="120" height="98" border="0" style="display:block;"></a>
												</td>
												<td valign="top" colspan="1" align="center" bgcolor="#ffffff">
													<a href="http://whitehorsetruckandbus.com.au/"><img src="{{ asset('images/emails/send-friend-poster/img_13.jpg') }}" width="79" height="98" border="0" style="display:block;"></a>
												</td>
												<td valign="top" colspan="1" align="center" bgcolor="#ffffff">
													<a href="http://www.iscsport.com/"><img src="{{ asset('images/emails/send-friend-poster/img_14.jpg') }}" width="78" height="98" border="0" style="display:block;"></a>
												</td>
										   </tr>
									   </table>
									</td>
                                </tr>
                            </table>
                        </td>
                     </tr>
                      <tr>
                    	<td valign="top" bgcolor="#000001" style="padding:20px 0;">
                        	<table width="100%" class="em_wrapper" cellpadding="0" cellspacing="0" border="0" align="right">
                                <tr>
                                    <td valign="top" colspan="1" align="center" style="font-family:Arial, Helvetica, sans-serif;
	font-size:11px; color:#040200;  line-height:18px; ">
                                      <span class="span9">
										  <a href="http://membership.melbournestorm.com.au/terms-conditions/">Terms &amp; Conditions</a>   |   <a href="https://www.melbournestorm.com.au/about/privacy-policy.html">Privacy Policy</a>    </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                     </tr>
                     <tr>
                     	<td bgcolor="#000001"><img src="https://ad.doubleclick.net/ddm/activity/src=6013727;type=melproud;cat=melbo0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" alt=""/>
						 </td>
					</tr>
                 </table>
            </td>
       	</tr>
    </table>
    <div style="display:none; white-space:nowrap; font:15px courier; line-height:0;">
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</div>
</body>
</html>
