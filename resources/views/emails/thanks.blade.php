<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Found Bulldog!</title>
<style>
html, body{
    margin: 0;
    padding: 0;
	-webkit-text-size-adjust:none;
}
.ReadMsgBody { width: 100%;}
.ExternalClass {width: 100%;}
.ExternalClass * {line-height: 100%}
.span1{
	font-family:Helvetica, sans-serif;
	font-size:29px;
	font-weight:bold;
	color:#e16800;
}



.span9{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;

	color:#000001;
}
.span9 a{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	text-decoration:none;
	color:#000001;
}
.preheader {
    display: none !important;
}

</style>
</head>

<body><span class="preheader">{{ $name }}, thank you for your interest in {{ $membership }}</span>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#f7f7f7">
    	<tr>
        	<td>
            	<table width="800" cellpadding="0" cellspacing="0" border="0" align="center">
                	<tr>
                    	<td width="800" valign="top" colspan="1">
                    		<img src="{{ asset('images/emails/thanks/pic_02.jpg') }}" width="800" height="44" border="0" style="display:block;" >
                    	</td>
                    </tr>
                    <tr>
                    	<td width="800" valign="top" colspan="1">
                    		<img src="{{ asset('images/emails/thanks/pic_04.jpg') }}" width="800" height="514" border="0" style="display:block;" >
                    	</td>
                    </tr>
                	<tr>
                    	<td width="800" valign="top" bgcolor="#ffffff" style="padding:0px;">
                        	<table width="800" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td width="120" valign="top" colspan="1">

                                        <img src="{{ asset('images/emails/thanks/pic_05.jpg') }}" width="120" height="259" border="0" style="display:block;"  />
                                    </td>
                                    <td width="560" valign="top" colspan="1">
                                    	 <table width="560" cellpadding="0" cellspacing="0" border="0" align="center">

                                            <tr>
                                                <td width="560" valign="middle" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:24px; text-decoration:none;font-weight:bold;color:#c50b2f; line-height:31px;">
                                                     {{ $name }}, thank you for your interest in <br>
                                                     {{ $membership }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="560" height="45" valign="top"  align="center" style="
	color:#0039a6; padding-top:5px;">
                                                    ____________________
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="560" valign="middle" align="center" style="font-family:Arial, Helvetica, sans-serif;
	font-size:15px;
	text-decoration:none;
    font-weight:normal;
	color:#000001; line-height:21px;">
                                                     If you have any questions about membership or would like to discuss more<br> options that are suitable for you this season, please do not hesitate to contact us<br> on <span style="color:#0039a6; font-weight:bold;">1300 46 36 47</span> or on email at <a href="mailto:membership@westernbulldogs.com.au"  style="color:#0039a6; font-weight:bold; text-decoration:none;">membership@westernbulldogs.com.au</a><br><br>

We hope to see you at the game in 2016!<br>

Go Dogs, <br>
<span style="color:#0039a6; font-weight:bold;">The Lost Bulldogs Squad</span><br>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="120" valign="top" colspan="1">

                                        <img src="{{ asset('images/emails/thanks/pic_07.jpg') }}" width="120" height="259" border="0" style="display:block;"  />
                                    </td>
                                </tr>
                            </table>
                        </td>
                     </tr>
                      <tr>
                    	<td width="800" valign="top" colspan="1">
                    		<img src="{{ asset('images/emails/thanks/pic_08.jpg') }}" width="800" height="325" border="0" style="display:block;" >
                    	</td>
                    </tr>
                    <tr>
                    	<td width="800" align="center" valign="top" colspan="1" style="font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	text-decoration:none;
	color:#ffffff; line-height:17px; padding-bottom:10px;">
                    		<span class="span9">This email was sent to <a href="mailto:{{ $email }}" style="color:#000001; text-decoration:none;">{{ $email }}</a> by Western Bulldogs Football Club<br>
<a href="http://lostbulldog.com.au/privacy-policy">Privacy Policy</a>   |   <a href="mailto:membership@westernbulldogs.com.au">Contact Sender</a>
&copy; 2015-2016 Western Bulldogs, 417 Barkly St, Footscray West 3012. All Rights Reserved.</span>
                    	</td>
                    </tr>
                    <tr>
                    	<td width="800" valign="top"  style="padding:0px 0 45px 0;">
                        	<table width="800" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td width="274" valign="top" colspan="1">

                                        <img src="{{ asset('images/emails/thanks/img_10.jpg') }}" width="274" height="88" border="0" style="display:block;"  />
                                    </td>
                                    <td width="73" valign="top" colspan="1">

                                        <a href="http://www.westernbulldogs.com.au/"><img src="{{ asset('images/emails/thanks/img_11.jpg') }}" width="73" height="88" border="0" style="display:block;"  /></a>
                                    </td>
                                    <td width="66" valign="top" colspan="1">

                                        <a href="http://www.missionmenus.com/au/"><img src="{{ asset('images/emails/thanks/img_12.jpg') }}" width="66" height="88" border="0" style="display:block;"  /></a>
                                    </td>
                                    <td width="102" valign="top" colspan="1">

                                        <a href="http://geelongtravel.com.au/"><img src="{{ asset('images/emails/thanks/img_13.jpg') }}" width="102" height="88" border="0" style="display:block;"  /></a>
                                    </td>
                                    <td width="285" valign="top" colspan="1">

                                        <img src="{{ asset('images/emails/thanks/img_14.jpg') }}" width="285" height="88" border="0" style="display:block;"  />
                                    </td>
                                </tr>
                            </table>
                        </td>
                     </tr>
                 </table>
            </td>
       	</tr>
    </table>
</body>
</html>
