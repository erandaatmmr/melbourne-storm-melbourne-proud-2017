<p><strong>A new submission from melbourneproud.com.au enquiry form has been received:</strong></p>
<p><strong>Name:</strong> {{ $request->name }}</p>
<p><strong>Phone:</strong> {{ $request->phone }}</p>
<p><strong>Email:</strong> <a href="mailto:{{ $request->email }}">{{ $request->email }}</a></p>
<p><strong>Message:</strong></p>
<p>{{ $request->message }}</p>
