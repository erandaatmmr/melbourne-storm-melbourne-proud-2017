<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

    <head>

        @include('includes.header-common')

        @if(isset($fb_meta))
        @foreach($fb_meta as $property => $content)
        <meta property="{{ $property }}" content="{{ $content }}" />
        @endforeach
        @endif

    </head>

    <body class="route-{{ Route::currentRouteName() }}">

        @include('includes.page-header')

        <section class="main">
            @yield('content')
        </section>

        @include('includes.footer-common')

    </body>

</html>
