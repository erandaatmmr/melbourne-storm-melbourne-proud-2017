<!DOCTYPE html>
<html lang="en">

    <head>
    	<meta charset="utf-8">
    	<title>{{ Config::get('datacleanse.title') }}</title>
    	<link href="{{ asset('/css/poster.css') }}" rel="stylesheet">
    </head>

    <body>
        @yield('content')
    </body>

</html>
