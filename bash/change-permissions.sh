#!/bin/bash

LOCAL_PATH='/var/www/vhosts/datacleanse.com.au/httpdocs/cricket-australia/acf-2015'

# set owner/group
chown -R www-data:www-data "$LOCAL_PATH"

# WP Dirs and Files
chmod -R 755 "$LOCAL_PATH"