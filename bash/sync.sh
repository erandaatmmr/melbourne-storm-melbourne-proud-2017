#!/bin/bash

REMOTE_SERVER='mmrstudio@52.64.102.169'
LOCAL_PATH='/Users/chameleon/Sites/ca-datacleanse-2015/'
REMOTE_PATH='/var/www/vhosts/datacleanse.com.au/httpdocs/cricket-australia/acf-2015/'

clear

printf "Server: $REMOTE_SERVER\n"
printf "Sync from: $LOCAL_PATH\n"
printf "Sync to:   $REMOTE_PATH\n\n"

printf "Sync files? y/n: "
read SYNC_FILES

printf "\n"

if [ $SYNC_FILES == 'y' ]; then

  printf "Dry run? y/n: "
  read DRY_RUN

  if [ $DRY_RUN == 'y' ]; then

    rsync -azhvn \
        --stats \
        --delete \
        --exclude '.env' \
        --exclude '.git' \
        --exclude '.sass-cache' \
        --exclude '.DS_Store' \
        --exclude '.env.example' \
        --exclude '.gitattributes' \
        --exclude '.gitignore' \
        --exclude 'bower_components' \
        --exclude 'database' \
        --exclude 'node_modules' \
        --exclude 'tests' \
        --exclude 'app-config.json' \
        --exclude 'artisan' \
        --exclude 'bower.json' \
        --exclude 'composer.json' \
        --exclude 'composer.lock' \
        --exclude 'gulpfile.js' \
        --exclude 'package.json' \
        --exclude 'phpspec.yml' \
        --exclude 'phpunit.xml' \
        --exclude 'readme.md' \
        --exclude 'server.php' \
        --exclude 'logs' \
        --exclude '._*' \
        "$LOCAL_PATH" "${REMOTE_SERVER}":"$REMOTE_PATH"

  fi

  printf "\nStart file sync? y/n: "
  read START_SYNC

  if [ $START_SYNC == 'y' ]; then

    rsync -chavzP --rsync-path="sudo rsync" \
        --stats \
        --delete \
        --exclude '.env' \
        --exclude '.git' \
        --exclude '.sass-cache' \
        --exclude '.DS_Store' \
        --exclude '.env.example' \
        --exclude '.gitattributes' \
        --exclude '.gitignore' \
        --exclude 'bower_components' \
        --exclude 'node_modules' \
        --exclude 'app-config.json' \
        --exclude 'bower.json' \
        --exclude 'gulpfile.js' \
        --exclude 'package.json' \
        --exclude 'logs' \
        --exclude '._*' \
        "$LOCAL_PATH" "${REMOTE_SERVER}":"$REMOTE_PATH"

    #printf "\nChanging permissions...\n"
    #ssh "${REMOTE_SERVER}" "/bin/bash ${REMOTE_PATH}bash/change-permissions.sh"

    printf "DONE!\n\n"

  fi

fi

printf "\n"
exit 1
