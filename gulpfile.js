var config = require('./app-config.json');

var gulp = require('gulp');
var sass = require('gulp-sass');
var fileinclude = require('gulp-file-include');
var uglify = require('gulp-uglify');
var modernizr = require('gulp-modernizr');
var replace = require('gulp-batch-replace');
var symlink = require('gulp-sym')
var rename = require("gulp-rename");
var shell = require('gulp-shell');
var run = require('gulp-run');
var cssmin = require('gulp-cssmin');
var autoprefixer = require('gulp-autoprefixer');

// WATCH

gulp.task('watch', [], function () {
    gulp.watch('resources/assets/scss/front/**/*.scss', ['cssfront']);
    gulp.watch('resources/assets/scss/poster/**/*.scss', ['cssposter']);
    gulp.watch('resources/assets/scss/admin/**/*.scss', ['cssadmin']);
    gulp.watch('resources/assets/js/front/**/*.js', ['jsfront']);
    gulp.watch('resources/assets/js/admin/**/*.js', ['jsadmin']);
});

// TASKS

gulp.task('cssfront', function() {

    gulp.src('resources/assets/scss/front/front.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/css/'));

});

gulp.task('cssadmin', function() {

    gulp.src('resources/assets/scss/admin/admin.scss')
        .pipe(compass(config.compass_admin))
        .pipe(gulp.dest('public/css/'));

});

gulp.task('cssposter', function() {

    gulp.src('resources/assets/scss/poster/poster.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/css/'));

});

gulp.task('jsfront', function() {

    gulp.src(['resources/assets/js/front/front.js'])
        .pipe(fileinclude({ prefix: ' @@', basepath: '@file' }))
        .pipe(gulp.dest('public/js/'));

});

gulp.task('jsadmin', function() {

    gulp.src(['resources/assets/js/admin/admin.js'])
        .pipe(fileinclude({ prefix: ' @@', basepath: '@file' }))
        .pipe(gulp.dest('public/js/'));

});

gulp.task('copybower', function() {

    // jquery
    gulp.src('bower_components/jquery/dist/jquery.js')
        .pipe(gulp.dest('public/js/vendor'));

    // modernizr
    gulp.src('bower_components/modernizr/modernizr.js')
        .pipe(gulp.dest('public/js/vendor'));

});

// gulp.task('publish', function() {
//
//     // prepare modernizr
//     gulp.src('public/js/*.js')
//         .pipe(modernizr('modernizr.js', config.modernizr))
//         .pipe(uglify())
//         .pipe(gulp.dest("public/js/vendor/"))
//
//     // uglify js
//     gulp.src(['public/js/front.js']).pipe(uglify()).pipe(gulp.dest('public/js'));
//     gulp.src(['public/js/admin.js']).pipe(uglify()).pipe(gulp.dest('public/js'));
//     gulp.src(['public/js/vendor/jquery.js']).pipe(uglify()).pipe(gulp.dest('public/js/vendor'));
//
// });

    gulp.task('publish', function() {

        // autoprefix & minify
        gulp.src('public/css/front.css')
            .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
            .pipe(cssmin())
            .pipe(gulp.dest('public/css/'));

        // uglify js
        gulp.src(['public/js/front.js']).pipe(uglify()).pipe(gulp.dest('public/js/'));

    });
