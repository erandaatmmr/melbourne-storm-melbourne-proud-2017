<?php namespace App\Exceptions;

use Request;
use Mail;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
    	if ($e instanceof \PDOException)
    	{
            Mail::send('emails.db-error', ['exception' => $e], function($message)
            {
                $message->subject('DB Error - '.config('datacleanse.title'));
                $message->from('web@mmr.com.au', 'MMR Server');
                $message->to('web@mmr.com.au', 'MMR Dev Team');
                
            });

        	return redirect()->action('FrontController@dberror');
        }

		return parent::render($request, $e);
	}	
}
