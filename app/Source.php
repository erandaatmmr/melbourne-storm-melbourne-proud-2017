<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Source extends Model {

	protected $table = 'source';

    protected $fillable = ['acct_id','name_prefix1','name_first','name','birthdate',
                            'gender','street_addr_1', 'street_addr_2', 'city', 'state', 'zip', 'country', 'phone_mobile', 'email_addr',
                            'import_batch','hash'];

    public static function hash($hash)
    {
        return Source::where('hash', '=', $hash)->first();
    }

    public function confirmed()
    {
        return ($this->output_id == '0') ? false : true;
    }

    public static function sourceCount()
    {
        return [
            'total' => Source::count(),
            'confirmed' => Source::confirmedCount(),
            'unconfirmed' => Source::unconfirmedCount(),
        ];
    }

    public static function confirmedCount()
    {
        return Source::where('output_id', '<>', '0')->count();
    }

    public static function unconfirmedCount()
    {
        return Source::where('output_id', '=', '0')->count();
    }

    public static function batches()
    {
        return Source::select('import_batch')->groupBy('import_batch')->get();
    }

    public static function batchesCount($mailing_lists=false)
    {
        $batches = [];

        foreach(Source::batches() as $batch)
        {
            $batch_arr = Source::batchCount($batch->import_batch);
            $batch_arr['mailing_lists'] = false;

            if($mailing_lists)
            {
                $batch_arr['mailing_lists'] = Source::mailingListsCount($batch->import_batch);
            }

            $batches[] = $batch_arr;
        }

        return $batches;

    }

    public static function batchCount($batch)
    {
        $total = Source::where('import_batch', '=', $batch)->count();
        $confirmed = Source::batchConfirmedCount($batch);
        $unconfirmed = Source::batchUnconfirmedCount($batch);

        return [
            'batch' => $batch,
            'total' => $total,
            'confirmed' => $confirmed,
            'confirmed_pc' => number_format(round(($confirmed/$total) * 100, 1), 1),
            'unconfirmed' => $unconfirmed,
            'unconfirmed_pc' => number_format(round(($unconfirmed/$total) * 100, 1), 1),
        ];
    }

    public static function mailingLists($batch=false)
    {
        if($batch)
        {
            return Source::select('mailing_list')->where('import_batch', '=', $batch)->groupBy('mailing_list')->get();
        }
        else
        {
            return Source::select('mailing_list')->groupBy('mailing_list')->get();
        }
    }

    public static function mailingListsCount($batch)
    {
        $mailing_lists = [];

        foreach(Source::mailingLists($batch) as $list)
        {
            $mailing_lists[] = Source::mailingListCount($list->mailing_list);
        }

        return $mailing_lists;

    }

    public static function mailingListCount($mailing_list)
    {
        $total = Source::where('mailing_list', '=', $mailing_list)->count();
        $confirmed = Source::mailingListConfirmedCount($mailing_list);
        $unconfirmed = Source::mailingListUnconfirmedCount($mailing_list);

        return [
            'mailing_list' => $mailing_list,
            'total' => $total,
            'confirmed' => $confirmed,
            'confirmed_pc' => number_format(round(($confirmed/$total) * 100, 1), 1),
            'unconfirmed' => $unconfirmed,
            'unconfirmed_pc' => number_format(round(($unconfirmed/$total) * 100, 1), 1),
        ];
    }

    public static function batchConfirmedCount($batch)
    {
        return Source::where('output_id', '<>', '0')->where('import_batch', '=', $batch)->count();
    }

    public static function batchUnconfirmedCount($batch)
    {
        return Source::where('output_id', '=', '0')->where('import_batch', '=', $batch)->count();
    }

    public static function mailingListConfirmedCount($mailing_list)
    {
        return Source::where('output_id', '<>', '0')->where('mailing_list', '=', $mailing_list)->count();
    }

    public static function mailingListUnconfirmedCount($mailing_list)
    {
        return Source::where('output_id', '=', '0')->where('mailing_list', '=', $mailing_list)->count();
    }

	public static function columns($except=[]) {
        return array_except(\Schema::getColumnListing('source'), $except);
    }

}
