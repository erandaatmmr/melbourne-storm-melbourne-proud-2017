<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	'admin' => 'AdminController',
]);


//Route::get('member/found/{hash}', ['as' => 'found', 'uses' => 'FrontController@getFound']);
Route::get('member/{hash}', ['as' => 'form', 'uses' => 'FrontController@getUpdate']);
Route::post('member', ['as' => 'processForm', 'uses' => 'FrontController@process']);

Route::get('poster/member/{hash}', ['as' => 'posterMember', 'uses' => 'PosterController@member']);
//Route::get('poster/member/share/{hash}', ['as' => 'posterMemberShare', 'uses' => 'PosterController@memberShare']);
Route::get('share/{hash}', ['as' => 'home', 'uses' => 'FrontController@getHome']);
Route::get('share/video/{hash}/{video_data?}', ['as' => 'sharevideo', 'uses' => 'FrontController@getShareVideo']);

Route::get('poster/member/photo/{hash}', ['as' => 'posterMemberPhoto', 'uses' => 'PosterController@memberPhoto']);
Route::get('poster/friend/share/{name}', ['as' => 'posterFriendShare', 'uses' => 'PosterController@friendShare']);
Route::get('poster/friend/{type}', ['as' => 'posterFriend', 'uses' => 'PosterController@friend']);

Route::post('poster/upload', ['as' => 'posterUpload', 'uses' => 'PosterController@uploadMemberPhoto']);

Route::get('create-poster/{hash?}', ['as' => 'posterCreate', 'uses' => 'FrontController@getCreatePoster']);

Route::get('video/url', ['as' => 'videoURL', 'uses' => 'FrontController@getVideoURL']);

Route::get('contact', ['as' => 'getTheKennel', 'uses' => 'FrontController@getContact']);
Route::post('contact', ['as' => 'postTheKennel', 'uses' => 'FrontController@postContact']);

Route::get('/', ['as' => 'home', 'uses' => 'FrontController@getHome']);
Route::post('request-call', ['as' => 'postRequestCall', 'uses' => 'FrontController@postRequestCall']);
Route::post('send-friend-poster', ['as' => 'postSendFriendPoster', 'uses' => 'FrontController@postSendFriendPoster']);
Route::post('send-member-poster', ['as' => 'postSendMemberPoster', 'uses' => 'FrontController@postSendMemberPoster']);

Route::get('member/update/{hash?}', function ($hash=false) {
	return redirect('member/'.$hash);
});

// Route::get('member/confirm/{hash}', 'FrontController@getConfirm');
// Route::get('member/update/{hash?}', 'FrontController@getUpdate
// Route::get('member/confirmed', 'FrontController@getConfirmed');
// Route::get('member/link-error', 'FrontController@getLinkError');
// Route::get('member/terms', 'FrontController@getTerms');
//
// Route::get('error', 'FrontController@dberror');
//
//
// Route::get('/', function () {
//     return redirect('member/update');
// });
//
// Route::get('datatable/source', 'DatatableController@source');
// Route::get('datatable/output', 'DatatableController@output');
//
// Route::get('pw/{pw}', function($pw) {
//     return \Hash::make($pw);
// });


// Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['administrator']], function()
// {
//     Route::get('admin', 'AdminController@index');
//     Route::get('admin/source', 'AdminController@source');
//     Route::get('admin/data/source', 'AdminController@datatableSource');
// });
