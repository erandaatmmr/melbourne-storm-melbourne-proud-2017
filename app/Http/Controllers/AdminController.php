<?php namespace App\Http\Controllers;

use Session;
use Request;
use Schema;
use \Datatable;
use App\Source;
use App\Output;
use Excel;

class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        $view_data = [
            'source' => Source::sourceCount(),
            'confirmed' => Output::where('status', '=', 'confirm')->count(),
            'updated' => Output::where('status', '=', 'update')->count(),
            'batches' => Source::batchesCount(true),
            'column_stats' => Output::columnStats([
                /*'member_status' => 'Member Status',
                'favourite_player' => 'Favourite Player',*/
            ]),
        ];

        //dd($view_data);

		return view('admin/home', $view_data);
	}

	public function getSource()
	{
    	// view batch
    	$batch = Request::get('batch', false);

        if($batch)
        {
            if($batch != 'all')
            {
                Session::put('source_batch', $batch);
            }
            else
            {
                Session::forget('source_batch');
            }

            return redirect('admin/source');
        }
        else
        {
            $batch = Session::get('source_batch', false);
        }


    	// view confirmed/unconfirmed
    	$show = Request::get('show', false);

        if($show)
        {
            if($show != 'all')
            {
                Session::put('source_show', $show);
            }
            else
            {
                Session::forget('source_show');
            }

            return redirect('admin/source');
        }
        else
        {
            $show = Session::get('source_show', false);
        }


		return view('admin/source', [
            'batch' => $batch,
            'show' => $show
        ]);
	}

	public function getOutput()
	{
    	// view batch
    	$batch = Request::get('batch', false);

        if($batch)
        {
            if($batch != 'all')
            {
                Session::put('output_batch', $batch);
            }
            else
            {
                Session::forget('output_batch');
            }

            return redirect('admin/output');
        }
        else
        {
            $batch = Session::get('output_batch', false);
        }

		return view('admin/output', [
            'batch' => $batch
        ]);
	}

	public function getExportPreview()
	{

        ini_set('memory_limit','1024M');

        //$ids = Request::get('ids', false);
        //$ids = ($ids) ? explode(',', $ids) : [];

        $ids = Session::get(Request::get('data', 'null'), []);
        $model = Request::get('model', false);
        //print_r($ids); exit;

        // get columns
        $columns = Schema::getColumnListing($model);

        // get rows
        if($model === 'source')
        {
            $rows = Source::whereIn('id', $ids)->get();
        }
        elseif($model === 'output')
        {
            $rows = Output::whereIn('id', $ids)->get();
        }

        //print_r($rows->toArray()); exit;

		return view('admin/export-preview', [
            'columns' => $columns,
            'rows' => $rows->toArray(),
        ]);

	}

	public function getExport()
	{

        ini_set('memory_limit','1024M');

        $format = Request::get('format', 'csv');
        $ids = Session::get(Request::get('data', 'null'), []);
        $model = Request::get('model', 'null');
        //print_r($ids); exit;

        // get columns
        $columns = Schema::getColumnListing($model);

        // get rows
        if($model === 'source')
        {
            $rows = Source::whereIn('id', $ids)->get();
        }
        elseif($model === 'output')
        {
            $rows = Output::whereIn('id', $ids)->get();
        }

        Excel::create($model, function($excel) use ($rows) {

            $excel->sheet('Data', function($sheet) use ($rows) {
                $sheet->fromArray($rows->toArray());
            });
        
        })->download($format);


	}

}
