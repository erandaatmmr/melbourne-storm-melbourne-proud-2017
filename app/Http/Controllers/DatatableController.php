<?php namespace App\Http\Controllers;

use DB;
use Session;
use Request;
use \Datatable;
use App\Source;
use App\Output;

class DatatableController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function source()
    {
        ini_set('memory_limit','1024M');

        $request = Request::all();
        $response = ['draw' => (int) $request['draw']];

        //print_r($request);// exit;

        // prepare columns
        $exclude_columns = []; $columns = [];
        foreach($request['columns'] as $column)
        {
            if(! in_array($column['name'], $exclude_columns)) $columns[] = $column['name'];
        }

        //print_r($columns);

        // start query
        $get_data = Source::query();

        // select columns
        $get_data->select($columns);

        // get by batch
        $batch = Session::get('source_batch', false);
        if($batch) $get_data->where('import_batch', '=', $batch);

        // get by show confirmed/unconfirmed
        $show = Session::get('source_show', false);
        if($show === 'confirmed') $get_data->where('output_id', '>', '0');
        if($show === 'unconfirmed') $get_data->where('output_id', '=', '0');

        // get total records
        $total_records = $get_data->count();
        $filtered_records = $total_records;


        // sorting
        $sorting = [];
        foreach($request['order'] as $sort) $sorting[ $request['columns'][$sort['column']]['name'] ] = $sort['dir'];
        foreach($sorting as $col => $dir) $get_data->orderBy($col, $dir);
        //print_r($sorting);

        // searching
        $searching = [];
        if(strlen($request['search']['value']) > 0)
        {

            $get_data->where(function($query) use ($columns, $searching, $request) {

                foreach($columns as $col)
                {
                    if($col !== 'id') $searching[$col] = $request['search']['value'];
                }
    
                foreach($searching as $col => $val) $query->orWhere($col, 'LIKE', "%$val%");

            });

            $filtered_records = $get_data->count();

        }
        //print_r($searching);

        $get_ids = $get_data->get();

        $ids = array_fetch($get_ids->toArray(), 'id');
        //dd($ids);

        Session::put('source_ids', $ids);

        // limit
        $get_data->skip($request['start']);
        $get_data->take($request['length']);

        //print_r($get_data->toSql());

        // retrieve data
        $data = $get_data->get();

        $response['recordsTotal'] = $total_records;
        $response['recordsFiltered'] = $filtered_records;
        $response['ids'] = $ids;

        //print_r($data->toArray());

        // prepare data
        $response['data'] = [];

        foreach($data as $record)
        {
            $record = $record->toArray();

            // replace confirmed column with HTML
            $record['output_id'] = ($record['output_id'] > 0) ? 'Yes' : '-';


            $response['data'][] = array_flatten($record);
        }

        //print_r($response);

        return response()->json($response);

    }

    public function output()
    {
        $request = Request::all();
        $response = ['draw' => (int) $request['draw']];

        //print_r($request);// exit;

        // prepare columns
        $exclude_columns = []; $columns = [];
        foreach($request['columns'] as $column)
        {
            if(! in_array($column['name'], $exclude_columns)) $columns[] = $column['name'];
        }

        //print_r($columns);

        // start query
        $get_data = Output::query();

        // select columns
        $get_data->select($columns);

        // get by batch
        $batch = Session::get('output_batch', false);
        if($batch) $get_data->where('import_batch', '=', $batch);

        // get total records
        $total_records = $get_data->count();
        $filtered_records = $total_records;


        // sorting
        $sorting = [];
        foreach($request['order'] as $sort) $sorting[ $request['columns'][$sort['column']]['name'] ] = $sort['dir'];
        foreach($sorting as $col => $dir) $get_data->orderBy($col, $dir);
        //print_r($sorting);

        // searching (all columns)
        if(strlen($request['search']['value']) > 0)
        {
            $searching = [];

            $get_data->where(function($query) use ($columns, $searching, $request) {

                foreach($columns as $col)
                {
                    if($col !== 'id') $searching[$col] = $request['search']['value'];
                }
    
                foreach($searching as $col => $val) $query->orWhere($col, 'LIKE', "%$val%");

            });

            $filtered_records = $get_data->count();

        }

        // searching (individual columns)
        $search_cols = [];
        foreach($request['columns'] as $column)
        {

            if(strlen($column['search']['value']) > 0)
            {
                $search_cols[$column['name']] = [
                    'regex' => $column['search']['regex'],
                    'term' => $column['search']['value'],
                ];
            }

        }

        //print_r($search_cols); exit;

        if(count($search_cols) > 0)
        {

            foreach($search_cols as $col => $search)
            {
                $term = $search['term'];
                
                if($search['regex'])
                {
                    $get_data->where($col, 'REGEXP', $term);
                }
                else
                {
                    $get_data->where($col, 'LIKE', "%$term%");
                }
            }

            $filtered_records = $get_data->count();

        }

        $get_ids = $get_data->get();
        $ids = array_fetch($get_ids->toArray(), 'id');

        Session::put('output_ids', $ids);

        // limit
        $get_data->skip($request['start']);
        $get_data->take($request['length']);

        //print_r($get_data->toSql());

        // retrieve data
        $data = $get_data->get();

        $response['recordsTotal'] = $total_records;
        $response['recordsFiltered'] = $filtered_records;
        $response['ids'] = $ids;

        //print_r($data->toArray());

        // prepare data
        $response['data'] = [];

        foreach($data as $record)
        {
            $record = $record->toArray();

            $response['data'][] = array_flatten($record);
        }

        //print_r($response);

        return response()->json($response);

    }

}
