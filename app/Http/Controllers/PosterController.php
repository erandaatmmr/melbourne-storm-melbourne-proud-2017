<?php namespace App\Http\Controllers;

use App\Source;
use App\Output;
use App\Poster;
use App\Http\Requests\ProcessRequest;

use Illuminate\Http\Request as Request;
use Input;
use HTML;
use Form;
use Mail;
use Validator;
use Log;
use Route;
use File;

use mikehaertl\wkhtmlto\Pdf as WkPDF;
use mikehaertl\wkhtmlto\Image;
use Snipe\BanBuilder\CensorWords;

class PosterController extends Controller {

	public function __construct()
	{
		//$this->middleware('auth');
	}

    public function uploadMemberPhoto(Request $request)
    {

        //ini_set('memory_limit','4096M');
        $input = $request->all();

        //$id = $input['id'];
        $name = ucfirst($input['name_first']);
        $email = $input['email'];

        if(isset($input['files'][0]))
        {
            $uploaded_file = $input['files'][0];
            //dd($uploaded_file);

            // create hash from filename
            $hash = md5($email . $uploaded_file->getClientOriginalName());

            $file_ext = strtolower($uploaded_file->getClientOriginalExtension());
            $file_ext = $file_ext === 'jpeg' ? 'jpg' : $file_ext;

            $photos_path = storage_path('posters/photos');
            $file_name = $hash . '.' . $file_ext;
            $file_path = $photos_path . '/' . $file_name;

            $uploaded_file->move($photos_path, $file_name);
    
            // create poster
            $poster = Poster::where('hash', $hash)->first(); // check for existing
            
            if(! $poster)
            {
                $poster = new Poster;
                //$poster->source_id = $id;
                $poster->name = $name;
                $poster->hash = $hash;
                if(!empty($input['hash']) ){
                    $member = Source::where('hash', $input['hash'])->first();
                    $poster->member = $member->acct_id;
                }
                $poster->email = $email;
                $poster->recieve_emails = isset($input['recieve_emails']) ? $input['recieve_emails'] : 'false';
                $poster->save();
            }

            $this->_delete_cached_posters('friend_' . $hash);

            // pre-render the share image so FB can access immedietely
            $poster_jpg = $this->_render_jpg('friend_' . $hash, url('/poster/member/' . $hash . '.html?share=1'), 1200, 630, false);

            return response()->json([
                'status' => 'ok',
                'name' => $name,
                'email' => $email,
                'hash' => $hash,
                'thumb_url' => url('poster/member/' . $hash . '.thumb'),
                'pdf_url' => url('poster/member/' . $hash . '.pdf'),
                'pdf_download_url' => url('poster/member/' . $hash . '.pdfd'),
                'download_url' => url('poster/member/' . $hash . '.download'),
                'share_url' => url('share/' . $hash),
                'poster_jpg' => $poster_jpg,
            ]);

        }

        exit;

    }

    public function memberPhoto($hash)
    {
        //ini_set('memory_limit','4096M');

        $photo_path = storage_path('posters/photos/' . $hash . '.jpg');

        if(! is_file($photo_path)) abort(404);

        //dd($photo_path);

        $img = \IImage::make($photo_path)->orientate();

        //dd($img);

        $img->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $img->response('jpg');
    }

    public function memberShare($hash, Request $request)
    {
        $poster = Poster::where('hash', $hash)->first();

        if($poster)
        {
            return view('front/member-share', [
                'poster' => $poster,
                'photo_url' => $this->_member_photo_url($hash),
                'fb_meta' => [
                    'og:type' => 'article',
                    'fb:app_id' => '501458776727781',
                    'og:site_name' => 'MelboUrne srorm\'s Secret Weapon',
                    'og:url' => $request->url(),
                    'og:title' => 'Secret weapon!',
                    'og:description' => ucfirst($poster->name) . ' is our secret weapon! Make your front page and let people know. #purplepride',
                    'og:image' => url('poster/member/'.$hash.'.share'),
                    'og:image:type' => 'image/jpeg',
                    'og:image:width' => '1200',
                    'og:image:height' => '630',
                ],
                'posted' => false
            ]);
        }
        else
        {
            return abort(404);
        }

    }

    public function friendShare($name, Request $request)
    {

        if($name)
        {
            return view('front/friend-share', [
                'name' => $name,
                'fb_meta' => [
                    'og:type' => 'article',
                    'fb:app_id' => '501458776727781',
                    'og:site_name' => 'MelboUrne srorm\'s Secret Weapon',
                    'og:url' => $request->url(),
                    'og:title' => 'Have you seen a lost bulldog?',
                    'og:description' => ucfirst($name) .' is our secret weapon! Make your front page and let people know. #purplepride',
                    'og:image' => url('poster/friend/share?name=' . $name),
                    'og:image:type' => 'image/jpeg',
                    'og:image:width' => '1200',
                    'og:image:height' => '630',
                ],
                'posted' => false
            ]);
        }
        else
        {
            return abort(404);
        }

    }

    public function member($hash, $poster_file=[])
    {
        //ini_set('memory_limit','4096M');

        list($poster_info['hash'], $poster_info['file_type']) = explode('.', $hash);

        // get existing poster
        $poster = Poster::where('hash', $poster_info['hash'])->first();
        if(! $poster) abort(404);

        // set poster deets
        $poster_info['name'] = $poster->name;
        $poster_info['photo'] = $this->_member_photo_url($poster_info['hash']);
        $poster_info['file_name'] = 'friend_' . $poster_info['hash'];

        // generate share version?
        if(Input::get('share') || $poster_info['file_type'] == 'share')
        {
            $view = 'posters/member-share';
        }
        else
        {
            $view = 'posters/friend';
        }


        // render html
        $poster_html = view($view, $poster_info);


        // return requested poster
        switch($poster_info['file_type'])
        {
            case 'pdf' :
                return $this->_render_pdf($poster_info['file_name'], $poster_html, $poster_info['name'])->send();
            break;

            case 'pdfd' :
                return $this->_render_pdf($poster_info['file_name'], $poster_html, $poster_info['name'], true, true);
            break;

            case 'image' :
                return $this->_render_jpg($poster_info['file_name'], url('/poster/member/' . $poster_info['hash'] . '.html'))->response('jpg');
            break;

            case 'download' :
                return response()->download($this->_render_jpg($poster_info['file_name'], url('/poster/member/' . $poster_info['hash'] . '.html'), 900, 1255, false, true), 'Secret weapon - ' . $poster_info['name'] . '.jpg');
            break;

            case 'share' :

                // generate share image
                $poster_jpg = $this->_render_jpg($poster_info['file_name'], url('/poster/member/' . $poster_info['hash'] . '.html?share=1'), 952, 952, false);

                // resize
                $poster_jpg = \IImage::make($poster_jpg)->orientate();
                /*$poster_jpg->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                });*/

                return $poster_jpg->response('jpg');

            break;

            case 'thumb' :

                // generate image
                $poster_jpg = $this->_render_jpg($poster_info['file_name'], url('/poster/member/' . $poster_info['hash'] . '.html'), 900, 1255, false);

                //dd(url('/poster/member/' . $poster_info['hash'] . '.html'));

                // resize
                $poster_jpg = \IImage::make($poster_jpg)->orientate();
                $poster_jpg->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                return $poster_jpg->response('jpg');

            break;

            // return html by default
            default :
                return $poster_html;
            break;

        }

    }

    public function friend($type)
    {
        //dd(Input::all());
        //ini_set('memory_limit','4096M');

        // set poster deets
        $poster_info = [
            'name' => Input::get('name'),
            'file_name' => 'friend_' . str_slug(Input::get('name')),
            'file_type' => $type,
        ];

        // censor friend name
        $censor = new CensorWords;
        $poster_info['name_censored'] = $censor->censorString($poster_info['name']);
        $poster_info['name'] = str_replace('*', '_', $poster_info['name_censored']['clean']);

        // generate share version?
        if(Input::get('share') || $poster_info['file_type'] == 'share')
        {
            $view = 'posters/friend-share';
        }
        else
        {
            $view = 'posters/friend';
        }

        //dd($poster_info);

        // render html
        $poster_html = view($view, $poster_info);

        //dd($poster_info);

        // return requested poster
        switch($poster_info['file_type'])
        {
            case 'pdf' :
                $pdf = $this->_render_pdf($poster_info['file_name'], $poster_html, $poster_info['name_censored']['clean'], true);

                if(Input::get('download'))
                {
                    return $pdf;
                }
                else
                {
                    $pdf->send();
                }

            break;

            case 'image' :
                return $this->_render_jpg($poster_info['file_name'], url('/poster/friend/html?name=' . $poster_info['name_censored']['clean']))->response('jpg');
            break;

            case 'download' :
                return response()->download($this->_render_jpg($poster_info['file_name'], url('/poster/friend/html?name=' . $poster_info['name_censored']['clean']), 900, 1255, false, true), 'Melbourne Proud - ' . $poster_info['name_censored']['clean'] . '.jpg');
            break;

            case 'share' :

                // generate share image
                $poster_jpg = $this->_render_jpg($poster_info['file_name'], url('/poster/friend/html?share=1&name=' . $poster_info['name_censored']['clean']), 1200, 630, false);

                // resize
                $poster_jpg = \IImage::make($poster_jpg)->orientate();
                $poster_jpg->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                return $poster_jpg->response('jpg');

            break;

            case 'thumb' :

                // generate pdf
                //$this->_render_pdf($poster_info['file_name'], $poster_html, $poster_info['name_censored']['clean'], false);

                // generate image
                $poster_jpg = $this->_render_jpg($poster_info['file_name'], url('/poster/friend/html?name=' . $poster_info['name_censored']['clean']), 900, 1255, false);
                //$poster_jpg = $this->_render_jpg($poster_info['file_name'], url('/poster/friend/html?name=' . $poster_info['name_censored']['clean']), 900, 1274, false);
                //$poster_jpg = $this->_render_jpg($poster_info['file_name'], (string) $poster_html, 900, 1274, false);

                //dd($poster_jpg);

                // resize
                $poster_jpg = \IImage::make($poster_jpg)->orientate();
                $poster_jpg->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                return $poster_jpg->response('jpg');

            break;

            // return html by default
            default :
                return $poster_html;
            break;

        }

    }

    private function _get_cache_file($name, $file_type) {
        return storage_path('posters/' . $file_type . '/' . $name . '.' . $file_type);
    }

    private function _delete_cached_posters($name) {
        File::delete($this->_get_cache_file($name, 'pdf'));
        File::delete($this->_get_cache_file($name . '_900x1274', 'jpg'));
        File::delete($this->_get_cache_file($name . '_900x900', 'jpg'));
    }

    private function _create_pdf() {

        $wkhtmltopdf_config = array(
            'binary' => env('WKHTMLTOPDF_PATH', ''),
            'no-outline', // Make Chrome not complain
            'margin-top'    => 0,
            'margin-right'  => 0,
            'margin-bottom' => 0,
            'margin-left'   => 0,
            'page-width' => 210,
            'page-height' => 292.83,
            'orientation' => 'Portrait',
            //'disable-smart-shrinking',
            'dpi' => '96',
        );

        //dd($wkhtmltopdf_config);

        $pdf = new WkPDF($wkhtmltopdf_config);

        return $pdf;

    }

    private function _render_pdf($poster_file, $poster_html, $name, $return=true, $download=false) {

        $poster_cache = $this->_get_cache_file($poster_file, 'pdf');
        $poster_filename = 'MelbourneProud-' . $name . '.pdf';

        //dd($poster_cache);
        //echo $poster_html; exit;

        if(! is_file($poster_cache))
        {
            // generate pdf
            $pdf = $this->_create_pdf();
            $pdf->addPage($poster_html);

            // save cache file
            $pdf->saveAs($poster_cache);

            if(Input::get('download') || $download)
            {
                return $pdf->send($poster_filename);
                exit;
            }

            //dd($pdf);

            return $return ? $pdf : false;
        }
        else
        {

            if($return)
            {

                if(Input::get('download') || $download)
                {
                    //dd($poster_cache);
                    return response()->download($poster_cache, $poster_filename);
                }
                else
                {
                    header("Content-type: application/pdf");
                    echo file_get_contents($poster_cache);
                    exit;
                }

            }

        }

    }

    private function _render_jpg($poster_file, $poster_html, $width=900, $height=1255, $send=true, $download=false) {

        $poster_file = $poster_file . '_' . $width . 'x' . $height;

        $poster_cache = $this->_get_cache_file($poster_file, 'jpg');

        //dd($poster_cache);

        if(! is_file($poster_cache))
        {
            $image = new Image($poster_html);
            //$image = new Image('https://github.com/wkhtmltopdf/wkhtmltopdf/issues/1937');


            $image->setOptions(array(
                'binary' => env('WKHTMLTOIMAGE_PATH', ''),
                'width' => $width,
                'height' => $height,
                'disable-smart-width',
                'type' => 'jpg',
                'commandOptions' => array(
        'useExec' => true,      // Can help if generation fails without a useful error message
        'procEnv' => array(
            // Check the output of 'locale' on your system to find supported languages
            'LANG' => 'en_US.utf-8',
        ),
    ),
                //'load-error-handling' => 'ignore',
            ));

            // save cache file
            $image->saveAs($poster_cache);

            //dd($image);

            if($download) return $poster_cache;

            //return image
            return $send ? $image->send() : $poster_cache;
        }
        else
        {
            if($download) return $poster_cache;

            if($send)
            {
                $img = \IImage::make($poster_cache)->orientate();
                return $img;
            }
            else
            {
                return $poster_cache;
            }
        }

    }

    private function _member_photo_url($hash)
    {
        return url('poster/member/photo/' . $hash);
    }

}
