<?php namespace App\Http\Controllers;

use App\Source;
use App\Output;
use App\Poster;
use App\Video;
use App\PackageLink;
use App\Http\Requests\ProcessRequest;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\RequestCallRequest;
use App\Http\Requests\SendFriendPosterRequest;
use App\Http\Requests\SendMemberPosterRequest;

use Illuminate\Http\Request as Request;
use Input;
use HTML;
use Form;
use Mail;
use Validator;
use Log;
use Route;

use Snipe\BanBuilder\CensorWords;

class FrontController extends Controller {

    public function getHome($hash=null){

        if($hash){

            $poster = Poster::where('hash', $hash)->first();

            if($poster){
                return view('front/home', [
                    'posted' => false,
                    'video_url' => false,
                    'fb_share_url' => '#',
                    'download_url' => false,
                    'fb_meta' => [
                        'og:type' => 'website',
                        'fb:app_id' => config('datacleanse.facebook_app_id'),
                        'og:site_name' => config('datacleanse.title'),
                        'og:url' => url('share/' . $hash),
                        'og:title' => config('datacleanse.sub_title'),
                        'og:description' => ucfirst($poster->name) . ' is our secret weapon! Make your front page and let people know. #purplepride',
                        'og:image' => url('poster/member/'.$hash.'.share'),
                        'og:image:type' => 'image/jpeg',
                        'og:image:width' => '952',
                        'og:image:height' => '952',
                    ],
                ]);
            }
        }

        return view('front/home', [
            'posted' => false,
            'fb_share_url' => '#',
            'download_url' => false,
            'video_url' => false,
            'fb_meta' => [
                'og:type' => 'website',
                'fb:app_id' => config('datacleanse.facebook_app_id'),
                'og:site_name' => config('datacleanse.title'),
                'og:url' => url(),
                'og:title' => config('datacleanse.sub_title'),
                'og:description' => 'Make your front page and let people know. #purplepride',
                'og:image' => url('images/create-poster-hero.jpg'),
                'og:image:type' => 'image/jpeg',
                'og:image:width' => '940',
                'og:image:height' => '1132',
            ],
        ]);

    }

    public function getShareVideo($hash=false, $video_data=false) {

        if($hash){

            if($hash === 'dynamic' && $video_data)
            {
                $video_data_ext = base64_decode($video_data);
                $video_data_ext = explode('/', $video_data_ext);

                $name_first = $video_data_ext[0];
                $og_url = url('share/video/dynamic/' . $video_data);
                $video_url = 'https://s3-ap-southeast-2.amazonaws.com/mstorm1216/render/' . $video_data_ext[1];
            }
            else
            {
                // load source record
                $source = Source::hash($hash);
                if( ! $source) return abort(404);

                $video_url = $this->_get_video_url($source->name_first, $source->name_last);
                $name_first = $source->name_first;
                $og_url = url('share/video/' . $hash);
            }

            $view_data = [
                'posted' => false,
                'fb_share_url' => 'https://www.facebook.com/dialog/feed?app_id=' . config('datacleanse.facebook_app_id') . '&link=' . $og_url,
                'download_url' => false,
                'video_url' => $video_url,
                'fb_meta' => [
                    'og:type' => 'website',
                    'fb:app_id' => config('datacleanse.facebook_app_id'),
                    'og:site_name' => config('datacleanse.title'),
                    'og:url' => $og_url,
                    'og:title' => config('datacleanse.sub_title'),
                    'og:description' => ucfirst($name_first) . ' is our secret weapon! Make your front page and let people know. #purplepride',
                    'og:image' => asset('/images/video-poster_play.jpg'),
                    'og:image:type' => 'image/jpeg',
                    'og:image:width' => '940',
                    'og:image:height' => '1132',
                ],
            ];

            //dd($view_data);

            return view('front/home', $view_data);


        }else{
            return redirect(url());
            //return abort(404);
        }

    }

	public function getUpdate($hash=false)
	{

        if($hash)
        {

            // load source record
            $source = Source::hash($hash);

            // if not found (bad hash) redirect
            if( ! $source) return abort(404);

            // record click
            $source->clicked_at = \DB::raw('NOW()');
            $source->save();

            // check if this member has already confirmed, if so use that instead
            $output = Output::where('source_id', '=', $source->id)->first();
            if($output) $source = $output;

            foreach(config('datacleanse.format_source') as $key => $callback)
            {
                if(isset($source->$key) && is_callable($callback))
                {
                    $source->$key = $callback($source->$key);
                }
            }

            $source->video_url = $this->_get_video_url($source->name_first, $source->name_last);

            if($source->data_set == 'Prospects' || $source->data_set == 'Lapsed'){
                $source->option1_title = '4 game membership';
                $source->option1_description = 'General Admission access to 4 home games at AAMI Park plus a member scarf';
                $source->option1_url = 'http://membership.melbournestorm.com.au/package/4-game-membership/';
                $source->option1_price_month = 9.50;
                $source->option2_title = 'storm spirit';
                $source->option2_description = 'General Admission access to 11 home games, member scarf plus other benefits';
                $source->option2_url = 'http://membership.melbournestorm.com.au/package/storm-spirit/';
                $source->option2_price_month = 21;

                 $source->option3_title = 'club house';
                $source->option3_description = 'Level 2 seating, the ability to watch the game behind glass, player interviews and much more';
                $source->option3_url = 'http://membership.melbournestorm.com.au/package/club-house/';
                $source->option3_price_month = 75;
            }

            return view('front/form', [
                'source' => $source,
                'entry_type' => 'update',
                'video_url' => $this->_get_video_url($source->name_first, $source->name_last),
                'fb_share_url' => 'https://www.facebook.com/dialog/feed?app_id=' . config('datacleanse.facebook_app_id') . '&link=' . url((isset($source->hash) ? 'share/video/'. $source->hash : '/')),
                'download_url' => $this->_get_video_url($source->name_first, $source->name_last),
            ]);

        }else{
            return redirect(url());
        }

	}

    public function process(ProcessRequest $request)
    {
        $entry_type = Input::get('entry_type', 'NEW');
        $id = Input::get('id', false);
        $hash = Input::get('source', false);

        if($entry_type == 'update' && $id && $hash)
        {

            // get source record
            $source = Source::hash($hash);

            // return error if source not found
            if(! $source) abort(404);

            $output = Output::where('source_id','=',$source->id)->first();

        }
        else
        {
            $source = false;
            $output = false;
        }



        // get form data
        $input = $request->except(['_token']);
        $input['status'] = $entry_type;

        foreach(config('datacleanse.format_save') as $key => $callback)
        {
            if(is_callable($callback))
            {
                $input = $callback($input);
            }
        }

        //dd($input);

        if($output)
        {

            // update existing record;
            $output->update($input);

            // send UPDATE notification to admin
            // Mail::send('emails.updated_response', ['data' => $output, 'input' => $input], function($message)
            // {
            //     $message->to(config('datacleanse.notification_to_email'), config('datacleanse.notification_to_name'));
            //     $message->subject('UPDATED Lost Bulldog Response');
            //     $message->from(config('datacleanse.notification_from_email'), config('datacleanse.notification_from_name'));
            // });

        } else {

            // copy data from source (not shown on form)
            foreach(config('datacleanse.fields') as $key => $options)
            {
                if(! isset($input[$key]) && isset($options['copy']))
                    $input[$key] = ($source && $source->$key) ? $source->$key : '';
            }

            // set additional columns in output data
            $input['source_id']     = ($source) ? $source->id : 0;
            $input['hash']          = ($source) ? $source->hash : '';
            $input['import_batch']  = ($source) ? $source->import_batch : 'NEW';
            $input['send_method']   = ($source) ? $source->send_method : '';

            //dd($input);

            // create new output
            $result = Output::create($input);

            if('update' === $entry_type) {
                // update output info on source record
                $source->output_id = $result->id;
                $source->output_at = now();
                $source->save();
            }

            $output = $source;

            // send NEW notification to admin
            // Mail::send('emails.new_response', ['data' => $source, 'input' => $input], function($message)
            // {
            //     $message->to(config('datacleanse.notification_to_email'), config('datacleanse.notification_to_name'));
            //     $message->subject('NEW Lost Bulldog Response');
            //     $message->from(config('datacleanse.notification_from_email'), config('datacleanse.notification_from_name'));
            // });

        }

        //dd($output->toArray());

        if($output->email_address)
        {

            Mail::send('emails.thanks', ['name' => ucfirst(trim($output->first_name)), 'email' => $output->email_address, 'membership' => $request->selected_product], function($message) use ($output)
            {
                $message->to($output->email_address, $output->first_name . ' ' . $output->surname);
                $message->subject('You\'ve Been Found!');
                $message->from(config('datacleanse.notification_from_email'), config('datacleanse.notification_from_name'));
            });

        }

        // show thank you screen
        return redirect()->back();
        //return redirect('member/found/' . $source->hash);

    }

    public function getCreatePoster($hash=false)
    {
        return view('front/create-poster', ['hash' => $hash]);
    }

    public function getContact()
    {
        return view('front/contact', ['posted' => false]);
    }

    public function postContact(ContactRequest $request)
    {

			//dd(config('datacleanse.notification_to_name'));
        // send NEW notification to admin
        Mail::send('emails.the_contact', ['request' => $request], function($message) use ($request)
        {
            $message->to(config('datacleanse.notification_to_email'), config('datacleanse.notification_to_name'));
            $message->subject('Melbourne Proud Enquiry');
            $message->from(config('datacleanse.notification_from_email'), config('datacleanse.notification_from_name'));
            $message->cc('web@mmr.com.au');
        });

        return view('front/contact', ['posted' => true]);

    }

    public function postRequestCall(RequestCallRequest $request)
    {
        //dd($request->all());

        // send NEW notification to admin
        Mail::send('emails.request_call', ['request' => $request], function($message)
        {
            $message->to(config('datacleanse.notification_to_email'), config('datacleanse.notification_to_name'));
            $message->subject('Request a Call');
            $message->from(config('datacleanse.notification_from_email'), config('datacleanse.notification_from_name'));
            $message->cc('web@mmr.com.au');
        });

        if($request->ajax())
        {
            return response()->json('ok');
        }
        else
        {
            return view('front/home', ['posted' => true]);
        }

    }

    public function postSendFriendPoster(SendFriendPosterRequest $request)
    {
        //dd($request->all());

        // send NEW notification to admin
        Mail::send('emails.send_friend_poster', ['request' => $request], function($message) use ($request)
        {
            $message->to($request->get('email'));
            $message->subject('Have you seen a lost bulldog?');
            $message->from($request->get('sender_email'), $request->get('sender_name'));
        });

        $poster = new Poster;
        $poster->name = $request->get('sender_name');
        $poster->friend_name = $request->get('name');
        $poster->friend_email = $request->get('email');
        $poster->save();

        return response()->json('ok');

    }

    public function postSendMemberPoster(SendMemberPosterRequest $request)
    {
        //dd($request->all());

        $hash = Input::get('poster_hash');

        $poster = Poster::where('hash', $hash)->first();

        if($poster)
        {
            $poster_data = [
                'thumb_url' => url('poster/member/' . $hash . '.thumb'),
                'pdf_url' => url('poster/member/' . $hash . '.pdf'),
                'pdf_download_url' => url('poster/member/' . $hash . '.pdfd'),
                'download_url' => url('poster/member/' . $hash . '.download'),
                'share_url' => url('poster/member/share/' . $hash),
            ];

            //dd($poster_data);

            // send NEW notification to admin
            Mail::send('emails.send_member_poster', ['request' => $request, 'poster' => $poster, 'poster_data' => (object) $poster_data], function($message) use ($request)
            {
                $message->to($request->email);
                $message->subject('I\'ve Been Found!');
                $message->from(config('datacleanse.notification_from_email'), config('datacleanse.notification_from_name'));
            });

            return response()->json('ok');

        }

    }

    public function getFound($hash)
    {

        if($hash)
        {

            // load source record
            $source = Source::hash($hash);

            // if not found (bad hash) redirect
            if( ! $source) return abort(404);

            return view('front/found', [
                'source' => $source,
            ]);

        }
        else
        {
            return abort(404);
        }

    }

    public function getVideoURL()
    {
        $video_data = [
            'namefirst' => trim(Input::get('namefirst', '')),
            'namelast' => trim(Input::get('namelast', '')),
            'email' => trim(Input::get('email', '')),
            'recieve_emails' => trim(Input::get('recieve_emails', '')),
        ];

        $censor = new CensorWords;
        $censor->setDictionary('en-uk');
        $video_data['namefirst_censored'] = $censor->censorString($video_data['namefirst']);
        $video_data['namelast_censored'] = $censor->censorString($video_data['namelast']);

        $video_data['url'] = 'https://balancer.creativa.com.au/201611melbournestorm/?first_name=' . urlencode(strtoupper($video_data['namefirst_censored']['clean'])) . '&last_name=' . urlencode(strtoupper($video_data['namelast_censored']['clean'])) . '&sectionid=1,2,3,4,5,6&audiosectionid=1';

        //echo $videoURL; exit;

        // Get cURL resource
        $curl = curl_init();

        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $video_data['url'],
        ));

        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        // Close request to clear up some resources
        curl_close($curl);

        // record video entry
        $video = new Video;
        $video->namefirst = $video_data['namefirst'];
        $video->namelast = $video_data['namelast'];
        $video->email = $video_data['email'];
        $video->data = json_encode($video_data);
        $video->url = $video_data['url'];
        $video->response = $resp;
        $video->recieve_emails = $video_data['recieve_emails'];
        $video->save();

        return $resp;

    }

    public function dberror()
    {
        return view('front/db-error');
    }

    public function getTerms()
    {
        return view('front/terms-conditions');
    }

    private function _get_video_url ($name_first="", $name_last="")
    {
        // generate video url
        // fist check if prerendered vid exists. If not, generate the dynamic video
        $video_url = 'https://mstorm1216.s3.amazonaws.com/render/' . strtoupper(str_replace(' ', '', $name_first) . str_replace(' ', '', $name_last)) . '.mp4';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $video_url,
            CURLOPT_HEADER => 1,
            CURLOPT_FILETIME => 1,
            CURLOPT_NOBODY => 1
        ));

        $resp = curl_exec($curl);

        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if($httpCode == 403) {
            $dynamic_video = 'https://balancer.creativa.com.au/201611melbournestorm/?first_name=' . urlencode(strtoupper($name_first)) . '&last_name=' . urlencode(strtoupper($name_last)) . '&sectionid=1,2,3,4,5,6&audiosectionid=1';

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $dynamic_video,
            ));

            $resp = curl_exec($curl);

            $dom = new \DOMDocument();
            $dom->loadHTML($resp);
            $div = $dom->getElementsByTagName('div')->item(0);
            $video_url = $div->attributes->getNamedItem('va')->value;
        }
        // Okay, we have our URL, close curl now
        curl_close($curl);

        return $video_url;
    }

}
