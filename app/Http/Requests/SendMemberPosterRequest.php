<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SendMemberPosterRequest extends Request {

	public function authorize()
	{
		return true;
	}

    public function sanitize()
    {
        return $this->all();
    }

	public function rules()
	{
        return [
            'friend_name' => 'required',
            'email' => 'required:email',
            //'message' => 'required',
        ];
	}

    public function messages()
    {
        return [
            'friend_name.required' => 'Please enter your friend\'s name',
            'email.required' => 'Please enter a valid email address',
            'email.email' => 'Please enter a valid email address',
            //'message.required' => 'Please enter a message',
        ];
    }

}
