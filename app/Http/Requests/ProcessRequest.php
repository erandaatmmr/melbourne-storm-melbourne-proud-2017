<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProcessRequest extends Request {

	public function authorize()
	{
		return true;
	}

    public function sanitize()
    {
        $input = $this->all();
        //dd($input);

        foreach(config('datacleanse.validation_sanitize') as $key => $callback)
        {
            if(is_callable($callback))
            {
                $input = $callback($input);
            }
        }

        $this->replace($input);

        return $this->all();

    }

	public function rules()
	{
        $input = $this->all();
        //dd($input);

		$rules = [];

        foreach(config('datacleanse.validation_rules') as $key => $rule)
        {
            if(is_callable($rule))
            {
                $rules = $rule($input, $rules);
            }
            else
            {
                $rules[$key] = $rule;
            }
        }

        //dd($rules);

        return $rules;

	}

    public function messages()
    {
        return config('datacleanse.validation_messages');
    }

}
