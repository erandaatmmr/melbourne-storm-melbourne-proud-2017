<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class RequestCallRequest extends Request {

	public function authorize()
	{
		return true;
	}

    public function sanitize()
    {
        return $this->all();
    }

	public function rules()
	{
        return [
            'phone' => 'required',
            'time' => 'required',
        ];
	}

    public function messages()
    {
        return [
            'phone.required' => 'Please enter your phone number',
            'time.required' => 'Please select the best time to call',
        ];
    }

}
