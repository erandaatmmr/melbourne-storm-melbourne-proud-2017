<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SendFriendPosterRequest extends Request {

	public function authorize()
	{
		return true;
	}

    public function sanitize()
    {
        return $this->all();
    }

	public function rules()
	{
        return [
            'sender_name' => 'required',
            'sender_email' => 'required:email',
            'email' => 'required:email',
            //'message' => 'required',
        ];
	}

    public function messages()
    {
        return [
            'sender_name.required' => 'Please enter your name',
            'email.required' => 'Please enter your friend\'s email address',
            'email.email' => 'Please enter your friend\'s email address',
            'sender_email.required' => 'Please enter your email address',
            'sender_email.email' => 'Please enter your email address',
            //'message.required' => 'Please enter a message',
        ];
    }

}
