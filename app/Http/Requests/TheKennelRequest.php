<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class TheKennelRequest extends Request {

	public function authorize()
	{
		return true;
	}

    public function sanitize()
    {
        return $this->all();
    }

	public function rules()
	{
        return [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ];
	}

    public function messages()
    {
        return [
            'name.required' => 'Name required',
            'phone.required' => 'Phone number required',
            'email.required' => 'Email required',
            'email.email' => 'Email invalid',
            'message.required' => 'Please enter a message',
        ];
    }

}
