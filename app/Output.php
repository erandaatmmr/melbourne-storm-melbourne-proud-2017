<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Output extends Model {

	protected $table = 'output';

    protected $guarded = ['id', 'source', 'entry_type', 'update'];

    public static function hash($hash)
    {
        return Output::where('hash', '=', $hash)->first();
    }

    public function source()
    {
        return $this->hasOne('App\Source', 'id', 'source_id');
    }

    public static function columnStats($columns)
    {
        $stats = [];

        foreach($columns as $column => $label)
        {
            $stats[$label] = Output::getColumnStats($column);
        }

        return $stats;
    }

    public static function getColumnStats($column)
    {
        $stats = [];

        $query = DB::table('output')->select(DB::raw("$column, count($column) as count"))->groupBy($column)->whereNotNull($column)->get();

        foreach($query as $record)
        {

            $stats[] = [
                'value' => $record->$column,
                'count' => $record->count,
            ];
        }

        $stats = array_sort($stats, function($value) {
            return $value['count'];
        });

        return $stats;
    }

}
