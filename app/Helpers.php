<?php

    function select($first_item, $list)
    {
        $merge = is_array($first_item) ? $first_item : ['' => $first_item];
        return array_merge($merge, $list);
    }

    function select_range($from, $to, $pad_length=false, $pad_str=false, $pad_direction=false) {
        $range = range($from, $to);
        $return = [];
        foreach($range as $val)
        {
            if($pad_length) $val = str_pad($val, $pad_length, $pad_str, $pad_direction);
            $return['_' . $val] = $val;
        }
        return $return;
    }

    function australian_states()
    {
        return [
            'ACT' => 'Australian Capital Territory',
            'NSW' => 'New South Wales',
            'NT' => 'Northern Territory',
            'QLD' => 'Queensland',
            'SA' => 'South Australia',
            'TAS' => 'Tasmania',
            'VIC' => 'Victoria',
            'WA' => 'Western Australia',
            //'-' => '-----------------',
            //'OTHER' => 'Other',
        ];
    }

    function nz_states()
    {
        return [
            'Auckland' => 'Auckland',
            'Bay of Plenty' => 'Bay of Plenty',
            'Canterbury' => 'Canterbury',
            'East Cape' => 'East Cape',
            'Hawkes Bay' => 'Hawkes Bay',
            'Manawatu-Whanganui' => 'Manawatu-Whanganui',
            'Marlborough' => 'Marlborough',
            'Nelson' => 'Nelson',
            'Northland' => 'Northland',
            'Otago' => 'Otago',
            'Southland' => 'Southland',
            'Taranaki' => 'Taranaki',
            'Tasman' => 'Tasman',
            'Waikato' => 'Waikato',
            'Wanganui' => 'Wanganui',
            'Wellington' => 'Wellington',
            'West Coast' => 'West Coast',
        ];
    }

    function genders()
    {
        return [
            'Male' => 'Male',
            'Female' => 'Female',
        ];
    }


    function countries()
    {
        return [
            'AUSTRALIA' => 'Australia',
            'NEW ZEALAND' => 'New Zealand',
            '-' => '-----------------',
            'AFGHANISTAN' => 'Afghanistan',
            'ALBANIA' => 'Albania',
            'ALGERIA' => 'Algeria',
            'AMERICAN SAMOA' => 'American Samoa',
            'ANDORRA' => 'Andorra',
            'ANGOLA' => 'Angola',
            'ANGUILLA' => 'Anguilla',
            'ANTARCTICA' => 'Antarctica',
            'ANTIGUA AND BARBUDA' => 'Antigua and Barbuda',
            'ARGENTINA' => 'Argentina',
            'ARMENIA' => 'Armenia',
            'ARUBA' => 'Aruba',
            'AUSTRIA' => 'Austria',
            'AZERBAIJAN' => 'Azerbaijan',
            'BAHAMAS' => 'Bahamas',
            'BAHRAIN' => 'Bahrain',
            'BANGLADESH' => 'Bangladesh',
            'BARBADOS' => 'Barbados',
            'BELARUS' => 'Belarus',
            'BELGIUM' => 'Belgium',
            'BELIZE' => 'Belize',
            'BENIN' => 'Benin',
            'BERMUDA' => 'Bermuda',
            'BHUTAN' => 'Bhutan',
            'BOLIVIA' => 'Bolivia',
            'BOSNIA AND HERZEGOWINA' => 'Bosnia and Herzegowina',
            'BOTSWANA' => 'Botswana',
            'BOUVET ISLAND' => 'Bouvet Island',
            'BRAZIL' => 'Brazil',
            'BRITISH INDIAN OCEAN TERRITORY' => 'British Indian Ocean Territory',
            'BRUNEI DARUSSALAM' => 'Brunei Darussalam',
            'BULGARIA' => 'Bulgaria',
            'BURKINA FASO' => 'Burkina Faso',
            'BURUNDI' => 'Burundi',
            'CAMBODIA' => 'Cambodia',
            'CAMEROON' => 'Cameroon',
            'CANADA' => 'Canada',
            'CAPE VERDE' => 'Cape Verde',
            'CAYMAN ISLANDS' => 'Cayman Islands',
            'CENTRAL AFRICAN REPUBLIC' => 'Central African Republic',
            'CHAD' => 'Chad',
            'CHILE' => 'Chile',
            'CHINA' => 'China',
            'CHRISTMAS ISLAND' => 'Christmas Island',
            'COCOS (KEELING) ISLANDS' => 'Cocos (Keeling) Islands',
            'COLOMBIA' => 'Colombia',
            'COMOROS' => 'Comoros',
            'CONGO' => 'Congo',
            'CONGO, THE DEMOCRATIC REPUBLIC OF THE' => 'Congo, the Democratic Republic of the',
            'COOK ISLANDS' => 'Cook Islands',
            'COSTA RICA' => 'Costa Rica',
            'COTE DIVOIRE' => 'Cote d\'Ivoire',
            'CROATIA (HRVATSKA)' => 'Croatia (Hrvatska)',
            'CUBA' => 'Cuba',
            'CYPRUS' => 'Cyprus',
            'CZECH REPUBLIC' => 'Czech Republic',
            'DENMARK' => 'Denmark',
            'DJIBOUTI' => 'Djibouti',
            'DOMINICA' => 'Dominica',
            'DOMINICAN REPUBLIC' => 'Dominican Republic',
            'EAST TIMOR' => 'East Timor',
            'ECUADOR' => 'Ecuador',
            'EGYPT' => 'Egypt',
            'EL SALVADOR' => 'El Salvador',
            'EQUATORIAL GUINEA' => 'Equatorial Guinea',
            'ERITREA' => 'Eritrea',
            'ESTONIA' => 'Estonia',
            'ETHIOPIA' => 'Ethiopia',
            'FALKLAND ISLANDS (MALVINAS)' => 'Falkland Islands (Malvinas)',
            'FAROE ISLANDS' => 'Faroe Islands',
            'FIJI' => 'Fiji',
            'FINLAND' => 'Finland',
            'FRANCE' => 'France',
            'FRANCE METROPOLITAN' => 'France Metropolitan',
            'FRENCH GUIANA' => 'French Guiana',
            'FRENCH POLYNESIA' => 'French Polynesia',
            'FRENCH SOUTHERN TERRITORIES' => 'French Southern Territories',
            'GABON' => 'Gabon',
            'GAMBIA' => 'Gambia',
            'GEORGIA' => 'Georgia',
            'GERMANY' => 'Germany',
            'GHANA' => 'Ghana',
            'GIBRALTAR' => 'Gibraltar',
            'GREECE' => 'Greece',
            'GREENLAND' => 'Greenland',
            'GRENADA' => 'Grenada',
            'GUADELOUPE' => 'Guadeloupe',
            'GUAM' => 'Guam',
            'GUATEMALA' => 'Guatemala',
            'GUINEA' => 'Guinea',
            'GUINEA-BISSAU' => 'Guinea-Bissau',
            'GUYANA' => 'Guyana',
            'HAITI' => 'Haiti',
            'HEARD AND MC DONALD ISLANDS' => 'Heard and Mc Donald Islands',
            'HOLY SEE (VATICAN CITY STATE)' => 'Holy See (Vatican City State)',
            'HONDURAS' => 'Honduras',
            'HONG KONG' => 'Hong Kong',
            'HUNGARY' => 'Hungary',
            'ICELAND' => 'Iceland',
            'INDIA' => 'India',
            'INDONESIA' => 'Indonesia',
            'IRAN (ISLAMIC REPUBLIC OF)' => 'Iran (Islamic Republic of)',
            'IRAQ' => 'Iraq',
            'IRELAND' => 'Ireland',
            'ISRAEL' => 'Israel',
            'ITALY' => 'Italy',
            'JAMAICA' => 'Jamaica',
            'JAPAN' => 'Japan',
            'JORDAN' => 'Jordan',
            'KAZAKHSTAN' => 'Kazakhstan',
            'KENYA' => 'Kenya',
            'KIRIBATI' => 'Kiribati',
            'KOREA, DEMOCRATIC PEOPLES REPUBLIC OF' => 'Korea, Democratic People\'s Republic of',
            'KOREA, REPUBLIC OF' => 'Korea, Republic of',
            'KUWAIT' => 'Kuwait',
            'KYRGYZSTAN' => 'Kyrgyzstan',
            'LAO, PEOPLES DEMOCRATIC REPUBLIC' => 'Lao, People\'s Democratic Republic',
            'LATVIA' => 'Latvia',
            'LEBANON' => 'Lebanon',
            'LESOTHO' => 'Lesotho',
            'LIBERIA' => 'Liberia',
            'LIBYAN ARAB JAMAHIRIYA' => 'Libyan Arab Jamahiriya',
            'LIECHTENSTEIN' => 'Liechtenstein',
            'LITHUANIA' => 'Lithuania',
            'LUXEMBOURG' => 'Luxembourg',
            'MACAU' => 'Macau',
            'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF' => 'Macedonia, The Former Yugoslav Republic of',
            'MADAGASCAR' => 'Madagascar',
            'MALAWI' => 'Malawi',
            'MALAYSIA' => 'Malaysia',
            'MALDIVES' => 'Maldives',
            'MALI' => 'Mali',
            'MALTA' => 'Malta',
            'MARSHALL ISLANDS' => 'Marshall Islands',
            'MARTINIQUE' => 'Martinique',
            'MAURITANIA' => 'Mauritania',
            'MAURITIUS' => 'Mauritius',
            'MAYOTTE' => 'Mayotte',
            'MEXICO' => 'Mexico',
            'MICRONESIA, FEDERATED STATES OF' => 'Micronesia, Federated States of',
            'MOLDOVA, REPUBLIC OF' => 'Moldova, Republic of',
            'MONACO' => 'Monaco',
            'MONGOLIA' => 'Mongolia',
            'MONTSERRAT' => 'Montserrat',
            'MOROCCO' => 'Morocco',
            'MOZAMBIQUE' => 'Mozambique',
            'MYANMAR' => 'Myanmar',
            'NAMIBIA' => 'Namibia',
            'NAURU' => 'Nauru',
            'NEPAL' => 'Nepal',
            'NETHERLANDS' => 'Netherlands',
            'NETHERLANDS ANTILLES' => 'Netherlands Antilles',
            'NEW CALEDONIA' => 'New Caledonia',
            'NICARAGUA' => 'Nicaragua',
            'NIGER' => 'Niger',
            'NIGERIA' => 'Nigeria',
            'NIUE' => 'Niue',
            'NORFOLK ISLAND' => 'Norfolk Island',
            'NORTHERN MARIANA ISLANDS' => 'Northern Mariana Islands',
            'NORWAY' => 'Norway',
            'OMAN' => 'Oman',
            'PAKISTAN' => 'Pakistan',
            'PALAU' => 'Palau',
            'PANAMA' => 'Panama',
            'PAPUA NEW GUINEA' => 'Papua New Guinea',
            'PARAGUAY' => 'Paraguay',
            'PERU' => 'Peru',
            'PHILIPPINES' => 'Philippines',
            'PITCAIRN' => 'Pitcairn',
            'POLAND' => 'Poland',
            'PORTUGAL' => 'Portugal',
            'PUERTO RICO' => 'Puerto Rico',
            'QATAR' => 'Qatar',
            'REUNION' => 'Reunion',
            'ROMANIA' => 'Romania',
            'RUSSIAN FEDERATION' => 'Russian Federation',
            'RWANDA' => 'Rwanda',
            'SAINT KITTS AND NEVIS' => 'Saint Kitts and Nevis',
            'SAINT LUCIA' => 'Saint Lucia',
            'SAINT VINCENT AND THE GRENADINES' => 'Saint Vincent and the Grenadines',
            'SAMOA' => 'Samoa',
            'SAN MARINO' => 'San Marino',
            'SAO TOME AND PRINCIPE' => 'Sao Tome and Principe',
            'SAUDI ARABIA' => 'Saudi Arabia',
            'SENEGAL' => 'Senegal',
            'SEYCHELLES' => 'Seychelles',
            'SIERRA LEONE' => 'Sierra Leone',
            'SINGAPORE' => 'Singapore',
            'SLOVAKIA (SLOVAK REPUBLIC)' => 'Slovakia (Slovak Republic)',
            'SLOVENIA' => 'Slovenia',
            'SRI LANKA' => 'Sri Lanka',
            'SOLOMON ISLANDS' => 'Solomon Islands',
            'SOMALIA' => 'Somalia',
            'SOUTH AFRICA' => 'South Africa',
            'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS' => 'South Georgia and the South Sandwich Islands',
            'SPAIN' => 'Spain',
            'ST. HELENA' => 'St. Helena',
            'ST. PIERRE AND MIQUELON' => 'St. Pierre and Miquelon',
            'SUDAN' => 'Sudan',
            'SURINAME' => 'Suriname',
            'SVALBARD AND JAN MAYEN ISLANDS' => 'Svalbard and Jan Mayen Islands',
            'SWAZILAND' => 'Swaziland',
            'SWEDEN' => 'Sweden',
            'SWITZERLAND' => 'Switzerland',
            'SYRIAN ARAB REPUBLIC' => 'Syrian Arab Republic',
            'TAIWAN' => 'Taiwan, Province of China',
            'TAJIKISTAN' => 'Tajikistan',
            'TANZANIA, UNITED REPUBLIC OF' => 'Tanzania, United Republic of',
            'THAILAND' => 'Thailand',
            'TOGO' => 'Togo',
            'TOKELAU' => 'Tokelau',
            'TONGA' => 'Tonga',
            'TRINIDAD AND TOBAGO' => 'Trinidad and Tobago',
            'TUNISIA' => 'Tunisia',
            'TURKEY' => 'Turkey',
            'TURKMENISTAN' => 'Turkmenistan',
            'TURKS AND CAICOS ISLANDS' => 'Turks and Caicos Islands',
            'TUVALU' => 'Tuvalu',
            'UGANDA' => 'Uganda',
            'UKRAINE' => 'Ukraine',
            'UNITED ARAB EMIRATES' => 'United Arab Emirates',
            'UNITED KINGDOM' => 'United Kingdom',
            'USA' => 'United States',
            'UNITED STATES MINOR OUTLYING ISLANDS' => 'United States Minor Outlying Islands',
            'URUGUAY' => 'Uruguay',
            'UZBEKISTAN' => 'Uzbekistan',
            'VANUATU' => 'Vanuatu',
            'VENEZUELA' => 'Venezuela',
            'VIETNAM' => 'Vietnam',
            'VIRGIN ISLANDS (BRITISH)' => 'Virgin Islands (British)',
            'VIRGIN ISLANDS (U.S.)' => 'Virgin Islands (U.S.)',
            'WALLIS AND FUTUNA ISLANDS' => 'Wallis and Futuna Islands',
            'WESTERN SAHARA' => 'Western Sahara',
            'YEMEN' => 'Yemen',
            'ZAMBIA' => 'Zambia',
            'ZIMBABWE' => 'Zimbabwe',
        ];
    }

    function afl_clubs()
    {
        return [
            'Adelaide Crows' => 'Adelaide Crows',
            'Brisbane Lions' => 'Brisbane Lions',
            'Carlton' => 'Carlton',
            'Collingwood' => 'Collingwood',
            'Essendon' => 'Essendon',
            'Fremantle' => 'Fremantle',
            'Geelong Cats' => 'Geelong Cats',
            'Gold Coast SUNS' => 'Gold Coast SUNS',
            'GWS GIANTS' => 'GWS GIANTS',
            'Hawthorn' => 'Hawthorn',
            'Kangaroos' => 'Kangaroos',
            'Melbourne' => 'Melbourne',
            'Port Adelaide' => 'Port Adelaide',
            'Richmond' => 'Richmond',
            'St Kilda' => 'St Kilda',
            'Sydney Swans' => 'Sydney Swans',
            'West Coast Eagles' => 'West Coast Eagles',
            'Western Bulldogs' => 'Western Bulldogs',
        ];
    }

function titles()
    {
        return [
            'Mr' => 'Mr',
            'Mrs' => 'Mrs',
            'Ms' => 'Ms',
            'Miss' => 'Miss',
            'Master' => 'Master',
            'Dr' => 'Dr',
            'Professor' => 'Professor',
            'Sister' => 'Sister',
            'Rev' => 'Rev',
            'Father' => 'Father',
            'Sir' => 'Sir',
            'Cmdr' => 'Cmdr',
            'Judge' => 'Judge',
        ];
    }

    function now($time=false) {
        $format = 'Y-m-d H:i:s';
        return $time ? date($format, $time) : date($format);
    }

    function do_wb_lookup($all_events, $i, $vi) {

        $value = '';
        $event = $all_events[$i];
        $var_text_i = 'var_text' . $vi;

        if($event !== '/')
        {
            //echo $event."\n";

            $lookup = App\Lookup::where('member_key', $event)->first();

            if($lookup)
            {

                // var_text1
                if($lookup->$var_text_i != '')
                {

                    //echo $lookup->$var_text_i."\n";

                    $text_code = App\TextCode::where('var_text_code', $lookup->$var_text_i)->first();
                    if($text_code)
                    {
                        //echo $text_code->text."\n";

                        $value = '<br><br><em>' . $text_code->text . '</em>';
                    }

                }

            }

        }

        return $value;

    }
