<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\DataImport',
		'App\Console\Commands\DataExport',
		'App\Console\Commands\DataProcess',
		'App\Console\Commands\DataDistribute',
		'App\Console\Commands\DataValidate',
		'App\Console\Commands\DataDump',
		'App\Console\Commands\DataHash',
		'App\Console\Commands\CMListImport',
		'App\Console\Commands\CMGetClients',
		'App\Console\Commands\CMGetTemplates',
		'App\Console\Commands\CMListCreate',
		'App\Console\Commands\CMCampaignCreate',
		'App\Console\Commands\DCSetup',
		'App\Console\Commands\URLShorten',
		'App\Console\Commands\URLTest',
		'App\Console\Commands\SMSExport',
		'App\Console\Commands\SMSProcess',
		'App\Console\Commands\SMSValidate',
		'App\Console\Commands\ListsPrepare',
		'App\Console\Commands\CampaignsPrepare',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{

	}

}
