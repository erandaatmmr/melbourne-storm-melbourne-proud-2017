<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Illuminate\Database\Eloquent\Model;

use DB;
use Schema;
use App\Source;
use App\Output;

//use GuzzleHttp\Psr7\Request;

class URLTest extends Command {

    private $client;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'urls:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tests generated short urls for given batch';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = new \GuzzleHttp\Client();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        ini_set('memory_limit','1024M');

        $batch = $this->option('batch');

        $get_source = Source::where('import_batch', '=', $batch)->where('short_url', '<>', '')->get();

        $i = 1;
        $num_source = count($get_source);

        echo "\n";

        foreach($get_source as $source)
        {

            $response = $this->testURL($source->short_url);

            if($response == '200')
            {
                $this->info($source->short_url . ' - 200 OK (' . $i . ' of ' . $num_source . ')');
            }
            else
            {
                $this->error($source->short_url . ' - ' . $response . ' (' . $i . ' of ' . $num_source . ')');
            }

            $i++;

        }

        echo "\n";

    }

    protected function testURL($url)
    {
        $request = $this->client->get($url);
        return $request->getStatusCode();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
        ];
    }

}
