<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use App\Source;

class SMSValidate extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sms:validate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Validate source data';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
    	ini_set('memory_limit','1024M');

		$nl = "\n";
		echo $nl;

        $mobile_field = config('datacleanse.mobile_field');

        $batch = $this->option('batch');
        //echo $batch; exit;
        $fields = ['id', $mobile_field, 'import_batch', 'hash'];
        $export_path = storage_path('export');
        $validation_errors = [];
        $error_ids = [];
        //echo $export_path; exit;

        $source = Source::select(['id', $mobile_field])->where('import_batch', $batch)->get();
        $num_source = count($source);
        $i = 1;

        // validate data
        foreach($source as $record)
        {

            if (strlen(trim($record->$mobile_field)) < 10)
            {
                $validation_errors[] = 'Invalid Mobile (' . $record->id . '): ' . $record->$mobile_field;
                if(! in_array($record->id, $error_ids)) $error_ids[] = $record->id;

                // mark record as invalid email
                //DB::table('source')->where($mobile_field, $record->$mobile_field)->update(['invalid_mobile' => '1']);
                $record->invalid_mobile = '1';
                $record->save();

            }

            if($i % 1000 == 0) $this->info('Validating ' . $i . ' of ' . $num_source);

            $i++;

        }

        if(count($validation_errors) > 0)
        {
            $this->error('VALIDATION ERRORS:');
            echo $nl;

            foreach($validation_errors as $error)
            {
                $this->error($error);
            }

            echo $nl;
            $this->error('IDs: ' . implode(',', $error_ids));

            echo $nl;
            return;

        }

        $this->info('No validation errors were encountered');

		echo $nl;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
