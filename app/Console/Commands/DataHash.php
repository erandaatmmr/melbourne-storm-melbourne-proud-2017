<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;

class DataHash extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'source:hash';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create hashes for source records (in specified batch)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$nl = "\n";
		echo $nl;

        $batch = $this->option('batch');

        DB::statement("UPDATE `source` SET `hash` = UUID() WHERE `import_batch` = '$batch'");

		echo $nl;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
