<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use Excel;
use Schema;
use Storage;

use App\Source;

class DataDump extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'source:dump';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Dumps given batch to SQL inserts';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
    	ini_set('memory_limit','1024M');

		$nl = "\n";
		echo $nl;

        $batch = $this->option('batch');
        $columns = Schema::getColumnListing('source');
        $columns_sql = array_map(function($a) { return '`' . $a . '`'; }, $columns);
        //print_r($columns); exit;

        $insert_open = "INSERT INTO `source` (". implode(', ', $columns_sql) .") VALUES (";
        $insert_close = ");";

        // create file
        $sql_file = 'export/'.$batch . '.sql';
        Storage::put($sql_file, '');

        $get_source = Source::where('import_batch', '=', $batch)->get();

        $row_count = 0;

        foreach($get_source as $source)
        {
            $row = [];

            foreach($source->toArray() as $key=>$value)
            {
                $row[] = ($key === 'id') ? 'NULL' : "'".$value."'";
            }

            $row = implode(', ', $row);

            $insert = $insert_open.$row.$insert_close;

            //echo $insert; exit;

            Storage::append($sql_file, $insert);

            $row_count++;

        }

        $this->info('Dumped ' . $row_count . ' records.');
        echo $nl;

        exit;

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
