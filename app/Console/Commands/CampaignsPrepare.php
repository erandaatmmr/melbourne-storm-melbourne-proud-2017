<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use App\Source;
use App\CMList;
use App\CMCampaign;

class CampaignsPrepare extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'campaigns:prepare';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create list entry from source mailing lists';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		echo "\n";

        ini_set('memory_limit','1024M');

        $batch = $this->option('batch');

        $lists = CMList::where('import_batch', $batch)
                       ->where('campaign_id', '=', '0')
                       ->get();

        //print_r($lists->toArray()); exit;

        foreach($lists as $list)
        {
            $campaign = new CMCampaign;
            $campaign->name = $list->name;
            $campaign->list_id = $list->id;
            $campaign->cm_list_id = $list->list_id;
            $campaign->import_batch = $list->import_batch;
            $campaign->mailing_list = $list->mailing_list;
            $campaign->template_id = config('datacleanse.cm_campaign_template_id');
            $campaign->from_name = config('datacleanse.cm_campaign_from_name');
            $campaign->from_email = config('datacleanse.cm_campaign_from_email');
            $campaign->reply_email = config('datacleanse.cm_campaign_reply_email');
            $campaign->subject = config('datacleanse.cm_campaign_subject');
            $campaign->save();

            $list->campaign_id = $campaign->id;
            $list->save();

            $this->info('Prepared campaign: ' . $campaign->name);
        }

        echo "\n";

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
