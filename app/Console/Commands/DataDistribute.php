<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;

class DataDistribute extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'source:distribute';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Evenly distribute emails across exported lists';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
    	ini_set('memory_limit','2048M');

		$nl = "\n";
		echo $nl;
        $this->info('Starting distribution...');
		echo $nl;

        $batch = $this->option('batch');
        $singles = 0;
        $duplicates = 0;

        // get mailing lists
        $mailing_lists = [];
        $get_mailing_lists = DB::table('source')->select('mailing_list')->where('import_batch', '=', $batch)->where('invalid_email', '=', '0')->groupBy('mailing_list')->get();

        // process lists
        if(count($get_mailing_lists) > 0)
        {
            foreach($get_mailing_lists as $list)
            {
                $mailing_lists[] = [
                    'name' => $list->mailing_list,
                    'records' => DB::table('source')->select('id')->where('import_batch', '=', $batch)->where('mailing_list', '=', $list->mailing_list)->count(),
                ];
            }
        }

        //print_r($mailing_lists); exit;

        // get numbers
        $total_records = DB::table('source')->select('id')->where('import_batch', '=', $batch)->count();
        $total_lists = count($mailing_lists);
        $per_list = ceil($total_records / $total_lists);
        $this->info('Mailing lists: ' . $total_lists);
        $this->info('Total Records: ' . $total_records);
        $this->info('Records Per List: ' . $per_list);

        // determine distributable records
        $count_distributable = DB::table('source')->select('id')->where('import_batch', '=', $batch)->where('mailing_list', '=', $mailing_lists[0]['name'])->where('email_duplicates', '=', '1')->count();
        $count_non_distributable = DB::table('source')->select('id')->where('import_batch', '=', $batch)->where('mailing_list', '=', $mailing_lists[0]['name'])->where('email_duplicates', '>', '1')->count();

        $leave_behind = $per_list - $count_non_distributable;

        // extract distributable records
        $distributable = [];
        $get_distributable = DB::table('source')->select('id')->where('import_batch', '=', $batch)->where('mailing_list', '=', $mailing_lists[0]['name'])->where('email_duplicates', '=', '1')->take($count_distributable - $leave_behind)->skip($leave_behind)->get();
        foreach($get_distributable as $record) $distributable[] = $record->id;

        $total_distributable = count($distributable);

        $this->info('Distributable Records: ' . $count_distributable);
        $this->info('Non-Distributable Records: ' . $count_non_distributable);
        $this->info('Leave Behind: ' . $leave_behind);
		echo $nl;

        //echo '$count_non_distributable ' . $count_non_distributable . "\n";
        //echo '$count_distributable ' . $count_distributable . "\n";
        //echo '$leave_behind ' . $leave_behind . "\n";
        //exit;
        //print_r(count($distributable)); exit;

        // determine how many for each list
        $from = 0;
        $distributable_index = 0;
        $distributable_remaining = $total_distributable;

        foreach($mailing_lists as $key => &$list)
        {
            $list['take'] = $per_list - $list['records'];
            $list['take'] = ($list['take'] > $distributable_remaining) ? $distributable_remaining : $list['take'];
            $list['from'] = ($key > 0) ? $from + $list['take'] : 0;
            $from = ($key > 0) ? $from + $list['take'] + 1 : $from;
            $list['ids'] = [];

            $this->info('Distributable Remaining: ' . $distributable_remaining);
            echo $nl;

            $this->info('List '.$list['name'].':');

            // divide distributabel ids
            for($i=0; $i<$list['take']; $i++)
            {
                if(isset($distributable[$distributable_index]))
                {
                    $list['ids'][] = $distributable[$distributable_index];
                    $distributable_index++;
                    $distributable_remaining--;
                }
            }
            
            $list['num_ids'] = count($list['ids']);
            $list['total'] = $list['records'] + $list['take'];

            $this->info('    Existing: ' . $list['records']);
            $this->info('    Take: ' . $list['take']);
            //$this->info('    From: ' . $list['from']);
            $this->info('    IDs: ' . $list['num_ids']);
            $this->info('    Total After Distribution: ' . $list['total']);

            echo $nl;

        }

        //print_r($mailing_lists); exit;

        // update mailing list col
        foreach($mailing_lists as $mailing_list)
        {
            $this->info('Distributing list ' . $mailing_list['name'] . ': ' . ($mailing_list['take'] > 0 ? '+' : '') . $mailing_list['take'] . ' records');

            if(count($mailing_list['ids']) > 0)
            {
                DB::table('source')->whereIn('id', $mailing_list['ids'])->update(['mailing_list' => $mailing_list['name']]);
            }
        }


        //print_r($per_list); exit;

		echo $nl;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
