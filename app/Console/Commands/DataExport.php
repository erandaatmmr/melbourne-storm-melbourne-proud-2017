<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use Excel;

use App\Source;
use App\Lookup;
use App\TextCode;

class DataExport extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'source:export';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Export source data to individual mailing lists accounting for duplicates';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
    	ini_set('memory_limit','1024M');

		$nl = "\n";
		echo $nl;

        $batch = $this->option('batch');
        //echo $batch; exit;

        $export_path = storage_path('export');
        $validation_errors = [];
        $error_ids = [];

        $this->info('Starting...');

        // get mailing lists
        $mailing_lists = [];
        $get_mailing_lists = DB::table('source')->select('mailing_list')->where('import_batch', '=', $batch)->where('mailing_list', '<>', '')->groupBy('mailing_list')->get();

        $datacleanse_url = config('datacleanse.url');
        $email_field = config('datacleanse.email_field');
        $first_name_field = config('datacleanse.first_name_field');
        $last_name_field = config('datacleanse.last_name_field');
        $list_fields = config('datacleanse.cm_list_fields');

        if(count($get_mailing_lists) > 0)
        {

            foreach($get_mailing_lists as $list) $mailing_lists[] = $list->mailing_list;

            //print_r($mailing_lists); exit;

            // generate each list
            foreach($mailing_lists as $list)
            {
                $export_file = $list;
                $export_data = [];

                $this->info('Exporting... ' . $list);

                $sourceData = Source::where('mailing_list', $list)->get();

                //print_r($sourceData->toArray()); exit;

                foreach($sourceData as $record)
                {

                    $export_row = [
                        'EmailAddress' => $record->$email_field,
                        'Name' => $record->$first_name_field . ' ' . $record->$last_name_field,
                    ];

                    foreach($list_fields as $key => $field)
                    {

                        $source_key = (isset($field['field'])) ? $field['field'] : false;

                        if($source_key && isset($record->$source_key))
                        {
                            $value = ($record->$source_key && strlen($record->$source_key) > 0) ? $record->$source_key : '';

                            if(isset($field['trim']))
                            {
                                $value = (strlen($value) > $field['trim']) ? substr($value, 0, $field['trim']-3).'...' : $value;
                            }

                            if(isset($field['date']))
                            {
                                $value = date($field['date'], strtotime($value));
                            }

                            if(isset($field['format']) && is_callable($field['format']))
                            {
                                $value = $field['format']($value);
                            }

                            $export_row[$key] = $value;

                        }
                        elseif(isset($field['custom']) && is_callable($field['custom']))
                        {
                            $export_row[$key] = $field['custom']($record);
                            $record->$key = $export_row[$key];
                        }
                        else
                        {
                            $export_row[$key] = '';
                        }

                    }

                    $export_row['confirm_url'] = config('datacleanse.url') . str_replace('(hash)', $record->hash, config('datacleanse.confirm_url'));
                    $export_row['update_url'] = config('datacleanse.url') . str_replace('(hash)', $record->hash, config('datacleanse.update_url'));

                    //print_r($export_row);

                    $export_data[] = $export_row;

                }

                //exit;
                //print_r($export_data); exit;

                Excel::create($export_file, function($excel) use($export_data) {

                    $excel->sheet('data', function($sheet) use($export_data) {
                        $sheet->fromArray($export_data);
                    });

                })->store('csv', $export_path);

                $this->info('Exported: ' . $export_file . '.csv');

            }

        }
        else
        {
            $this->error('There are no lists to export!');
        }

		echo $nl;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
