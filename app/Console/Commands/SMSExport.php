<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use Excel;

class SMSExport extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sms:export';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Export SMS list for upload to Clicksend';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
    	ini_set('memory_limit','1024M');

		$nl = "\n";
		echo $nl;

        $batch = $this->option('batch');
        //echo $batch; exit;
                   
        $export_path = storage_path('export');
        $validation_errors = [];
        $error_ids = [];
		$mobile_field = config('datacleanse.mobile_field');
		$first_name_field = config('datacleanse.first_name_field');
		$last_name_field = config('datacleanse.last_name_field');

        $this->info('Starting...');

        $mailing_lists = [];
        $get_mailing_lists = DB::table('source')->select('mailing_list')->where('import_batch', '=', $batch)->where('invalid_mobile', '=', '0')->groupBy('mailing_list')->get();

        if(count($get_mailing_lists) > 0)
        {

            foreach($get_mailing_lists as $list) $mailing_lists[] = $list->mailing_list;
    
            // generate each list
            foreach($mailing_lists as $list)
            {
                $export_file = $list;

                $this->info('Exporting... ' . $list);
    
                $data = [];
                $get_list = DB::table('source')->where('mailing_list', '=', $list)->where('invalid_mobile', '=', '0')->get();
    
                foreach($get_list as $record)
                {

                    $short_url = str_replace('http://', '', $record->short_url);

                    if(strlen(trim($record->$first_name_field)) >= config('datacleanse.sms_min_name') && strlen(trim($record->$first_name_field)) <= config('datacleanse.sms_max_name'))
                    {
                        $sms_message = config('datacleanse.sms');
                        $sms_message = str_replace('(First Name)', $record->$first_name_field, $sms_message);
                    }
                    else
                    {
                        $sms_message = config('datacleanse.sms_no_name');
                    }

                    $sms_message = str_replace('(URL)', $short_url, $sms_message);

                    $data[] = [
                        'Mobile No' => str_replace(' ', '', $record->$mobile_field),
                        'First Name' => $record->$first_name_field,
                        'Last Name' => $record->$last_name_field,
                        'Custom 1' => $sms_message,
                    ];
                }

                //print_r($data); exit;

                Excel::create($export_file, function($excel) use($data) {
                
                    $excel->sheet('data', function($sheet) use($data) {            
                        $sheet->fromArray($data);            
                    });
                
                })->store('csv', $export_path);
    
                $this->info('Exported: ' . $export_file . '.csv');
    
            }

        }
        else
        {
            $this->error('There are no lists to export!');
        }

		echo $nl;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
