<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Illuminate\Database\Eloquent\Model;

use DB;
use Excel;
use Schema;
use App\Source;
use App\Output;

class DCSetup extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'dc:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migrations, seed database & create source and output tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        echo "\n";

        // run migrations
        $this->info('Running migrations...');
        echo "\n";
        $this->call('migrate:reset');
        echo "\n";
        $this->call('migrate');
        echo "\n";

        // seed
        $this->info('Seeding...');
        $this->call('db:seed');
        echo "\n";

        // get fields
        $fields = config('datacleanse.fields');

        // update source
        $this->info('Configuring source table...');
        $this->updateTable('source', $fields);
        echo "\n";

        // update output
        $this->info('Configuring output table...');
        $this->updateTable('output', $fields);
        echo "\n";

        $this->info('Done!');
        echo "\n";

    }

    protected function updateTable($table_name, $fields)
    {
        echo "\n";

        Schema::table($table_name, function($table) use ($fields, $table_name)
        {
            $after = 'id';

            $other_table = ($table_name == 'source') ? 'output' : 'source';

            foreach($fields as $field => $options)
            {

                // skip if not for this table
                if(isset($options[$other_table . '_only'])) continue;

                $length = isset($options['length']) ? $options['length'] : 255;

                switch($options['type'])
                {
                    case 'integer' : $col = $table->integer($field)->nullable(); break;
                    case 'string' : $col = $table->string($field, $length)->nullable(); break;
                    case 'text' : $col = $table->text($field)->nullable(); break;
                    case 'date' : $col = $table->date($field)->nullable(); break;
                    case 'datetime' : $col = $table->datetime($field)->nullable(); break;
                }

                // set default value
                if(isset($options['default'])) $col->default($options['default']);

                $col->after($after);
                $after = $field;

                $this->info('    ' . $field);

            }

        });

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['batch', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
            //['file', null, InputOption::VALUE_REQUIRED, 'Import', null],
        ];
    }

}
