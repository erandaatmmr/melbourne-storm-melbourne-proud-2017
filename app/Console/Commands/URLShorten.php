<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Illuminate\Database\Eloquent\Model;

use DB;
use Schema;
use App\Source;
use App\Output;

//use GuzzleHttp\Psr7\Request;

class URLShorten extends Command {

    private $client;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'urls:shorten';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates dtcl.nz short links for given batch';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = new \GuzzleHttp\Client();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        ini_set('memory_limit','8000M');

        $batch = $this->option('batch');

        $get_source = Source::where('import_batch', '=', $batch)->where('short_url', '=', '')->where('invalid_mobile', '=', '0')->get();

        $i = 1;
        $num_source = count($get_source);

        foreach($get_source as $source)
        {
            $url = config('datacleanse.url') . 'member/update/' . $source->hash;

            $short_url = $this->shortenURL($url);

            $source->short_url = $short_url;
            $source->save();

            if($i % 100 == 0) {
                $this->info('Converted... ' . $i . ' of ' . $num_source);
            }

            $i++;

        }

    }

    protected function shortenURL($url)
    {

        $request = $this->client->post('http://dtcl.nz/link', [
            'headers' => ['X-Authorization' => 'f83573ace4098a04c7a3bec7301390b9aea3dfd9'],
            'form_params' => ['url' => $url],
        ]);

        $response = json_decode($request->getBody());

        //print_r($response);

        return $response->short_url;

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
        ];
    }

}
