<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use App\Source;
use App\CMList;

class ListsPrepare extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'lists:prepare';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create list entry from source mailing lists';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		echo "\n";

        ini_set('memory_limit','1024M');

        $batch = $this->option('batch');

        $mailing_lists = Source::select('import_batch', 'mailing_list')
                               ->where('import_batch', $batch)
                               ->where('mailing_list', '<>', '')
                               ->groupBy('mailing_list')
                               ->get();

        //print_r($mailing_lists->toArray()); exit;

        foreach($mailing_lists as $record)
        {
            $list = new CMList;
            $list->name = config('datacleanse.cm_list_prefix') . '_' . $record->mailing_list;
            $list->import_batch = $record->import_batch;
            $list->mailing_list = $record->mailing_list;
            $list->save();

            $this->info('Prepared: ' . $list->name);
        }

        echo "\n";

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
