<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;

class SMSProcess extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sms:process';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Process imported data to determine duplicates and prepare for export';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		ini_set('memory_limit','2048M');

		$nl = "\n";
		echo $nl;

        $batch = $this->option('batch');
        $singles = 0;
        $duplicates = 0;
		$mobile_field = config('datacleanse.mobile_field');

        // get all mobiles
        $mobiles = DB::table('source')->select('id', $mobile_field)->where('import_batch', '=', $batch)->where($mobile_field, '<>', '')->where('invalid_mobile', '=', '0')->get();

        foreach($mobiles as $record)
        {

            // count num mobiles
            $num_mobiles = DB::table('source')->select($mobile_field)->where('import_batch', '=', $batch)->where($mobile_field, '=', $record->$mobile_field)->count();

            if($num_mobiles === 1) $singles++;
            if($num_mobiles > 1) $duplicates++;

            // save number of duplicates against each record
            DB::table('source')->where('id', $record->id)->update(['mobile_duplicates' => $num_mobiles]);

            // divide into first mailing list
            if($num_mobiles === 1)
            {
                $mailing_list = $batch . '_1';
                DB::table('source')->where('id', $record->id)->update(['mailing_list' => $mailing_list]);
            }

        }

        // get max number of duplicates
        $max_duplicates = DB::table('source')->select(DB::raw('COUNT(*) as count'))->where('import_batch', '=', $batch)->groupBy($mobile_field)->orderBy('count', 'desc')->take(1)->get();

        $max_duplicates = (count($max_duplicates) > 0) ? $max_duplicates[0]->count : 0;

		$this->info('Total records: ' . count($mobiles));
		$this->info('Records with NO duplicates: ' . $singles);
		$this->info('Records with duplicates: ' . $duplicates);
		$this->info('Max duplicates: ' . $max_duplicates);

        // DIVIDE

        // get duplicate mobiles
        $duplicate_mobiles = DB::table('source')->select($mobile_field)->where('import_batch', '=', $batch)->where('mobile_duplicates', '>', 1)->groupBy($mobile_field)->get();

        // loop through each then assign mailing list number
        foreach($duplicate_mobiles as $duplicate_mobile)
        {
            $list_num = 1;

            // get all of same
            $get_duplicated = DB::table('source')->select('id', $mobile_field)->where('import_batch', '=', $batch)->where($mobile_field, '=', $duplicate_mobile->$mobile_field)->get();

            foreach($get_duplicated as $dup)
            {
                $mailing_list = $batch . '_' . $list_num;
                DB::table('source')->where('id', $dup->id)->update(['mailing_list' => $mailing_list]);
                $list_num++;
            }

        }

        //print_r($duplicate_mobiles); exit;


		echo $nl;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
