<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;

class DataProcess extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'source:process';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Process imported data to determine duplicates and prepare for export';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		ini_set('memory_limit','2048M');

		$nl = "\n";
		echo $nl;

        $batch = $this->option('batch');
        $singles = 0;
        $duplicates = 0;
		$email_field = config('datacleanse.email_field');

        // get all emails
        $emails = DB::table('source')->select('id', $email_field)->where('import_batch', '=', $batch)->where('invalid_email', '=', '0')->orderBy($email_field, 'asc')->get();
        $num_emails_to_process = count($emails);
        $i = 1;

        $this->info('Processing...');

        foreach($emails as $record)
        {

            // count num emails
            $num_emails = DB::table('source')->select($email_field)->where('import_batch', '=', $batch)->where($email_field, '=', $record->$email_field)->count();

            if($num_emails === 1) $singles++;
            if($num_emails > 1) $duplicates++;

            // save number of duplicates against each record
            DB::table('source')->where('id', $record->id)->update(['email_duplicates' => $num_emails]);

            // divide into first mailing list
            if($num_emails === 1)
            {
                $mailing_list = $batch . '_1';
                DB::table('source')->where('id', $record->id)->update(['mailing_list' => $mailing_list]);
            }

            if($i % 1000 == 0) $this->info('    ' . $i . ' of ' . $num_emails_to_process);

            $i++;

        }

        // get max number of duplicates
        $max_duplicates = DB::table('source')->select(DB::raw('COUNT(*) as count'))->where('import_batch', '=', $batch)->groupBy($email_field)->orderBy('count', 'desc')->take(1)->get();

        $max_duplicates = (count($max_duplicates) > 0) ? $max_duplicates[0]->count : 0;

		$this->info('Total records: ' . count($emails));
		$this->info('Records with NO duplicates: ' . $singles);
		$this->info('Records with duplicates: ' . $duplicates);
		$this->info('Max duplicates: ' . $max_duplicates);

        // DIVIDE

        // get duplicate emails
        $duplicate_emails = DB::table('source')->select($email_field)->where('import_batch', '=', $batch)->where('email_duplicates', '>', 1)->groupBy($email_field)->get();
        $num_duplicates_to_process = count($emails);
        $i = 1;

        $this->info('Allocating...');

        // loop through each then assign mailing list number
        foreach($duplicate_emails as $duplicate_email)
        {
            $list_num = 1;

            // get all of same
            $get_duplicated = DB::table('source')->select('id', $email_field)->where('import_batch', '=', $batch)->where($email_field, '=', $duplicate_email->$email_field)->get();

            foreach($get_duplicated as $dup)
            {
                $mailing_list = $batch . '_' . $list_num;
                DB::table('source')->where('id', $dup->id)->update(['mailing_list' => $mailing_list]);
                $list_num++;
            }

            if($i % 1000 == 0) $this->info('    ' . $i . ' of ' . $num_duplicates_to_process);

            $i++;

        }

        //print_r($duplicate_emails); exit;


		echo $nl;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
