<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use Excel;

class DataValidate extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'source:validate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Validate source data';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
    	ini_set('memory_limit','1024M');

		$nl = "\n";
		echo $nl;

        $batch = $this->option('batch');
        //echo $batch; exit;
        $fields = ['id', 'name_first', 'name', 'email_addr', 'import_batch', 'hash'];
        $export_path = storage_path('export');
        $validation_errors = [];
        $error_ids = [];
        //echo $export_path; exit;

        $email_field = config('datacleanse.email_field');

        $source = DB::table('source')->select(['id', $email_field])->where('import_batch', $batch)->get();
        $num_source = count($source);
        $i = 1;

        DB::table('source')->where('import_batch', $batch)->update(['invalid_email' => '0']);

        // validate data
        foreach($source as $record)
        {

            if (! filter_var($record->$email_field, FILTER_VALIDATE_EMAIL))
            {
                $validation_errors[] = 'Invalid Email (' . $record->id . '): ' . $record->$email_field;
                if(! in_array($record->id, $error_ids)) $error_ids[] = $record->id;

                // mark record as invalid email
                DB::table('source')->where($email_field, $record->$email_field)->where('import_batch', $batch)->update(['invalid_email' => '1']);

            }

            if($i % 1000 == 0) $this->info('Validating ' . $i . ' of ' . $num_source);

            $i++;

        }

        if(count($validation_errors) > 0)
        {
            $this->error('VALIDATION ERRORS:');
            echo $nl;

            foreach($validation_errors as $error)
            {
                $this->error($error);
            }

            echo $nl;
            $this->error('IDs: ' . implode(',', $error_ids));

            echo $nl;
            return;

        }

        $this->info('No validation errors were encountered');

		echo $nl;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
