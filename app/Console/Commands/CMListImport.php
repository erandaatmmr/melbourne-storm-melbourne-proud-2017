<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use App\Source;
use App\CMList;

class CMListImport extends Command {

    private $auth;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cm:lists-import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create CM list and import records';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

        $this->auth = ['api_key' => config('datacleanse.cm_api_key')];

	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        ini_set('memory_limit','8000M');

		$nl = "\n";
		echo $nl;

        $batch = $this->option('batch');

        $get_lists = CMList::query();
        $get_lists->where('list_created', '1');
        $get_lists->where('fields_created', '1');
        //$get_lists->where('list_imported', '0');
        $get_lists->where('import_batch', $batch);

        $mailing_list = $this->option('list');
        if($mailing_list) $get_lists->where('mailing_list', $mailing_list);

        $lists = $get_lists->get();

        //print_r($lists->toArray()); exit;

        foreach($lists as $list)
        {

            $this->info('Importing list: ' . $list->name . '...');
            echo "\n";

            $num_source = Source::where('mailing_list', $list->mailing_list)->count();
            $chunk_size = 999;
            $chunk = 1;
            $num_chunks = ceil($num_source / $chunk_size);
            $record_chunk = $chunk_size;

            $this->info('    (' . $num_source . ' total records)');
            echo "\n";

            // get source, chunk results
            $source = Source::where('mailing_list', $list->mailing_list)->chunk($chunk_size, function($source_records) use ($list, $num_source, &$chunk, $num_chunks, &$record_chunk, $chunk_size) {

                $this->info('    Importing chunk ' . $chunk . ' of ' . $num_chunks . ' (records ' . $record_chunk . ' of ' . $num_source . ')');

                //print_r($source_records->toArray());

                $this->importList($list, $source_records);

                $chunk++;
                $record_chunk = $record_chunk + $chunk_size;

            });

            $list->list_imported = $num_source;
            $list->save();

            echo "\n";

        }

		echo "\n";
		$this->info('Done.');
		echo "\n";

	}

    public function importList($list, $sourceData)
    {

        $this->info('        ...generating import data');

        $importData = [];

        $email_field = config('datacleanse.email_field');
        $first_name_field = config('datacleanse.first_name_field');
        $last_name_field = config('datacleanse.last_name_field');

        foreach($sourceData as $record)
        {

            $custom_fields = [];
            $list_fields = config('datacleanse.cm_list_fields');

            //print_r($list_fields);

            foreach($list_fields as $key => $field)
            {
                $custom_field = ['Key' => $key];

                $source_key = (isset($field['field'])) ? $field['field'] : false;

                if($source_key && isset($record->$source_key))
                {
                    $value = $record->$source_key;

                    if(isset($field['trim']))
                    {
                        $value = (strlen($value) > $field['trim']) ? substr($value, 0, $field['trim']-3).'...' : $value;
                    }

                    if(isset($field['date']))
                    {
                        $value = date($field['date'], strtotime($value));
                    }

                    if(isset($field['format']) && is_callable($field['format']))
                    {
                        $value = $field['format']($value);
                    }

                    $custom_field['Value'] = $value;

                }
                elseif(isset($field['custom']) && is_callable($field['custom']))
                {
                    $custom_field['Value'] = $field['custom']($record);
                }
                else
                {
                    $custom_field['Value'] = '';
                }

                $custom_fields[] = $custom_field;
            }


            $custom_fields[] = ['Key' => 'confirm_url', 'Value' => config('datacleanse.url') . 'member/confirm/' . $record->hash];
            $custom_fields[] = ['Key' => 'update_url', 'Value' => config('datacleanse.url') . 'member/update/' . $record->hash];

            //print_r($custom_fields); exit;

            $importData[] = [
                'EmailAddress' => $record->$email_field,
                'Name' => $record->$first_name_field . ' ' . $record->$last_name_field,
                'CustomFields' => $custom_fields,
            ];

        }

        //print_r($importData); exit;

        $this->cmImport($list, $importData);

    }

    public function cmImport($list, $importData) {

        $this->info('        ...sending to CM');

        $CS_REST_Subscribers = new \CS_REST_Subscribers($list->list_id, $this->auth);

        $importSubscribers = $CS_REST_Subscribers->import($importData, false);

        if(! $importSubscribers->was_successful())
        {
/*
            $this->error('Failed with code ' . $importSubscribers->http_status_code);
            print_r($importSubscribers->response);

            if(isset($importSubscribers->response->ResultData))
            {

                if($importSubscribers->response->ResultData->TotalExistingSubscribers > 0)
                {
                    $this->comment('Updated '.$importSubscribers->response->ResultData->TotalExistingSubscribers.' existing subscribers in the list');
                }
                else if($$importSubscribers->response->ResultData->TotalNewSubscribers > 0)
                {
                    $this->comment('Added '.$importSubscribers->response->ResultData->TotalNewSubscribers.' to the list');
                }
                else if(count($importSubscribers->response->ResultData->DuplicateEmailsInSubmission) > 0)
                { 
                    $this->comment($importSubscribers->response->ResultData->DuplicateEmailsInSubmission.' were duplicated in the provided array.');
                }

            }
*/

            $this->error('        ...imported with issues');
            //$this->error('The following emails failed to import correctly:');
            //echo $nl;
            //print_r($importSubscribers->response->ResultData->FailureDetails);
        }
        else
        {
            $this->info('        ...imported successfully!');
        }

        echo "\n";

    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
			['list', null, InputOption::VALUE_OPTIONAL, 'Mailing List', null],
		];
	}

}
