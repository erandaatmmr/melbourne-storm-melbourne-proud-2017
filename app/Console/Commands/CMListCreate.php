<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use App\Source;
use App\CMList;

class CMListCreate extends Command {

    private $auth;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cm:lists-create';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create a list on Campaign Monitor';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

        $this->auth = ['api_key' => config('datacleanse.cm_api_key')];

	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        ini_set('memory_limit','1024M');

		$nl = "\n";
		echo "\n";

        //$list_name = $this->ask('Enter a name for the new list:');

        $lists = CMList::where('list_created', '0')->orWhere('fields_created', '0')->get();

        //print_r($lists->toArray()); exit;

        foreach($lists as $list)
        {
            $this->createList($list);
        }

        $this->info('Done yo  =)');

		echo "\n";
	}

    public function createList($list)
    {

        $cm_client_id = config('datacleanse.cm_client_id');

        echo "\n";

        $this->info('Creating list - '.$list->name.'...');

        echo "\n";

        $CS_REST_Lists = new \CS_REST_Lists(NULL, $this->auth);

        $createList = $CS_REST_Lists->create($cm_client_id, [
            'Title' => $list->name,
            'ConfirmedOptIn' => false,
            'UnsubscribeSetting' => 'OnlyThisList'
        ]);

        if($createList->was_successful())
        {

            $listID = $createList->response;

            $this->info('    Created List: ' . $list->name . ' (' . $listID . ')');

            echo "\n";

            // save list
            $list->list_created = '1';
            $list->list_id = $listID;
            $list->save();

            // Create custom fields
            $fields_created = $this->createCustomFields($listID);

            $list->fields_created = ($fields_created) ? '1' : '0';
            $list->save();

        }
        else
        {
            //$this->error('Could not create list: ' . $createList->http_status_code);
            //print_r($createList->response);

            if($createList->response->Code == '250')
            {
                $this->info('    List already exists!');

                echo "\n";

                // Create custom fields
                if($list->list_id && ! $list->fields_created)
                    $fields_created = $this->createCustomFields($list->list_id);

                $list->fields_created = ($fields_created) ? '1' : '0';
                $list->save();

            }

        }

        echo "\n";

    }

    public function createCustomFields($listID) {

        $this->info('    Creating custom fields...');

        echo "\n";

        // Get list
        $cm_list = new \CS_REST_Lists($listID, $this->auth);

        $fields = array_keys(config('datacleanse.cm_list_fields'));
        $fields[] = 'confirm_url';
        $fields[] = 'update_url';

        $fields_created = true;

        foreach($fields as $field)
        {

            $createField = $cm_list->create_custom_field(array(
                'FieldName' => $field,
                'DataType' => 'Text',
            ));

            if($createField->was_successful())
            {
                $this->info('        Created Field: ' . $createField->response);
            }
            else
            {
                $this->error('       Could not create field: ' . $field);
                $fields_created = false;
            }

        }

        return $fields_created;

    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			//['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
