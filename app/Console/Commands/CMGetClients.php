<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use App\Source;

class CMGetClients extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cm:clients';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Lists all MMR Campaign Monitor clients';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$nl = "\n";
		echo $nl;

        $auth = ['api_key' => config('datacleanse.cm_api_key')];

        $wrap = new \CS_REST_General($auth);

        $result = $wrap->get_clients();

        if($result->was_successful())
        {
            $clients = $result->response;

            foreach($clients as $client)
            {
                $this->info($client->ClientID . ' : ' . $client->Name);
            }

        }

		echo $nl;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
