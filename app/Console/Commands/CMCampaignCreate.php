<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use App\Source;
use App\CMCampaign;

class CMCampaignCreate extends Command {

    private $auth;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cm:campaigns-create';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create a bunch of campaigns on Campaign Monitor';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

        $this->auth = ['api_key' => config('datacleanse.cm_api_key')];

	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        ini_set('memory_limit','1024M');

		$nl = "\n";
		echo "\n";

        $campaigns = CMCampaign::where('created', '0')->get();

        //print_r($campaigns->toArray()); exit;

        foreach($campaigns as $campaign)
        {

            $campaign_data = [
                'Subject' => $campaign->subject,
                'Name' => $campaign->name,
                'FromName' => $campaign->from_name,
                'FromEmail' => $campaign->from_email,
                'ReplyTo' => $campaign->from_email,
                'ListIDs' => [$campaign->cm_list_id],
                'TemplateID' => $campaign->template_id,
                'TemplateContent' => [
                    'Singlelines' => [],
                    'Multilines' => [],
                    'Images' => [],
                    'Repeaters' => [],
                ],
            ];

            //print_r($campaign_data); exit;

            $this->createCampaign($campaign_data, $campaign);
        }

        $this->info('Done yo  =)');

		echo "\n";
	}

    public function createCampaign($campaign_data, $campaign)
    {

        $cm_client_id = config('datacleanse.cm_client_id');

        echo "\n";

        $this->info('Creating campaign - '.$campaign_data['Name'].'...');

        echo "\n";

        $CS_REST_Campaigns = new \CS_REST_Campaigns(NULL, $this->auth);

        $createCampaign = $CS_REST_Campaigns->create_from_template($cm_client_id, $campaign_data);

        if($createCampaign->was_successful())
        {
            $this->info('    Created campaign: ' . $createCampaign->response);
            echo "\n";

            $campaign->created = 1;
            $campaign->campaign_id = $createCampaign->response;
            $campaign->save();

        }
        else
        {
            $this->error('    Could not create campaign!');
            echo "\n";
            print_r($createCampaign->response);
            echo "\n";
        }

    }


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['batch', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			//['batch', null, InputOption::VALUE_REQUIRED, 'Batch Number', null],
		];
	}

}
