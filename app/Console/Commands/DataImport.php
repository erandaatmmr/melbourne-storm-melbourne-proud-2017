<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Illuminate\Database\Eloquent\Model;

use DB;
use Excel;
use App\Source;

use Storage;

class DataImport extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'source:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import a CSV to source table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        Model::unguard();
        ini_set('memory_limit','2048M');

        echo "\n";

        $batches = [];
        $import_batch_option = $this->option('batch');

        //echo storage_path('import/imported'); exit;

        if(! $import_batch_option)
        {

            $source_files = Storage::files('import');
            $source_files = preg_grep("/.*\.csv/i", $source_files);
            //print_r($source_files); exit;
    
            foreach($source_files as $file)
            {
                $file_name = str_replace('import/', '', $file);
                $batch_name = str_replace('.csv', '', $file_name);
    
                $batches[] = [
                    'name' => $batch_name,
                    'file' => storage_path($file),
                    'file_name' => $file_name,
                ];
            }

        }
        else
        {

            $file_option = $this->option('file');

            $file = ($file_option) ? $file_option : $import_batch_option . '.csv';

            $batches[] = [
                'name' => $import_batch_option,
                'file' => storage_path($file),
                'file_name' => $file,
            ];

        }

        //print_r($batches); exit;

        $total_imported = 0;

        foreach($batches as $batch)
        {
            $batch_imported = $this->importBatch($batch);
            $total_imported = $total_imported + $batch_imported;
        }

        $this->info('Imported: ' . $total_imported . ' total records');

        echo "\n";

    }

    public function importBatch($batch)
    {

        $import_count = 0;
        $source_data = [];

        $email_field = config('datacleanse.email_field');

        $this->info('Importing file: ' . $batch['file_name']);

        // check if batch already exists
        if(DB::table('source')->select('id')->where('import_batch', '=', $batch['name'])->count() > 0)
        {
            return $this->error('    Records already exist for batch ID "' . $batch['name'] . '"');
        }

        Excel::load($batch['file'], function($reader) use ($batch, $import_count, $email_field) {

            $source_data = $reader->all()->toArray();
    
            foreach($source_data as $member)
            {
                $member[$email_field] = trim($member[$email_field]);
                $member['import_batch'] = $batch['name'];
                $member['hash'] = md5(implode('', $member));
                $import = Source::create($member);

                if($import)
                {
                    $import_count++;
                }
                else
                {
                    $this->error('    Could not import: ' . $member[$email_field]);
                }

                //print_r($member);
            }

            $this->info('    Imported: ' . $import_count . ' records');

        });

        // move import file
        //Storage::move($batch['file'], storage_path('import/done/' . $batch['file_name']));

        echo "\n";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['batch', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['batch', null, InputOption::VALUE_OPTIONAL, 'Batch Number', null],
            ['file', null, InputOption::VALUE_OPTIONAL, 'Import File', null]
        ];
    }

}
