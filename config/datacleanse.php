<?php

return [

	'title' => 'Melbourne Storm - Are you our secret weapon?',
    'sub_title' => 'Check out who Storm\'s Secret Weapon is!',

	'url' => 'http://melbourneproud.com.au/',
    'confirm_url' => 'member/(hash)',
    'update_url' => 'member/(hash)',

    'cm_api_key' => '709a8e2ecf9cd2af18a31e2f1140747c',

    'cm_client_id' => '569484c77b626fe5cd539f77fa9d021e',

    'cm_campaign_from_name' => 'Melbourne Storm',
    'cm_campaign_from_email' => 'membership@melbournestorm.com.au',
    'cm_campaign_reply_email' => 'membership@melbournestorm.com.au',
    'cm_campaign_subject' => 'Are you our secret weapon?',
    'cm_campaign_template_id' => '64c49e411458efb4bc7f6f870eab2120',

    'cm_list_prefix' => 'Melbourne_Proud_2017',
    'cm_list_fields' => [
        'member_id' => ['field' => 'acct_id'],
        'first_name' => ['field' => 'name_first'],
        'last_name' => ['field' => 'name_last'],
    ],

	'fields' => [
        'data_set' => ['type' => 'string', 'copy' => true],
        'acct_id' => ['type' => 'string', 'copy' => true],
        'name_first' => ['type' => 'string', 'copy' => true],
        'name_last' => ['type' => 'string', 'copy' => true],
        'email_addr' => ['type' => 'string', 'copy' => true],
        'single_family_flag' => ['type' => 'string', 'copy' => true],
        'member_since' => ['type' => 'string', 'copy' => true],
        'gender' => ['type' => 'string', 'copy' => true],
        'solicit_mail' => ['type' => 'string', 'copy' => true],
        'phone_mobile' => ['type' => 'string', 'copy' => true],
        'option1_title' => ['type' => 'string', 'copy' => true],
        'option1_description' => ['type' => 'string', 'copy' => true],
        'option1_url' => ['type' => 'string', 'copy' => true],
        'option1_price' => ['type' => 'string', 'copy' => true],
        'option1_price_month' => ['type' => 'string', 'copy' => true],
        'option2_title' => ['type' => 'string', 'copy' => true],
        'option2_description' => ['type' => 'string', 'copy' => true],
        'option2_url' => ['type' => 'string', 'copy' => true],
        'option2_price_month' => ['type' => 'string', 'copy' => true],
        'option3_description' => ['type' => 'string', 'copy' => true],
        'option3_url' => ['type' => 'string', 'copy' => true],
        'selected_product' => ['type' => 'string', 'output_only' => true],
	],

    'email_field' => 'email_addr',
    'mobile_field' => 'phone_mobile',
    'first_name_field' => 'name_first',
    'last_name_field' => 'name_last',

    'format_source' => [

    ],

    'validation_rules' => [

    ],

    'validation_messages' => [

    ],

    'validation_sanitize' => [

    ],

    'format_save' => [

    ],

    'google_tracking_id' => 'UA-5333835-53',
    'facebook_pixel_id' => '',
    'facebook_app_id' => '1514935971851671',

    // (First Name) (URL) (Opt Out)
    'sms' => '(First Name), don\'t stay a Lost Bulldog, come back to the Kennel in 2016. Watch this and help us find you! (URL) Opt out reply STOP 0427407841',
    'sms_no_name' => 'Dont stay a Lost Bulldog, come back to the Kennel in 2016. Watch this and help us find you! (URL) Opt out reply STOP to 0427407841',
    'sms_min_name' => 3,
    'sms_max_name' => 15,

    'notification_to_email' => 'membership@melbournestorm.com.au',
    'notification_to_name' => 'Melbourne Storm',
    'notification_from_email' => 'membership@melbournestorm.com.au',
    'notification_from_name' => 'Melbourne Storm',

];
